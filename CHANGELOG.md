# EasyUtils

***

#### v1.1.8

### Easy Identification (NEW)

#### New

* Introduced **Easy Identification** as a new id generation engine via interface implementation. All **Easy Utils**
  utilities that involve Ids will use this system in further updates.

### Easy Save

#### Changes

* Changed the save manager to produce calls to the IdManager (from **Easy Identification**) to bind the Ids at runtime.

#### Requirements

* **Easy Save** now needs the **IdManager** to be on the scene in order to work.

***

#### v.1.1.9

### Reflection Type Selector

#### New

* Added a new interface **IReflectionTypeCategory** to allow the user to define custom categories to be shown in the
  Generic
  Menu for the **ReflectionTypeSelector**.

### Persistent Singleton (NEW)

#### New

* Added a new class **PersistentSingleton** that allows the user to easily create a singleton that persists through
  scenes.

***

#### v1.2.0

### Easy Identification

#### New

* Added a new and enhanced editor for the **IdManager** that allows the user to easily create and manage Ids.

* Added **IdHierarchyMonitor** that allows the user to monitor the hierarchy of the scene and automatically assign/erase
  IDs to new objects.
* Added some Menu Items available on MonoBehaviours implementing **IIdentifiable** to easily assign and erase Ids.

* Added a new bool **LockId** to **IIdentifiable** that allows the user to lock the Id of an object, preventing it from
  being changed.

***

#### v1.2.1

### Easy Identification

#### Fixes

* Several bug fixes and improvements to the **IdHierarchyMonitor**

### Easy Save

#### New

* Now you can append data to an existing save file when saving.

* Added a new implementation to save settings for your game by simply using a **EasySettings** class. New available
  methods to save and load settings are available in the **SaveManager** class.

***

#### v1.2.2

### Easy Behaviour Tree (NEW)

#### New

* Added a new utility **EasyBT** that allows the user to create and manage behaviour trees in a visual way.
* Editor available at _EasyUtils/AI/Behaviour Tree Editor_.
* There are several nodes built-in:
    * Actions - Nodes that perform actions. These are always the leaf nodes of the tree.
        * Wait - Waits for a certain amount of time.
        * Log - Logs a message to the console.
        * Breakpoint - Pauses the execution of the game.
        * SetProperty - Sets a property in the blackboard.
        * CompareProperty - Compares a property of the blackboard with a value.
        * RandomFailure - Randomly fails.
        * RandomNumber - Generates a random number.
        * RandomPosition - Generates a random position.
        * SubTree - Executes a sub-tree.
    * Conditions - Nodes that return success/failure based on a condition. Also leaf nodes.
        * N/A
    * Composites - Nodes that have children and control their execution.
        * Sequence - Executes children in order until one fails.
        * Selector - Executes children in order until one succeeds.
        * RandomSelector - Executes children in random order until one succeeds.
        * Parallel - Executes all children in parallel.
    * Decorators - Nodes that modify the execution of their children.
        * Inverter - Inverts the result of the child.
        * Succeed - Always succeeds.
        * Failure - Always fails.
        * Repeat - Repeats the child a number of times.
        * Timeout - Fails if the child takes too long to execute.

### Easy Identification

#### New

* Added a new method GetBehaviourById<T> that allows the user to get a specific behaviour from the **IdManager** by its
  Id.

#### Fixes

* Several fixes and improvements to the **IdHierarchyMonitor**.

### Easy Singleton

#### New

* Added project settings to manage singletons lifecycle. Now you can specify whether a singleton can be created when
  loading the scene or when getting the instance.

#### Changes

* Renamed the namespace **EasyUtils.DesignPatterns** to **EasyUtils.EasySingleton**.

### Easy Pool

#### Changes

* Some variable names were changed to better reflect their purpose.

### Helpers

#### New

* Added new helper **AssetDatabaseHelper**.
* Added new helper **DirectoryHelper**.
* Added new helper **CoroutineManager** for managing coroutines when you don't have a MonoBehaviour.

### Easy WFC

#### Fixes

* Fixed a NullReferenceException when creating a new **WfcNode** from scratch.

***
#### v1.2.3

### Easy Debug

#### Changes

* Modified the whole structure of the system to work with the new Unity's runtime binding system.

#### Removed

* Removed the **IDebugField** interface.
* Removed the **DebugField** class.
* Removed the **DebugManager** class.
