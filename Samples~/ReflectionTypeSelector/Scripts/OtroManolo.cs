﻿using System;
using EasyUI.Interfaces;

namespace Samples.ReflectionTypeSelector.Scripts
{
    /// <summary>
    /// <inheritdoc cref="SubManolo"/>
    /// </summary>

    [ReflectionTypeCategory("Manolos/ManoloSubtipos")]
    [Serializable]
    public class OtroManolo: Manolo
    {
        //We define some values and what the Execute method does with them.
        public override int Num1 => 20;
        public override int Num2 => 2;
    }
}
