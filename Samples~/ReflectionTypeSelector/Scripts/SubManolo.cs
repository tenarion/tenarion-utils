﻿using System;
using EasyUI.Interfaces;

namespace Samples.ReflectionTypeSelector.Scripts
{
    /// <summary>
    /// An class inheriting from <see cref="Manolo"/>
    /// </summary>

    [ReflectionTypeCategory("Manolos/ManoloSubtipos")]
    [Serializable]
    public class SubManolo: Manolo
    {
        //We define some values and what the Execute method does with them.
        public override int Num1 => 5;
        public override int Num2 => 10;
    }
}
