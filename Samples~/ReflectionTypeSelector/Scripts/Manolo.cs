using System;
using EasyUI.Interfaces;

namespace Samples.ReflectionTypeSelector.Scripts
{
    [Serializable]
    public abstract class Manolo
    {
        //We define a sample class with some data values and an abstract function called in the OnSelect method.
        public abstract int Num1 { get; }
        public abstract int Num2 { get; }
    }
}
