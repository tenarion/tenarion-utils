﻿using Editor.EasyUI.CustomVisualElements;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Samples.ReflectionTypeSelector.Scripts.Editors
{
    [CustomEditor(typeof(ManoloHandler))]
    public class ManoloHandlerEditor: UnityEditor.Editor
    {
        public override VisualElement CreateInspectorGUI()
        {

            var data = target as ManoloHandler;

            if (data == null) return null;
            
            var root = new VisualElement();

            var selector = new ReflectionTypeSelector<Manolo>();
            
            var resultLabel = new Label
            {
                text = data.MyManolo != null ? $"Manolo selected: {data.MyManolo?.GetType().Name}" : ""
            };

            

            selector.OnItemSelected += manolo =>
            {
                
                //create an instance of the given type
                
                var instance = System.Activator.CreateInstance(manolo) as Manolo;

                if (instance == null) return;
                
                resultLabel.text = $"Manolo selected: {instance.GetType().Name}";
                data.MyManolo = instance;
                
                EditorUtility.SetDirty(data);
            };

            root.Add(selector);
            root.Add(resultLabel);
            return root;
        }
    }
}
