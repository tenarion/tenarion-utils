﻿using System;
using UnityEngine;

namespace Samples.ReflectionTypeSelector.Scripts
{
    /// <summary>
    /// A wrapper <see cref="MonoBehaviour"/> containing a <see cref="Manolo"/> for use at runtime.
    /// </summary>
    [Serializable]
    public class ManoloHandler : MonoBehaviour
    {

        [SerializeReference] public Manolo MyManolo;
    }
}
