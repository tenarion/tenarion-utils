using System.Collections;
using System.Collections.Generic;
using EasySave;
using Resources.EasyDebug;
using UnityEngine;

public class SettingsTest : MonoBehaviour
{
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SaveSettings()
    {
        var settings = new EasySettings.Builder()
            .AddCategory("Gameplay")
            .SetSetting("Setting 1", 10f)
            .SetSetting("Setting 2", "Hello, world!")
            .Wrap()
            .AddCategory("Graphics")
            .SetSetting("Setting 3", 20)
            .SetSetting("Setting 4", true)
            .SetSetting("Setting 5", 0.5f)
            .Wrap()
            .Build();

        SaveManager.Instance.SaveSettings(settings);
    }

    public void DisplaySettings()
    {
        var settings = SaveManager.Instance.LoadSettings();

        foreach (var category in settings.Categories)
        {
            EasyLog.LogColor($"Category: {category.Name}", Color.green);
            foreach (var setting in category.Settings)
            {
                EasyLog.LogColor($"Setting: {setting.Name} = {setting.Value}", Color.cyan);
            }
        }
    }
}
