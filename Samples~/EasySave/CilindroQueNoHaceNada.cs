using Attributes;
using EasyIdentification;
using Editor.Attributes;
using UnityEngine;

namespace Samples.EasySave
{
    public class CilindroQueNoHaceNada : MonoBehaviour, IIdentifiable
    {
        [field:SerializeField] [field:ReadOnly] public string Id { get; set; }
        public bool LockId { get; set; }
    }
}
