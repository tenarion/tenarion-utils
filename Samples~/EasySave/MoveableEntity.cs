using System;
using System.Collections;
using System.Collections.Generic;
using Attributes;
using EasySave;
using Editor.Attributes;
using Newtonsoft.Json;
using TMPro;
using UnityEngine;

public class MoveableEntity : MonoBehaviour, ISaveable
{
    public int timesTouched = 0;
    [SerializeField] [ReadOnly] private string _id;

    private static readonly int TimesTapped = Shader.PropertyToID("_TimesTapped");

    public string Id
    {
        get => _id;
        set => _id = value;
    }

    [field:SerializeField] public bool LockId { get; set; }

    public object SaveData => new MoveableEntityData(timesTouched, transform.position, transform.rotation.eulerAngles);
    public void LoadState(string jsonData)
    {
        var data = JsonConvert.DeserializeObject<MoveableEntityData>(jsonData);
        timesTouched = data.timesTouched;
        transform.position = data.position;
        transform.eulerAngles = data.rotation;
        GetComponent<MeshRenderer>().material.SetFloat(TimesTapped, timesTouched);
        GetComponentInChildren<TMP_Text>().text = timesTouched.ToString();
    }

    private void OnCollisionEnter(Collision other)
    {
        //check if the collision is from the player
        if (other.gameObject.CompareTag("Player"))
        {

            timesTouched++;
            print(timesTouched);
            GetComponent<MeshRenderer>().material.SetFloat(TimesTapped, timesTouched);
            GetComponentInChildren<TMP_Text>().text = timesTouched.ToString();
        }
    }


}

public class MoveableEntityData
{
    public int timesTouched = 0;
    public Vector3 position;
    public Vector3 rotation;

    public MoveableEntityData(int timesTouched, Vector3 position, Vector3 rotation)
    {
        this.timesTouched = timesTouched;
        this.position = position;
        this.rotation = rotation;
    }
}
