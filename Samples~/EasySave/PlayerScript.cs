using System;
using Attributes;
using EasySave;
using Editor.Attributes;
using Newtonsoft.Json;
using UnityEngine;

namespace Samples.EasySave
{
    public class PlayerScript : MonoBehaviour, ISaveable
    {
        private Vector3 dir;

        public string playerName;
        public int playerLevel;
        public float playerHealth;
        public float playerMana;
        public float playerStamina;
        public float playerSpeed = 5.0f;
        [SerializeField] [ReadOnly] private string _id;
        private Rigidbody _rigidbody;


        public string Id
        {
            get => _id;
            set => _id = value;
        }

        public bool LockId { get; set; }


        public object SaveData => new PlayerData(playerName, playerLevel, playerHealth, playerMana, playerStamina,
            transform.position, playerSpeed);

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        public void LoadState(string jsonData)
        {
            var playerData = JsonConvert.DeserializeObject<PlayerData>(jsonData);

            playerName = playerData.playerName;
            playerLevel = playerData.playerLevel;
            playerHealth = playerData.playerHealth;
            playerMana = playerData.playerMana;
            playerStamina = playerData.playerStamina;
            playerSpeed = playerData.playerSpeed;

            transform.position = playerData.playerPosition;
        }

        void Update()
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            dir = new Vector3(moveHorizontal / 10, 0.0f, moveVertical / 10);

            if (Input.GetKeyDown(KeyCode.F))
            {
                SaveManager.Instance.SaveGame();
            }

            if(Input.GetKeyDown(KeyCode.G))
                SaveManager.Instance.SaveGame(append: true);
            
            if (Input.GetKeyDown(KeyCode.L))
            {
                SaveManager.Instance.LoadGame();
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                PointSystem.Instance.points++;
                PointSystem.Instance.RefreshDisplay();
            }
        }

        private void FixedUpdate()
        {
            _rigidbody.AddForce(dir * playerSpeed, ForceMode.VelocityChange);
        }
    }

    [Serializable]
    public class PlayerData
    {

        public string playerName;
        public int playerLevel;
        public float playerHealth;
        public float playerMana;
        public float playerStamina;
        public Vector3 playerPosition;
        public float playerSpeed = 5.0f;

        public PlayerData(string playerName, int playerLevel, float playerHealth, float playerMana, float playerStamina,
            Vector3 playerPosition, float playerSpeed)

        {
            this.playerName = playerName;
            this.playerLevel = playerLevel;
            this.playerHealth = playerHealth;
            this.playerMana = playerMana;
            this.playerStamina = playerStamina;
            this.playerPosition = playerPosition;
            this.playerSpeed = playerSpeed;
        }
    }
}
