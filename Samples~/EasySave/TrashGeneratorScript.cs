using System;
using System.Collections.Generic;
using System.Linq;
using Attributes;
using EasySave;
using Editor.Attributes;
using Newtonsoft.Json;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Samples.EasySave
{
    public class TrashGeneratorScript : MonoBehaviour, ISaveable
    {
        [SerializeField] [ReadOnly] private string _id;

        private List<float> _trashValues = new();

        private void Start()
        {
            for (int i = 0; i < 10000; i++)
            {
                _trashValues.Add(Random.Range(0f,100f));
            }

            SaveManager.Instance.OnSaveComplete += (isAutoSave) =>
            {
                print($"Save finished! Autosave? {isAutoSave}");
            };
        }

        public string Id
        {
            get => _id;
            set => _id = value;
        }

        public bool LockId { get; set; }

        public object SaveData => new TrashSaveData(_trashValues);
        public void LoadState(string jsonData)
        {
            _trashValues = JsonConvert.DeserializeObject<TrashSaveData>(jsonData)._trashValues;
            
            print($"Trash generated and loaded: {_trashValues.Sum(f => f)}");
        }
    }

    public class TrashSaveData
    {
        public List<float> _trashValues;
        public TrashSaveData(List<float> values)
        {
            _trashValues = values;
        }
    }
}
