using System;
using System.Collections.Generic;
using Attributes;
using EasySave;
using Editor.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TMPro;
using UnityEngine;

namespace Samples.EasySave
{
    public class PointSystem : MonoBehaviour, ISaveable
    {
        public static PointSystem Instance;
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        [SerializeField] private TMP_Text pointText;
        public int points = 0;
        [SerializeField] [ReadOnly] private string _id;

        public void RefreshDisplay()
        {
            pointText.text = points.ToString();
        }

        
        public string Id
        {
            get => _id;
            set => _id = value;
        }

        public bool LockId { get; set; }
        public object SaveData => new PointSaveState(points);
        public void LoadState(string jsonData)
        {
            var pointSaveState = JsonConvert.DeserializeObject<PointSaveState>(jsonData);
            points = pointSaveState.points;
            RefreshDisplay();

        }
    }

    [Serializable]
    public class PointSaveState
    {
        public int points;

        public PointSaveState()
        {
            points = 0;
        }

        public PointSaveState(int points)
        {
            this.points = points;
        }
    }
}
