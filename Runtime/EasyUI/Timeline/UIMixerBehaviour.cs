using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UIElements;

namespace EasyUI.Timeline
{
    /// <summary>
    /// A mixer for mixing / blending together UIT clips (behaviours).
    /// </summary>
    [Serializable]
    public class UIMixerBehaviour : PlayableBehaviour, IMultipleElements
    {
        public List<VisualElement> Elements { get; set; }
        public bool AutomaticUsageHints { get; internal set; }

        public override void OnGraphStart(Playable playable)
        {
            var suggestedHints = UsageHints.None;

            // Initialize clips
            for (var i = 0; i < playable.GetInputCount(); i++)
            {
                var playableInput = (ScriptPlayable<UIBehaviour>)playable.GetInput(i);
                var behaviour = playableInput.GetBehaviour();

                suggestedHints |= behaviour.Hints;

                if(behaviour is IMultipleElements multipleElements) multipleElements.Elements = Elements;
            }

            if (AutomaticUsageHints)
            {
                foreach (var e in Elements)
                {
                    e.usageHints = suggestedHints;
                }
            }
        }

        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            var position = new Vector2();
            var rotation = 0f;
            var scale = new Vector2(1f, 1f);
            var opacity = 1f;

            for (var i = 0; i < playable.GetInputCount(); i++)
            {
                var playableInput = (ScriptPlayable<UIBehaviour>)playable.GetInput(i);
                var behaviour = playableInput.GetBehaviour();
                var weight = playable.GetInputWeight(i);
                
                switch (behaviour)
                {
                    case UIPositionBehaviour trs:
                        position += trs.Position * weight;
                        break;
                    case UIScaleBehaviour scl:
                        scale += scl.Scale * weight;
                        break;
                    case UIRotationBehaviour rot:
                        rotation += rot.Rotation * weight;
                        break;
                    case UIOpacityBehaviour op:
                        opacity += op.Opacity * weight;
                        break;
                }
            }

            foreach (var e in Elements)
            {
                e.transform.position = position;
                e.transform.scale = scale;
                e.transform.rotation = Quaternion.Euler(0, 0, rotation);
                e.style.opacity = opacity;
            }
        }
    }
}
