using UnityEngine.Timeline;

namespace EasyUI.Timeline
{
    public class UIPositionClip : UIClip<UIPositionBehaviour>
    {
        public override ClipCaps clipCaps => ClipCaps.All;
    }
}