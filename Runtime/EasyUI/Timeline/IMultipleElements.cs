using System.Collections.Generic;
using UnityEngine.UIElements;

namespace EasyUI.Timeline
{
    public interface IMultipleElements
    {
        List<VisualElement> Elements { get; set; }
    }
}