using UnityEngine.Timeline;

namespace EasyUI.Timeline
{
    public class UIDisplayClip : UIClip<UIDisplayBehaviour>
    {
        public override ClipCaps clipCaps => ClipCaps.None;
    }
}