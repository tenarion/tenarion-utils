using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace EasyUI.Timeline
{
    /// <summary>
    /// The behaviour / data of the scale operation.
    /// 
    /// <see cref="UIScaleClip"/>
    /// </summary>
    [Serializable]
    public class UIScaleBehaviour : UIBehaviour
    {
        public override UsageHints Hints => UsageHints.DynamicTransform;

        public Vector2 Scale;
    }
}
