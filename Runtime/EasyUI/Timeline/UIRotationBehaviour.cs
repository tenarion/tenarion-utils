using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace EasyUI.Timeline
{
    /// <summary>
    /// The behaviour / data of the rotation operation.
    /// 
    /// <see cref="UIScaleClip"/>
    /// </summary>
    [Serializable]
    public class UIRotationBehaviour : UIBehaviour
    {
        public override UsageHints Hints => UsageHints.DynamicTransform;

        public float Rotation;
    }
}
