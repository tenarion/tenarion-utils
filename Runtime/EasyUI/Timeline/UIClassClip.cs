using UnityEngine.Timeline;

namespace EasyUI.Timeline
{
    public class UIClassClip : UIClip<UIClassBehaviour>
    {
        public override ClipCaps clipCaps => ClipCaps.None;
    }
}