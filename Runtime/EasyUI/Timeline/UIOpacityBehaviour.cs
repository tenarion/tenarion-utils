using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UIElements;

namespace EasyUI.Timeline
{
    /// <summary>
    /// The behaviour / data of the opacity operation.
    /// 
    /// <see cref="UIOpacityClip"/>
    /// </summary>
    [Serializable]
    public class UIOpacityBehaviour : UIBehaviour
    {
        [Range(-1, 0)] public float Opacity;
    }
}
