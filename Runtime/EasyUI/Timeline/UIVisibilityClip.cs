using UnityEngine.Timeline;

namespace EasyUI.Timeline
{
    public class UIVisibilityClip : UIClip<UIVisibilityBehaviour>
    {
        public override ClipCaps clipCaps => ClipCaps.None;
    }
}