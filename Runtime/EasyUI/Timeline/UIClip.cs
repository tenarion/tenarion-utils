using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace EasyUI.Timeline
{
    public abstract class UIClip<T> : PlayableAsset, ITimelineClipAsset where T : UIBehaviour , new()
    {
        public T Template = new();
        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            return ScriptPlayable<T>.Create(graph, Template);
        }

        public abstract ClipCaps clipCaps { get; }
    }
}
