using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UIElements;

namespace EasyUI.Timeline
{
    /// <summary>
    /// Generic behaviour for all UI behaviours.
    /// </summary>
    [Serializable]
    public class UIBehaviour : PlayableBehaviour
    {
        /// <summary>
        /// Defines which usage hints, if any, should be used when this behaviour is active, to optimize performance.
        /// </summary>
        public virtual UsageHints Hints => UsageHints.None;
    }
}
