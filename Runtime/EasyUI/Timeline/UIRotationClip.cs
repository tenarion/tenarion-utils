using UnityEngine.Timeline;

namespace EasyUI.Timeline
{
    public class UIRotationClip : UIClip<UIRotationBehaviour>
    {
        public override ClipCaps clipCaps => ClipCaps.All;
    }
}