using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UIElements;

namespace EasyUI.Timeline
{
    /// <summary>
    /// The behaviour / data of the class add / remove operation.
    /// 
    /// <see cref="UIClassClip"/>
    /// </summary>
    [Serializable]
    public class UIClassBehaviour : UIBehaviour, IMultipleElements
    {
        public string ClassName;

        public List<VisualElement> Elements { get; set; }

        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
            if (Elements == null || string.IsNullOrEmpty(ClassName)) return;
            foreach (var e in Elements)
            {
                e.AddToClassList(ClassName);
            }
        }

        public override void OnBehaviourPause(Playable playable, FrameData info)
        {
            if (Elements == null || string.IsNullOrEmpty(ClassName)) return;
            foreach (var e in Elements)
            {
                e.RemoveFromClassList(ClassName);
            }
        }
    }
}
