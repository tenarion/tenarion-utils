using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UIElements;

namespace EasyUI.Timeline
{
    /// <summary>
    /// The behaviour / data of the position operation.
    /// 
    /// <see cref="UIPositionClip"/>
    /// </summary>
    [Serializable]
    public class UIPositionBehaviour : UIBehaviour
    {
        public override UsageHints Hints => UsageHints.DynamicTransform;

        public Vector2 Position;
    }
}
