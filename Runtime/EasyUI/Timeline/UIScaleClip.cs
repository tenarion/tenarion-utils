using UnityEngine.Timeline;

namespace EasyUI.Timeline
{
    public class UIScaleClip : UIClip<UIScaleBehaviour>
    {
        public override ClipCaps clipCaps => ClipCaps.All;
    }
}