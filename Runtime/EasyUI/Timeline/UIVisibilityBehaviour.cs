using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UIElements;

namespace EasyUI.Timeline
{
    /// <summary>
    /// The behaviour / data of the visibility enable / disable.
    /// 
    /// <see cref="UIVisibilityClip"/>
    /// </summary>
    [Serializable]
    public class UIVisibilityBehaviour : UIBehaviour, IMultipleElements
    {
        public bool Visible;

        public List<VisualElement> Elements { get; set; }
        
        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
            if (Elements == null) return;
            foreach (var e in Elements)
            {
                e.visible = Visible;
            }
        }

        public override void OnBehaviourPause(Playable playable, FrameData info)
        {
            if (Elements == null) return;
            foreach (var e in Elements)
            {
                e.visible = !Visible;
            }
        }
    }
}
