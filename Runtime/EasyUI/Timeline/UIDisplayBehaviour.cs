using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UIElements;

namespace EasyUI.Timeline
{
    /// <summary>
    /// The behaviour / data of the display enable / disable (Flex / None).
    /// 
    /// <see cref="UIDisplayClip"/>
    /// </summary>
    [Serializable]
    public class UIDisplayBehaviour : UIBehaviour, IMultipleElements
    {
        public bool Display;

        public List<VisualElement> Elements { get; set; }

        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
            if (Elements == null) return;
            foreach (var e in Elements)
            {
                e.style.display = Display ? DisplayStyle.Flex : DisplayStyle.None;
            }
        }

        public override void OnBehaviourPause(Playable playable, FrameData info)
        {
            if (Elements == null) return;
            foreach (var e in Elements)
            {
                e.style.display = !Display ? DisplayStyle.Flex : DisplayStyle.None;
            }
        }
    }
}
