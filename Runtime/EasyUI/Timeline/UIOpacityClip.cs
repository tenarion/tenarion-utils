using UnityEngine.Timeline;

namespace EasyUI.Timeline
{
    public class UIOpacityClip : UIClip<UIOpacityBehaviour>
    {
        public override ClipCaps clipCaps => ClipCaps.All;
    }
}
