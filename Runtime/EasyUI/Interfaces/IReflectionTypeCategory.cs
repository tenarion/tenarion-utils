using System;

namespace EasyUI.Interfaces
{
    /// <summary>
    /// Optional interface for reflection types to be categorized when showing them on the Generic Menu.
    /// </summary>
    public class ReflectionTypeCategoryAttribute : Attribute
    {
        public string CategoryPath { get; set; }

        public ReflectionTypeCategoryAttribute(string categoryPath)
        {
            CategoryPath = categoryPath;
        }
    }
}