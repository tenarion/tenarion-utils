namespace EasyIdentification
{
    public interface IIdentifiable
    {
        string Id { get; set; }
        bool LockId { get; set; }
    }
}