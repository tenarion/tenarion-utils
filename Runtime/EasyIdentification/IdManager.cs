using System;
using System.Collections.Generic;
using System.Linq;
using AYellowpaper.SerializedCollections;
using DesignPatterns.EasySingleton;
using UnityEngine;

namespace EasyIdentification
{
    public class IdManager : Singleton<IdManager>
    {
        public SerializedDictionary<SaveableData, MonoBehaviour> SavedIds = new();
        public bool Verbose;
        public bool MonitorHierarchy;

        public IEnumerable<MonoBehaviour> RegisterIds()
        {
            var result = new List<MonoBehaviour>();
            var behaviours = FindObjectsByType<MonoBehaviour>(FindObjectsSortMode.None);
            foreach (var behaviour in behaviours)
                if (behaviour is IIdentifiable identifiable)
                {
                    RegisterId(identifiable);
                    result.Add(behaviour);
                }

            return result;
        }

        public void RegisterId(IIdentifiable identifiable)
        {
            var newGuid = identifiable.Id;
            if (string.IsNullOrEmpty(identifiable.Id))
            {
                newGuid = Guid.NewGuid().ToString();
                identifiable.Id = newGuid;
            }

            if (SavedIds.All(pair => pair.Key.Id != newGuid))
                SavedIds.Add(
                    new SaveableData(newGuid,
                        $"{(identifiable as MonoBehaviour)!.name} ({identifiable.GetType().Name})"),
                    identifiable as MonoBehaviour);
        }

        public IEnumerable<MonoBehaviour> EraseIds()
        {
            var result = new List<MonoBehaviour>();
            var toErase = SavedIds.ToList();
            foreach (var pair in toErase)
            {
                if (pair.Value is not IIdentifiable identifiable || identifiable.LockId) continue;
                EraseId(identifiable);
                result.Add(pair.Value);
            }

            return result;
        }

        public void EraseId(IIdentifiable identifiable, bool bypassLock = false)
        {
            if (!bypassLock && identifiable.LockId) return;
            identifiable.Id = string.Empty;
            SavedIds.Remove(SavedIds.FirstOrDefault(x => x.Value == identifiable as MonoBehaviour).Key);
        }

        public MonoBehaviour GetBehaviourById(string id)
        {
            return SavedIds.FirstOrDefault(x => x.Key.Id == id).Value;
        }

        public T GetBehaviourById<T>(string id) where T : MonoBehaviour
        {
            return GetBehaviourById(id) as T;
        }

        #region Debug

        public IEnumerable<MonoBehaviour> EraseIds_Debug()
        {
            var result = new List<MonoBehaviour>();
            foreach (var behaviour in FindObjectsByType<MonoBehaviour>(FindObjectsInactive.Include,
                         FindObjectsSortMode.None))
                if (behaviour is IIdentifiable identifiable)
                {
                    identifiable.Id = string.Empty;
                    result.Add(behaviour);
                }

            SavedIds.Clear();
            return result;
        }

        #endregion

        [Serializable]
        public class SaveableData
        {
            public string Id;
            public string Name;

            public SaveableData(string id, string name)
            {
                Id = id;
                Name = name;
            }
        }
    }
}
