using System;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DesignPatterns.EasySingleton
{
    /// <summary>
    /// A manager for creating singleton instances that are marked to be created on a certain time.
    /// </summary>
    public static class SingletonManager
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void ManageSingletonCreation()
        {
            var settings = EasySingletonSettings.GetOrCreateSettings();
            var singletons = settings.Singletons;
            foreach (var singleton in singletons.Where(singleton =>  singleton.CreateOnLoad && !InstanceExists(singleton)))
                CreateInstance(singleton);
        }

        public static Component CreateInstance(SingletonData singleton)
        {
            var go = new GameObject(singleton.TypeName);
            var instance = go.AddComponent(Type.GetType(singleton.AssemblyTypeName));
            return instance;
        }

        private static bool InstanceExists(SingletonData singleton) => Object.FindFirstObjectByType(Type.GetType(singleton.AssemblyTypeName)) != null;
    }
}
