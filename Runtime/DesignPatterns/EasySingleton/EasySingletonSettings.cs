using System;
using System.Collections.Generic;
using System.Linq;
using Core.Attributes;
using Core.Helpers;
using UnityEditor;
using UnityEngine;

namespace DesignPatterns.EasySingleton
{
    public class EasySingletonSettings : ScriptableObject
    {
        public const string EasySingletonSettingsPath =
            "Assets/Resources/EasyUtils/EasySingletonSettings.asset";

        [ReadOnly] public List<SingletonData> Singletons = new();

        public static EasySingletonSettings GetOrCreateSettings()
        {
#if UNITY_EDITOR
            var settings = AssetDatabase.LoadAssetAtPath<EasySingletonSettings>(EasySingletonSettingsPath);
            if (settings == null)
            {
                //if folder doesn't exist create it
                AssetDatabaseHelper.CreateFolderHierarchyIfNotExists("Resources/EasyUtils");

                settings = CreateInstance<EasySingletonSettings>();

                AssetDatabase.CreateAsset(settings, EasySingletonSettingsPath);
                AssetDatabase.SaveAssets();
            }

            return settings;
#else
            var settings = UnityEngine.Resources.Load<EasySingletonSettings>("EasyUtils/EasySingletonSettings");
            return settings;
#endif
        }

#if UNITY_EDITOR
        public static SerializedObject GetSerializedSettings()
        {
            return new SerializedObject(GetOrCreateSettings());
        }
#endif
        public SingletonData GetSingletonData(Type type)
        {
            return Singletons.FirstOrDefault(s => s.TypeName == type.Name);
        }
    }

    [Serializable]
    public class SingletonData
    {
        [field: SerializeField] public string TypeName { get; set; }
        [field: SerializeField] public bool CreateOnLoad { get; set; }
        [field: SerializeField] public bool CreateOnGet { get; set; }

        [field: SerializeField, HideInInspector]
        public string AssemblyTypeName { get; set; }

        public SingletonData(Type type, bool createOnLoad, bool createOnGet)
        {
            TypeName = type.Name;
            AssemblyTypeName = type.AssemblyQualifiedName;
            CreateOnLoad = createOnLoad;
            CreateOnGet = createOnGet;
        }

        public SingletonData(string typeName, string assemblyQualifiedTypeName, bool createOnLoad, bool createOnGet)
        {
            TypeName = typeName;
            AssemblyTypeName = assemblyQualifiedTypeName;
            CreateOnLoad = createOnLoad;
            CreateOnGet = createOnGet;
        }
    }
}
