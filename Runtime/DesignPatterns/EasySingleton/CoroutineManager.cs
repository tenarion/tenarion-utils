using System.Collections;
using UnityEngine;

namespace DesignPatterns.EasySingleton
{
    /// <summary>
    /// A manager for easily create and manage coroutines at runtime when you don't have a MonoBehaviour available.
    /// </summary>
    public class CoroutineManager : PersistentSingleton<CoroutineManager>
    {
        public static CoroutineHandle RunCoroutine(IEnumerator routine)
        {
            if (Instance == null)
            {
                Debug.LogError("[COROUTINE MANAGER] CoroutineManager instance is null. Coroutine will not run.");
                return null;
            }
            return new CoroutineHandle(Instance, Instance.StartCoroutine(routine));
        }

        public class CoroutineHandle
        {
            private readonly CoroutineManager _manager;
            public Coroutine Coroutine { get; }

            internal CoroutineHandle(CoroutineManager manager, Coroutine coroutine)
            {
                _manager = manager;
                Coroutine = coroutine;
            }

            public void Stop()
            {
                if (Coroutine == null) return;
                _manager.StopCoroutine(Coroutine);
            }
        }
    }
}
