using UnityEngine;

namespace DesignPatterns.EasySingleton
{
    public abstract class PersistentSingleton<T> : MonoBehaviour, ISingleton where T : MonoBehaviour
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    var settings = EasySingletonSettings.GetOrCreateSettings();
                    if (settings == null) return null;
                    var singletonData = settings.GetSingletonData(typeof(T));
                    if (singletonData.CreateOnGet) _instance = SingletonManager.CreateInstance(singletonData) as T;
                }

                return _instance;
            }
        }

        public virtual void Awake()
        {
            if (_instance == null)
            {
                _instance = this as T;
                transform.SetParent(null);
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Debug.LogWarning($"A singleton instance of {nameof(T)} already exists.");
            }
        }
    }
}
