using UnityEngine;

namespace DesignPatterns.EasySingleton
{
    /// <summary>
    /// A simple singleton pattern implementation.
    /// </summary>
    /// <typeparam name="T">The MonoBehaviour type.</typeparam>
    public abstract class Singleton<T> : MonoBehaviour, ISingleton where T : MonoBehaviour
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    var settings = EasySingletonSettings.GetOrCreateSettings();
                    var singletonData = settings.GetSingletonData(typeof(T));
                    if (singletonData.CreateOnGet) _instance = SingletonManager.CreateInstance(singletonData) as T;
                }

                return _instance;
            }
        }

        public virtual void Awake()
        {
            if (_instance == null)
                _instance = this as T;
            else
                Debug.LogWarning(
                    $"A singleton instance of {nameof(T)} already exists. The active instance will be used instead.");
        }
    }
}
