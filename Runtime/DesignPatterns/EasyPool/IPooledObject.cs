namespace EasyPool
{
    /// <summary>
    /// The interface for objects that are pooled.
    /// Used together with the <see cref="PoolManager"/> singleton.
    /// </summary>
    public interface IPooledObject
    {
        void OnGet();
        void OnRelease();
        void OnDestroy();
    }
}
