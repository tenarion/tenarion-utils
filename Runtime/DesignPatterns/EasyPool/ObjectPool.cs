using System;
using System.Collections.Generic;
using UnityEngine;

namespace EasyPool
{
    public class ObjectPool<T> : IObjectPool<T> where T : class
    {
        private Queue<T> _pool = new();
        public int Count { get; private set; }
        public int CountActive => Count - CountInactive;
        public int CountInactive => _pool.Count;
        public int Capacity { get; }

        public event Action<T> OnGet;
        public event Action<T> OnRelease;
        public event Action<T> OnDestroy;

        private readonly Func<T> _createObjectFunc;

        public ObjectPool(int maxCapacity, Func<T> createObjectFunc, Action<T> commonGetAction = null,
            Action<T> commonReleaseAction = null, Action<T> commonDestroyAction = null)
        {
            if (maxCapacity <= 0)
            {
                throw new ArgumentException("Capacity must be greater than 0.");
            }

            Capacity = maxCapacity;
            _createObjectFunc = createObjectFunc ?? throw new ArgumentNullException(nameof(createObjectFunc));
            OnGet += commonGetAction;
            OnRelease += commonReleaseAction;
            OnDestroy += commonDestroyAction;
        }

        public T Get()
        {
            T obj;

            if (_pool.Count > 0)
                obj = _pool.Dequeue();
            else
            {
                obj = _createObjectFunc.Invoke();
                Count++;
            }

            OnGet?.Invoke(obj);
            if (obj is IPooledObject pooled) pooled.OnGet();
            return obj;
        }

        public void Release(T obj)
        {
            if (CountInactive < Capacity)
            {
                _pool.Enqueue(obj);
                OnRelease?.Invoke(obj);
                if (obj is IPooledObject pooled) pooled.OnRelease();
            }
            else
            {
                OnDestroy?.Invoke(obj);
                if (obj is IPooledObject pooled) pooled.OnDestroy();
            }
        }

        public void Clear()
        {
            while (_pool.Count > 0)
            {
                var obj = _pool.Dequeue();
                OnDestroy?.Invoke(obj);
                if (obj is IPooledObject pooled) pooled.OnDestroy();
            }
        }
    }
}
