namespace EasyPool
{
    public interface IObjectPool<T> where T : class
    {
        public int Count { get; }
        public T Get();
        public void Release(T obj);
        public void Clear();
    }
}
