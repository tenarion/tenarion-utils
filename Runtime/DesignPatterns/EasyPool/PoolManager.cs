using System;
using System.Collections.Generic;
using System.Linq;
using DesignPatterns.EasySingleton;
using UnityEngine;
using Object = UnityEngine.Object;

namespace EasyPool
{
    /// <summary>
    /// A struct for defining a pool.
    /// </summary>
    [Serializable]
    public struct PoolData
    {
        public string Name;
        public GameObject PoolPrefab;
        public Transform Parent;
        public int Size;
    }

    /// <summary>
    /// A simple Pool manager for Unity GameObjects built on top of <see cref="ObjectPool{T}"/>.
    /// </summary>
    public class PoolManager : Singleton<PoolManager>
    {
        /// <summary>
        /// Whether the object must implement the <see cref="IPooledObject"/> interface.
        /// </summary>
        [SerializeField] private bool _pooledObjectRequired;

        [SerializeField] private List<PoolData> _poolDataArray;
        private Dictionary<string, ObjectPool<GameObject>> _objectPools;


        public override void Awake()
        {
            base.Awake();
            _objectPools = new Dictionary<string, ObjectPool<GameObject>>();
            foreach (var poolData in _poolDataArray) InitPool(poolData);
        }

        private void InitPool(PoolData poolData)
        {
            if (_pooledObjectRequired && !poolData.PoolPrefab.TryGetComponent<IPooledObject>(out _))
            {
                Debug.LogWarning(
                    $"[EASY POOL] Cannot add object to the pool. Object '{nameof(poolData.PoolPrefab)}' does not implement IPooledObject interface.");
                return;
            }

            // Initialize a new object pool with actions to set the object active and inactive.
            var objectPool = new ObjectPool<GameObject>(poolData.Size, () =>
                {
                    var obj = Instantiate(poolData.PoolPrefab);
                    if (poolData.Parent) obj.transform.parent = poolData.Parent;
                    return obj;
                }, commonGetAction: obj => obj.SetActive(true), commonReleaseAction: obj => obj.SetActive(false),
                commonDestroyAction: Destroy);

            _objectPools.Add(poolData.Name, objectPool);
        }

        /// <summary>
        /// Retrieves an object from a specified pool.
        /// </summary>
        /// <param name="poolName">The name of the pool.</param>
        /// <param name="position">The default position where the object will spawn.</param>
        /// <param name="forceRetrieve">Whether the object should be retrieved even if it's still active.</param>
        /// <typeparam name="T">The component to retrieve.</typeparam>
        /// <returns>The desired component of the retrieved object or the default value if not found.</returns>
        [Obsolete("Use Get<T> instead.")]
        public T RetrieveObject<T>(string poolName, Vector3 position = new(), bool forceRetrieve = false)
        {
            return Get<T>(poolName, position);
        }

        /// <summary>
        /// Gets an object from a pool.
        /// </summary>
        /// <param name="poolName">The name of the pool.</param>
        /// <param name="position">The starting position.</param>
        /// <param name="rotation">The starting rotation.</param>
        /// <typeparam name="T">The component of the GameObject that you want to get.</typeparam>
        /// <returns></returns>
        public T Get<T>(string poolName, Vector3 position = new(), Quaternion rotation = new())
        {
            while (true)
            {
                if (!_objectPools.TryGetValue(poolName, out var pool))
                {
                    Debug.LogError($"[EASY POOL] Pool '{poolName}' not found.");
                    return default;
                }

                var obj = pool.Get();
                obj.transform.position = position;
                obj.transform.rotation = rotation;
                return obj.GetComponent<T>() ?? default;
            }
        }

        public GameObject Get(string poolName, Vector3 position = new(), Quaternion rotation = new())
        {
            while (true)
            {
                if (!_objectPools.TryGetValue(poolName, out var pool))
                {
                    Debug.LogError($"[EASY POOL] Pool '{poolName}' not found.");
                    return default;
                }

                var obj = pool.Get();
                obj.transform.position = position;
                obj.transform.rotation = rotation;
                return obj;
            }
        }

        /// <summary>
        /// Releases an object back to its pool.
        /// </summary>
        /// <param name="poolName">The name of the pool</param>
        /// <param name="obj">The object you want to release</param>
        public void Release(string poolName, GameObject obj)
        {
            if (!_objectPools.TryGetValue(poolName, out var pool))
            {
                Debug.LogError($"[EASY POOL] Pool '{poolName}' not found.");
                return;
            }

            pool.Release(obj);
        }

        /// <summary>
        /// Gets a pool by its name.
        /// <param name="poolName">The name of the pool</param>
        /// <returns>The desired pool or the default value if not found.</returns>
        /// </summary>
        public ObjectPool<GameObject> GetPool(string poolName)
        {
            if (!_objectPools.TryGetValue(poolName, out var pool))
            {
                Debug.LogError($"Pool '{poolName}' not found.");
                return default;
            }

            return pool;
        }

        public void Create(string poolName, GameObject prefab, Transform parent = null, int size = 10)
        {
            if (_objectPools.ContainsKey(poolName))
            {
                Debug.LogWarning($"[EASY POOL] Pool '{poolName}' already exists.");
                return;
            }

            var poolData = new PoolData
            {
                Name = poolName,
                PoolPrefab = prefab,
                Parent = parent,
                Size = size
            };

            Instance._poolDataArray.Add(poolData);
            Instance.InitPool(poolData);
        }
    }
}
