using EasyEventChannel.EventChannels;
using UnityEngine;
using UnityEngine.Events;

namespace EasyEventChannel.EventListeners
{
    /// <summary>
    /// A listener for an event channel that invokes a UnityEvent when the event is raised. Based on the Unite talk by Ryan Hipple.
    /// </summary>
    public class EventListener : MonoBehaviour
    {
        public UnityEvent Response;
        public EventChannel EventChannel;

        private void OnEnable()
        {
            EventChannel.RegisterListener(this);
        }

        private void OnDisable()
        {
            EventChannel.UnregisterListener(this);
        }

        public void OnEventRaised()
        {
            Response.Invoke();
        }
    }

    /// <summary>
    /// A listener for an event channel that invokes a UnityEvent when the event is raised. Based on the Unite talk by Ryan Hipple.
    /// </summary>
    /// <typeparam name="T">The type used as a parameter.</typeparam>
    public abstract class EventListener<T> : MonoBehaviour
    {
        public EventChannel<T> EventChannel;
        public UnityEvent<T> Response;

        private void OnEnable()
        {
            EventChannel.RegisterListener(this);
        }

        private void OnDisable()
        {
            EventChannel.UnregisterListener(this);
        }

        public void OnEventRaised(T parameter)
        {
            Response.Invoke(parameter);
        }
    }

    /// <summary>
    /// A listener for an event channel that invokes a UnityEvent when the event is raised. Based on the Unite talk by Ryan Hipple.
    /// </summary>
    /// <typeparam name="T1">The type 1 used as a parameter.</typeparam>
    /// <typeparam name="T2">The type 2 used as a parameter.</typeparam>
    public abstract class EventListener<T1, T2> : MonoBehaviour
    {
        public EventChannel<T1, T2> EventChannel;
        public UnityEvent<T1, T2> Response;

        private void OnEnable()
        {
            EventChannel.RegisterListener(this);
        }

        private void OnDisable()
        {
            EventChannel.UnregisterListener(this);
        }

        public void OnEventRaised(T1 parameter1, T2 parameter2)
        {
            Response.Invoke(parameter1, parameter2);
        }
    }
}