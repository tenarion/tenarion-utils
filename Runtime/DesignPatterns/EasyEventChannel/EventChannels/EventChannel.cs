using System.Collections.Generic;
using EasyEventChannel.EventListeners;
using UnityEngine;

namespace EasyEventChannel.EventChannels
{
    /// <summary>
    /// An event channel that can be raised without any parameters.
    /// </summary>
    [CreateAssetMenu(menuName = "Easy Utils/Event Channels/NoArgs")]
    public class EventChannel : ScriptableObject
    {
        private readonly HashSet<EventListener> _eventListeners = new();

        public void Raise()
        {
            foreach (var eventListener in _eventListeners) eventListener.OnEventRaised();
        }

        public void RegisterListener(EventListener listener)
        {
            _eventListeners.Add(listener);
        }

        public void UnregisterListener(EventListener listener)
        {
            _eventListeners.Remove(listener);
        }
    }

    /// <summary>
    /// An event channel that can be raised with one parameter.
    /// </summary>
    public abstract class EventChannel<T> : ScriptableObject
    {
        private readonly HashSet<EventListener<T>> _eventListeners = new();

        public void Raise(T parameter)
        {
            foreach (var eventListener in _eventListeners) eventListener.OnEventRaised(parameter);
        }

        public void RegisterListener(EventListener<T> listener)
        {
            _eventListeners.Add(listener);
        }

        public void UnregisterListener(EventListener<T> listener)
        {
            _eventListeners.Remove(listener);
        }
    }

    /// <summary>
    /// An event channel that can be raised with two parameters.
    /// </summary>
    public abstract class EventChannel<T1, T2> : ScriptableObject
    {
        private readonly HashSet<EventListener<T1, T2>> _eventListeners = new();

        public void Raise(T1 parameter1, T2 parameter2)
        {
            foreach (var eventListener in _eventListeners) eventListener.OnEventRaised(parameter1, parameter2);
        }

        public void RegisterListener(EventListener<T1, T2> listener)
        {
            _eventListeners.Add(listener);
        }

        public void UnregisterListener(EventListener<T1, T2> listener)
        {
            _eventListeners.Remove(listener);
        }
    }
}