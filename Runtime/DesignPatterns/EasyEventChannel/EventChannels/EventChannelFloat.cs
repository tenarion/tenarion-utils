using UnityEngine;

namespace EasyEventChannel.EventChannels
{
    [CreateAssetMenu(menuName = "Easy Utils/Event Channels/Float")]
    public class EventChannelFloat : EventChannel<float>
    {
    }
}