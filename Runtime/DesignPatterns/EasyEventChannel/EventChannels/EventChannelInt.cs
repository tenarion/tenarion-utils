using UnityEngine;

namespace EasyEventChannel.EventChannels
{
    [CreateAssetMenu(menuName = "Easy Utils/Event Channels/Int")]
    public class EventChannelInt : EventChannel<int>
    {
    }
}