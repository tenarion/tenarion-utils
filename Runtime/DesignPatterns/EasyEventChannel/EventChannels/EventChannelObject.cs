using UnityEngine;

namespace EasyEventChannel.EventChannels
{
    [CreateAssetMenu(menuName = "Easy Utils/Event Channels/GameObject")]
    public class EventChannelObject : EventChannel<GameObject>
    {
    }
}