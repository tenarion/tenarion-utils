using UnityEngine;

namespace EasyEventChannel.EventChannels
{
    [CreateAssetMenu(menuName = "Easy Utils/Event Channels/Int2")]
    public class EventChannelInt2 : EventChannel<int, int>
    {
    }
}