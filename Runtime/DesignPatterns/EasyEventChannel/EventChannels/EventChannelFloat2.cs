using UnityEngine;

namespace EasyEventChannel.EventChannels
{
    [CreateAssetMenu(menuName = "Easy Utils/Event Channels/Float2")]
    public class EventChannelFloat2 : EventChannel<float, float>
    {
    }
}