using DesignPatterns.EasySM.GenericStateMachine;

namespace DesignPatterns.EasySM.UnityStateMachine
{
    public interface IUnityFiniteStateMachine<TBaseState> : IFiniteStateMachine<TBaseState>, IUnityStateMachine
        where TBaseState : IUnityState
    {
    }
}
