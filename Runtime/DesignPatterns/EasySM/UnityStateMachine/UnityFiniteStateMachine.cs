using System;
using System.Collections.Generic;
using System.Linq;
using Core.Helpers;
using UnityEngine;

namespace DesignPatterns.EasySM.UnityStateMachine
{
    public class UnityFiniteStateMachine<TBaseState> : IUnityFiniteStateMachine<TBaseState>
        where TBaseState : UnityFiniteState<TBaseState>
    {
        public bool Active { get; set; }
        private TBaseState _currentState;
        public TBaseState[] States { get; set; }
        public event Action<TBaseState> OnStateChange;
        public event Action<TBaseState> OnStateEnter;
        public event Action<TBaseState> OnStateExit;
        public event Action<TBaseState> OnStateUpdate;
        public event Action<TBaseState> OnStateFixedUpdate;
        public event Action<TBaseState> OnStateLateUpdate;

        public UnityFiniteStateMachine(params object[] constructor)
        {
            try
            {
                States = ReflectionHelper.CreateChildInstances<TBaseState>(constructor);
                Active = true;
            }
            catch (Exception e)
            {
                throw new ArgumentException("[EASY SM] Error when creating states. Did you specify the correct constructor?", e);
            }
        }

        public UnityFiniteStateMachine(TBaseState[] states)
        {
            States = states;
        }

        public void SetState<T>() where T : TBaseState
        {
            _currentState?.OnStateExit(this);
            OnStateExit?.Invoke(_currentState);
            var state = GetState<T>();
            _currentState = state;
            _currentState.OnStateEnter(this);
            OnStateChange?.Invoke(state);
            OnStateEnter?.Invoke(state);
        }

        public T GetState<T>() where T : TBaseState
        {
            try
            {
                return (T)States.First(s => s.GetType() == typeof(T));
            }

            catch (Exception e)
            {
                Debug.LogError("[EASY SM] Error while trying to get the state: " + e);
                return default;
            }
        }

        public bool IsStateActive<T>() where T : TBaseState
        {
            return _currentState is T;
        }

        public void Start()
        {
            if (!Active) return;
            _currentState.Start(this);
        }

        public void Update()
        {
            if (!Active) return;
            _currentState.Update(this);
            OnStateUpdate?.Invoke(_currentState);
        }

        public void FixedUpdate()
        {
            if (!Active) return;
            _currentState.FixedUpdate(this);
            OnStateFixedUpdate?.Invoke(_currentState);
        }

        public void LateUpdate()
        {
            if (!Active) return;
            _currentState.LateUpdate(this);
            OnStateLateUpdate?.Invoke(_currentState);
        }

        public void OnCollisionEnter(Collision other)
        {
            if (!Active) return;
            _currentState.OnCollisionEnter(this, other);
        }

        public void OnCollisionStay(Collision other)
        {
            if (!Active) return;
            _currentState.OnCollisionStay(this, other);
        }

        public void OnCollisionExit(Collision other)
        {
            if (!Active) return;
            _currentState.OnCollisionExit(this, other);
        }

        public void OnTriggerEnter(Collider other)
        {
            if (!Active) return;
            _currentState.OnTriggerEnter(this, other);
        }

        public void OnTriggerStay(Collider other)
        {
            if (!Active) return;
            _currentState.OnTriggerStay(this, other);
        }

        public void OnTriggerExit(Collider other)
        {
            if (!Active) return;
            _currentState.OnTriggerExit(this, other);
        }

        public IEnumerable<string> ToStringList()
        {
            return States.Select(state => state.GetType().Name);
        }
    }
}
