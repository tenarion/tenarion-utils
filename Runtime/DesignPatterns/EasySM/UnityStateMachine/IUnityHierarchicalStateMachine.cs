using DesignPatterns.EasySM.GenericStateMachine;

namespace DesignPatterns.EasySM.UnityStateMachine
{
    public interface IUnityHierarchicalStateMachine<TBaseState> : IHierarchicalStateMachine<TBaseState>,
        IUnityStateMachine
        where TBaseState : IUnityState
    {
    }
}
