using UnityEngine;

namespace DesignPatterns.EasySM.UnityStateMachine
{
    public abstract class UnityFiniteState<TBaseState> : IUnityState<UnityFiniteStateMachine<TBaseState>, TBaseState>
        where TBaseState : UnityFiniteState<TBaseState>
    {
        public abstract void OnStateEnter(UnityFiniteStateMachine<TBaseState> unityFiniteStateMachine);
        public abstract void OnStateExit(UnityFiniteStateMachine<TBaseState> unityFiniteStateMachine);

        public virtual void Start(UnityFiniteStateMachine<TBaseState> unityFiniteStateMachine)
        {
        }

        public virtual void Update(UnityFiniteStateMachine<TBaseState> unityFiniteStateMachine)
        {
        }

        public virtual void FixedUpdate(UnityFiniteStateMachine<TBaseState> unityFiniteStateMachine)
        {
        }

        public virtual void LateUpdate(UnityFiniteStateMachine<TBaseState> unityFiniteStateMachine)
        {
        }

        public virtual void OnCollisionEnter(UnityFiniteStateMachine<TBaseState> unityFiniteStateMachine,
            Collision other)
        {
        }

        public virtual void OnCollisionStay(UnityFiniteStateMachine<TBaseState> unityFiniteStateMachine,
            Collision other)
        {
        }

        public virtual void OnCollisionExit(UnityFiniteStateMachine<TBaseState> unityFiniteStateMachine,
            Collision other)
        {
        }

        public virtual void OnTriggerEnter(UnityFiniteStateMachine<TBaseState> unityFiniteStateMachine,
            Collider other)
        {
        }

        public virtual void OnTriggerStay(UnityFiniteStateMachine<TBaseState> unityFiniteStateMachine, Collider other)
        {
        }

        public virtual void OnTriggerExit(UnityFiniteStateMachine<TBaseState> unityHierarchicalStateMachine,
            Collider other)
        {
        }
    }
}
