using System.Collections.Generic;
using UnityEngine;

namespace DesignPatterns.EasySM.UnityStateMachine
{
    /// <summary>
    /// A Hierarchical state compatible with Hierarchical StateMachines.
    /// </summary>
    /// <typeparam name="TBaseState">The type of state this state belongs to.</typeparam>
    public abstract class
        UnityHierarchicalState<TBaseState> : IUnityState<UnityHierarchicalStateMachine<TBaseState>, TBaseState>
        where TBaseState : UnityHierarchicalState<TBaseState>
    {
        public bool Active { get; internal set; }

        public TBaseState SuperState;
        public List<TBaseState> SubStates = new();
        public abstract void OnStateEnter(UnityHierarchicalStateMachine<TBaseState> unityHierarchicalStateMachine);
        public abstract void OnStateExit(UnityHierarchicalStateMachine<TBaseState> unityHierarchicalStateMachine);

        public virtual void Start(UnityHierarchicalStateMachine<TBaseState> unityHierarchicalStateMachine)
        {
        }

        public virtual void Update(UnityHierarchicalStateMachine<TBaseState> unityHierarchicalStateMachine)
        {
        }

        public virtual void FixedUpdate(UnityHierarchicalStateMachine<TBaseState> unityHierarchicalStateMachine)
        {
        }

        public virtual void LateUpdate(UnityHierarchicalStateMachine<TBaseState> unityHierarchicalStateMachine)
        {
        }

        public virtual void OnCollisionEnter(UnityHierarchicalStateMachine<TBaseState> unityHierarchicalStateMachine,
            Collision other)
        {
        }

        public virtual void OnCollisionStay(UnityHierarchicalStateMachine<TBaseState> unityHierarchicalStateMachine,
            Collision other)
        {
        }

        public virtual void OnCollisionExit(UnityHierarchicalStateMachine<TBaseState> unityHierarchicalStateMachine,
            Collision other)
        {
        }

        public virtual void OnTriggerEnter(UnityHierarchicalStateMachine<TBaseState> unityHierarchicalStateMachine,
            Collider other)
        {
        }

        public virtual void OnTriggerStay(UnityHierarchicalStateMachine<TBaseState> unityHierarchicalStateMachine,
            Collider other)
        {
        }

        public virtual void OnTriggerExit(UnityHierarchicalStateMachine<TBaseState> unityHierarchicalStateMachine,
            Collider other)
        {
        }

        public override string ToString()
        {
            var res = GetType().Name;
            SubStates.ForEach(state => res += " >> " + state);
            return res;
        }
    }
}
