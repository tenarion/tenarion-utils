using UnityEngine;

namespace DesignPatterns.EasySM.UnityStateMachine
{
    /// <summary>
    /// An interface for indicating that a state machine uses unity lifecycle methods.
    /// </summary>
    public interface IUnityStateMachine
    {
        public void Start();
        public void Update();
        public void FixedUpdate();
        public void LateUpdate();
        public void OnCollisionEnter(Collision other);
        public void OnCollisionStay(Collision other);
        public void OnCollisionExit(Collision other);
        public void OnTriggerEnter(Collider other);
        public void OnTriggerStay(Collider other);
        public void OnTriggerExit(Collider other);
    }
}
