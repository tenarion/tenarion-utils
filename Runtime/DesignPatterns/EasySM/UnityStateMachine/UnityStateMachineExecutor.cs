using System;
using System.Collections;
using System.Collections.Generic;
using DesignPatterns.EasySingleton;
using DesignPatterns.EasySM.GenericStateMachine;
using UnityEngine;

namespace DesignPatterns.EasySM.UnityStateMachine
{
    /// <summary>
    /// A state machine executor that can be attached to a game object to execute all UnityStateMachine lifecycle methods automatically.
    /// </summary>
    public class UnityStateMachineExecutor : MonoBehaviour
    {
        private IUnityStateMachine _unityStateMachine;

        private void Start()
        {
            var implementation = GetComponent<IStateMachineImplementation>();
            if (implementation == null)
            {
                Debug.LogError("No state machine implementation found in the game object.");
                return;
            }

            StartCoroutine(LateStart(implementation));
        }

        private IEnumerator LateStart(IStateMachineImplementation implementation)
        {
            yield return null;

            if (!implementation.TryGetUnityStateMachine(out _unityStateMachine))
            {
                Debug.LogError("No UnityStateMachine found in the implementation.");
                yield break;
            }

            _unityStateMachine?.Start();
        }

        private void Update()
        {
            _unityStateMachine?.Update();
        }

        private void FixedUpdate()
        {
            _unityStateMachine?.FixedUpdate();
        }

        private void LateUpdate()
        {
            _unityStateMachine?.LateUpdate();
        }

        private void OnTriggerEnter(Collider other)
        {
            _unityStateMachine?.OnTriggerEnter(other);
        }

        private void OnTriggerExit(Collider other)
        {
            _unityStateMachine?.OnTriggerExit(other);
        }

        private void OnTriggerStay(Collider other)
        {
            _unityStateMachine?.OnTriggerStay(other);
        }

        private void OnCollisionEnter(Collision other)
        {
            _unityStateMachine?.OnCollisionEnter(other);
        }

        private void OnCollisionExit(Collision other)
        {
            _unityStateMachine?.OnCollisionExit(other);
        }

        private void OnCollisionStay(Collision other)
        {
            _unityStateMachine?.OnCollisionStay(other);
        }
    }
}
