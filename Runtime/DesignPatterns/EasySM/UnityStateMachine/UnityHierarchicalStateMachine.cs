using System;
using System.Collections.Generic;
using System.Linq;
using Core.Helpers;
using UnityEngine;

namespace DesignPatterns.EasySM.UnityStateMachine
{
    /// <summary>
    /// <para>A pre-made Hierarchical State Machine.</para>
    /// <para>Can be generally used to implement Hierarchical State Machines with no need for further modification.</para>
    /// <para>You can also use it with a pre-made executor, which will allow the State Machine to execute its methods with a singleton
    /// <see cref="UnityStateMachineExecutor"/>, without having to explicitly invoke them on your main MonoBehaviour.</para>
    ///
    /// </summary>
    /// <typeparam name="TBaseState">The type of states this State Machine functions with.</typeparam>
    public class UnityHierarchicalStateMachine<TBaseState> : IUnityHierarchicalStateMachine<TBaseState>
        where TBaseState : UnityHierarchicalState<TBaseState>
    {
        public bool Active { get; set; }

        /// <summary>
        /// <para>The State Machine's default state.</para>
        /// <remarks>Whenever the State Machine has no state it will default to this one.</remarks>
        /// </summary>
        private TBaseState _defaultState;

        /// <summary>
        /// The State Machine's top most state.
        /// </summary>
        private TBaseState _currentState;

        /// <summary>
        /// An array containing all states of TBaseState.
        /// </summary>
        /// <typeparam name="TBaseState">The type of states this StateMachine functions with.</typeparam>
        public TBaseState[] States { get; set; }

        public event Action<TBaseState> OnStateChange;
        public event Action<TBaseState> OnStateEnter;
        public event Action<TBaseState> OnStateExit;
        public event Action<TBaseState> OnStateUpdate;
        public event Action<TBaseState> OnStateFixedUpdate;
        public event Action<TBaseState> OnStateLateUpdate;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityHierarchicalStateMachine{TBaseState}"/> class with the specified context and constructor.
        /// <para></para>
        /// With an executor available, the State Machine will be able to execute its methods with a <see cref="UnityStateMachineExecutor"/>
        /// without having to explicitly invoke them on your main MonoBehaviour.
        /// </summary>
        /// <param name="executor">Whether the State Machine will use the pre-made executor or not.</param>
        /// <param name="constructor"></param>
        public UnityHierarchicalStateMachine(params object[] constructor)
        {
            try
            {
                States = ReflectionHelper.CreateChildInstances<TBaseState>(constructor);
                Active = true;
            }
            catch (Exception e)
            {
                throw new ArgumentException("Error when creating states. Did you specify the correct constructor?", e);
            }
        }

        /// <summary>
        /// Sets a state as the State Machine's default state.
        /// </summary>
        /// <typeparam name="T">The state being set as default.</typeparam>
        /// <remarks><para>Should always be called at the State Machine's start.</para>
        /// If there is no current state active, the default state becomes active.</remarks>
        public void SetDefaultState<T>() where T : TBaseState
        {
            _defaultState = GetState<T>();

            if (_currentState != null) return;

            _currentState = _defaultState;

            EnterState(_currentState);
        }

        /// <summary>
        /// Gets the unique instance of <typeparamref name="T"/> pertaining to a
        /// <see cref="UnityHierarchicalState{TBaseState}"/>
        /// </summary>
        /// <typeparam name="T">The type of the state to get.</typeparam>
        /// <returns>A unique instance of <typeparamref name="T"/>.</returns>
        public T GetState<T>() where T : TBaseState
        {
            if (typeof(T) == typeof(TBaseState))
                throw new ArgumentException("The type of the given state cannot be the same as the base state.");

            try
            {
                return (T)States.First(s => s.GetType() == typeof(T));
            }

            catch (Exception e)
            {
                throw new ArgumentException($"The state {typeof(T).Name} does not exist in the state machine.", e);
            }
        }

        public void Start()
        {
            if (!Active) return;
            Start(_currentState);
        }

        /// <summary>
        /// This method begins the chain of Update beginning on the current state.
        /// </summary>
        public void Update()
        {
            if (!Active) return;
            Update(_currentState);
            OnStateUpdate?.Invoke(_currentState);
        }

        public void Update(TBaseState state)
        {
            state.Update(this);

            state.SubStates?.ForEach(Update);
        }


        public void FixedUpdate()
        {
            if (!Active) return;
            FixedUpdate(_currentState);
            OnStateFixedUpdate?.Invoke(_currentState);
        }

        public void LateUpdate()
        {
            if (!Active) return;
            LateUpdate(_currentState);
            OnStateLateUpdate?.Invoke(_currentState);
        }

        public void OnCollisionEnter(Collision other)
        {
            if (!Active) return;
            _currentState.OnCollisionEnter(this, other);
            _currentState.SubStates?.ForEach(state => state.OnCollisionEnter(this, other));
        }

        public void OnCollisionStay(Collision other)
        {
            if (!Active) return;
            _currentState.OnCollisionStay(this, other);
            _currentState.SubStates?.ForEach(state => state.OnCollisionStay(this, other));
        }

        public void OnCollisionExit(Collision other)
        {
            if (!Active) return;
            _currentState.OnCollisionExit(this, other);
            _currentState.SubStates?.ForEach(state => state.OnCollisionExit(this, other));
        }

        public void OnTriggerEnter(Collider other)
        {
            if (!Active) return;
            _currentState.OnTriggerEnter(this, other);
            _currentState.SubStates?.ForEach(state => state.OnTriggerEnter(this, other));
        }

        public void OnTriggerStay(Collider other)
        {
            if (!Active) return;
            _currentState.OnTriggerStay(this, other);
            _currentState.SubStates?.ForEach(state => state.OnTriggerStay(this, other));
        }

        public void OnTriggerExit(Collider other)
        {
            if (!Active) return;
            _currentState.OnTriggerExit(this, other);
            _currentState.SubStates?.ForEach(state => state.OnTriggerExit(this, other));
        }

        public bool IsStateActive<T>() where T : TBaseState
        {
            return GetState<T>().Active;
        }

        /// <summary>
        /// Changes one state with another, severing ties to any substates in the process.
        /// </summary>
        /// <typeparam name="T">The state being changed.</typeparam>
        /// <typeparam name="TK">The state to replace with.</typeparam>
        public void ChangeState<T, TK>() where T : TBaseState where TK : TBaseState
        {
            var exitingState = GetState<T>();
            var enteringState = GetState<TK>();

            var newParent = DetachFromParent(exitingState);

            CutSubstates(exitingState);
            CutSubstate(enteringState);

            AddSubstate(newParent, enteringState);
        }

        /// <summary>
        /// Replaces one state with another while inhereting it's relationships.
        /// </summary>
        /// <typeparam name="T">The state being replaced.</typeparam>
        /// <typeparam name="TK">The state to replace with.</typeparam>
        /// <remarks>Using this function to replace two active states will remove any substates on the latter.</remarks>
        public void ReplaceState<T, TK>() where T : TBaseState where TK : TBaseState
        {
            var exitingState = GetState<T>();
            var enteringState = GetState<TK>();

            var newParent = DetachFromParent(exitingState);

            var substates = PopSubstates(exitingState, true);

            AddSubstate(newParent, enteringState);

            AddSubstates(enteringState, substates, true);
        }

        public void RelegateState<T>() where T : TBaseState
        {
            var exitingState = GetState<T>();

            RelegateState(exitingState);
        }

        public void AddSubstate<T, TK>() where T : TBaseState where TK : TBaseState
        {
            var superState = GetState<T>();
            var subState = GetState<TK>();

            AddSubstate(superState, subState);
        }

        public IEnumerable<string> ToStringList()
        {
            return States.Select(state => state.GetType().Name);
        }

        public void PrintTree()
        {
            MonoBehaviour.print(_currentState.ToString());
        }

        public string GetTree()
        {
            return _currentState.ToString();
        }

        public string GetTree(TBaseState state)
        {
            return state.ToString();
        }

        public void PrintTree(TBaseState state)
        {
            MonoBehaviour.print(state.ToString());
        }

        //---------------------------------------PRIVATE-------------------------------------------------------------

        private void Start(TBaseState state)
        {
            state.Start(this);
            state.SubStates?.ForEach(Start);
        }

        private void FixedUpdate(TBaseState state)
        {
            state.FixedUpdate(this);

            state.SubStates?.ForEach(FixedUpdate);
        }

        private void LateUpdate(TBaseState state)
        {
            state.LateUpdate(this);

            state.SubStates?.ForEach(LateUpdate);
        }

        /// <summary>
        /// Recursively cuts down a branch starting from <paramref name="state"/> (inclusive).
        /// </summary>
        /// <param name="state">The state to start cutting from (inclusive).</param>
        /// <param name="forExchange">If this operation is a transaction (ignores <see cref="EnterState"/> and <see cref="ExitState"/> Calls)</param>
        private void CutSubstate(TBaseState state, bool forExchange = false)
        {
            DetachFromParent(state, forExchange);
            CutSubstates(state, forExchange);
        }

        /// <summary>
        /// Recursively cuts down a branch starting from <paramref name="superState"/> (exclusive).
        /// </summary>
        /// <param name="superState">The state to start cutting from (exclusive).</param>
        /// <param name="forExchange">If this operation is a transaction (ignores <see cref="EnterState"/> and <see cref="ExitState"/> Calls)</param>
        private void CutSubstates(TBaseState superState, bool forExchange = false)
        {
            var subStates = superState.SubStates.ToList();
            RemoveSubstates(superState, forExchange);
            subStates.ToList().ForEach(e => CutSubstate(e, forExchange));
        }

        /// <summary>
        /// Calls <see cref="CutSubstate"/> to recursively cut down a branch starting from a <paramref name="superState"/> (inclusive).
        /// </summary>
        /// <param name="superState">The state to start cutting from (inclusive).</param>
        /// <param name="forExchange">If this operation is a transaction (ignores <see cref="EnterState"/> and <see cref="ExitState"/> calls)</param>
        /// <returns>The previous substates of <paramref name="superState"/>.</returns>
        private List<TBaseState> PopSubstates(TBaseState superState, bool forExchange = false)
        {
            var res = superState.SubStates.ToList();
            CutSubstate(superState, forExchange);
            return res;
        }

        private TBaseState DetachFromParent(TBaseState state, bool forExchange = false)
        {
            var res = state.SuperState;
            RemoveSubstate(state.SuperState, state, forExchange);
            return res;
        }

        // private void DetachFromParent<T>(bool forExchange = false) where T : TBaseState
        // {
        //     var state = GetState<T>();
        //
        //     RemoveSubstate(state.SuperState, state, forExchange);
        // }
        //
        // private void CutTies(TBaseState state, bool forExchange = false)
        // {
        //     DetachFromParent(state, forExchange);
        //     RemoveSubstates(state, forExchange);
        // }

        private void AddSubstate(TBaseState superState, TBaseState subState, bool ignoreOnEnter = false)
        {
            DetachFromParent(subState, ignoreOnEnter);

            if (superState == null)
            {
                subState.SuperState = null;
                _currentState = subState;
            }
            else
            {
                subState.SuperState = superState;

                superState.SubStates.Add(subState);
            }


            if (ignoreOnEnter) return;
            EnterState(subState);
        }

        // private void AddSubstates(TBaseState superState, bool forExchange = false, params TBaseState[] subStates)
        // {
        //     subStates.ToList().ForEach(e => AddSubstate(superState, e, forExchange));
        // }

        private void RelegateState(TBaseState state)
        {
            var newParent = DetachFromParent(state);

            AddSubstates(newParent, PopSubstates(state, true));
        }

        private void AddSubstates(TBaseState superState, List<TBaseState> subStates, bool forExchange = false)
        {
            subStates.ForEach(e => AddSubstate(superState, e, forExchange));
        }

        private void RemoveSubstate(TBaseState superState, TBaseState subState, bool ignoreOnExit = false)
        {
            subState.SuperState = null;
            superState?.SubStates.Remove(subState);

            if (ignoreOnExit) return;
            ExitState(subState);
        }

        private void RemoveSubstates(TBaseState state, bool forExchange = false)
        {
            state.SubStates.ToList().ForEach(e => { RemoveSubstate(state, e, forExchange); });
        }

        private void EnterState(TBaseState state)
        {
            if (state.Active) return;

            state.Active = true;

            state.OnStateEnter(this);
            OnStateChange?.Invoke(state);
            OnStateEnter?.Invoke(state);
        }

        private void ExitState(TBaseState state)
        {
            if (!state.Active) return;

            state.Active = false;

            state.OnStateExit(this);
            OnStateChange?.Invoke(state);
            OnStateExit?.Invoke(state);
        }
    }
}
