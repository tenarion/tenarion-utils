using DesignPatterns.EasySM.GenericStateMachine;

namespace DesignPatterns.EasySM.UnityStateMachine
{
    public interface IStateMachineImplementation
    {
        public bool TryGetUnityStateMachine(out IUnityStateMachine unityStateMachine);
    }

    /// <summary>
    /// Interface for indicate that a class implements a state machine.
    /// </summary>
    /// <typeparam name="TStateMachine"></typeparam>
    /// <typeparam name="TBaseState"></typeparam>
    public interface IStateMachineImplementation<TStateMachine, TBaseState> : IStateMachineImplementation
        where TStateMachine : IStateMachine<TBaseState> where TBaseState : IState<TStateMachine, TBaseState>
    {
        public TStateMachine StateMachine { get; }

        bool IStateMachineImplementation.TryGetUnityStateMachine(out IUnityStateMachine unityStateMachine)
        {
            unityStateMachine = StateMachine as IUnityStateMachine;
            return unityStateMachine != null;
        }
    }
}
