using DesignPatterns.EasySM.GenericStateMachine;
using UnityEngine;

namespace DesignPatterns.EasySM.UnityStateMachine
{
    public interface IUnityState : IState {}
    public interface IUnityState<in TStateMachine, TBaseState> : IState<TStateMachine, TBaseState>, IUnityState
        where TStateMachine : IStateMachine<TBaseState>
        where TBaseState : IState<TStateMachine, TBaseState>
    {
        public void Start(TStateMachine stateMachine);
        public new void Update(TStateMachine stateMachine);
        public void FixedUpdate(TStateMachine stateMachine);
        public void LateUpdate(TStateMachine stateMachine);
        public void OnCollisionEnter(TStateMachine stateMachine, Collision other);
        public void OnCollisionStay(TStateMachine stateMachine, Collision other);
        public void OnCollisionExit(TStateMachine stateMachine, Collision other);
        public void OnTriggerEnter(TStateMachine stateMachine, Collider other);
        public void OnTriggerStay(TStateMachine stateMachine, Collider other);
        public void OnTriggerExit(TStateMachine stateMachine, Collider other);
    }
}
