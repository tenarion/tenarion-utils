using DesignPatterns.EasySM.GenericStateMachine;
using DesignPatterns.EasySM.UnityStateMachine;
using UnityEngine;

namespace DesignPatterns.EasySM.Helpers
{
    public class WaitForHierarchicalState<TBaseState, TWaitState> : CustomYieldInstruction
        where TBaseState : UnityHierarchicalState<TBaseState> where TWaitState : TBaseState
    {
        private readonly IStateMachine<TBaseState> _stateMachine;

        public WaitForHierarchicalState(IStateMachine<TBaseState> stateMachine)
        {
            _stateMachine = stateMachine;
        }

        public override bool keepWaiting => !_stateMachine.IsStateActive<TWaitState>();
    }

    public class WaitForFiniteState<TBaseState, TWaitState> : CustomYieldInstruction
        where TBaseState : UnityFiniteState<TBaseState> where TWaitState : TBaseState
    {
        private readonly IStateMachine<TBaseState> _stateMachine;

        public WaitForFiniteState(IStateMachine<TBaseState> stateMachine)
        {
            _stateMachine = stateMachine;
        }

        public override bool keepWaiting => !_stateMachine.IsStateActive<TWaitState>();
    }
}
