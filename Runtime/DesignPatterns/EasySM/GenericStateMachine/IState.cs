namespace DesignPatterns.EasySM.GenericStateMachine
{
    public interface IState
    {
    }

    /// <summary>
    /// Main interface for states. Used together with the <see cref="IStateMachine{T}"/> interface.
    /// </summary>
    /// <typeparam name="TStateMachine"></typeparam>
    /// <typeparam name="TBaseState"></typeparam>
    public interface IState<in TStateMachine, TBaseState> : IState
        where TStateMachine : IStateMachine<TBaseState>
        where TBaseState : IState<TStateMachine, TBaseState>
    {
        public void OnStateEnter(TStateMachine stateMachine);
        public void OnStateExit(TStateMachine stateMachine);
        public void Update(TStateMachine stateMachine);
    }
}
