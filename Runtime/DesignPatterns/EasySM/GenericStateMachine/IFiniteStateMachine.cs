namespace DesignPatterns.EasySM.GenericStateMachine
{
    /// <summary>
    /// Interface which can be extended to create custom Finite StateMachine implementations.
    /// </summary>
    /// <typeparam name="TBaseState">The type of states this StateMachine functions with.</typeparam>
    public interface IFiniteStateMachine<TBaseState> : IStateMachine<TBaseState> where TBaseState : IState
    {
        public void SetState<T>() where T : TBaseState;
    }
}
