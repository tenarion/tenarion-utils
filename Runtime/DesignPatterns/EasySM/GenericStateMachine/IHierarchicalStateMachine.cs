namespace DesignPatterns.EasySM.GenericStateMachine
{
    /// <summary>
    /// Interface which can be extended to create custom Hierarchical StateMachine implementations.
    /// </summary>
    /// <typeparam name="TBaseState">The type of states this StateMachine functions with.</typeparam>
    public interface IHierarchicalStateMachine<TBaseState> : IStateMachine<TBaseState>
        where TBaseState : IState
    {
        public void Update(TBaseState state);

        /// <summary>
        /// This method begins the chain of Update beginning on the current state.
        /// </summary>
        public void ChangeState<T,TK>() where T : TBaseState where TK : TBaseState;
    }
}
