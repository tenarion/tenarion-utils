using System;

namespace DesignPatterns.EasySM.GenericStateMachine
{
    /// <summary>
    /// Main interface for state machines. Used together with the <see cref="IState{TStateMachine, TBaseState}"/> interface.
    /// </summary>
    /// <typeparam name="TBaseState"></typeparam>
    public interface IStateMachine<TBaseState> where TBaseState : IState
    {
        public bool Active { get; set; }
        public TBaseState[] States { get; set; }
        public event Action<TBaseState> OnStateChange;
        public event Action<TBaseState> OnStateEnter;
        public event Action<TBaseState> OnStateExit;
        public event Action<TBaseState> OnStateUpdate;
        public event Action<TBaseState> OnStateFixedUpdate;
        public event Action<TBaseState> OnStateLateUpdate;
        public T GetState<T>() where T : TBaseState;
        public bool IsStateActive<T>() where T : TBaseState;
        public void Update();
    }
}
