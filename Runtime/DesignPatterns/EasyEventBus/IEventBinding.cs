using System;

namespace DesignPatterns.EasyEventBus
{
    /// <summary>
    /// Main interface for event bindings.
    /// Used together with the <see cref="EventBus{T}"/> static class and implemented in <see cref="EventBinding{T}"/> class.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IEventBinding<T>
    {
        public Action<T> OnEvent { get; set; }
        public Action OnEventNoArgs { get; set; }
    }
}