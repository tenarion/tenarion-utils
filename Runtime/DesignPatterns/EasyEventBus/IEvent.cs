namespace DesignPatterns.EasyEventBus
{
    /// <summary>
    /// Main interface for events.
    /// Used together with the <see cref="EventBus{T}"/> static class and <see cref="EventBinding{T}"/> class.
    /// </summary>
    public interface IEvent
    {
    }
}