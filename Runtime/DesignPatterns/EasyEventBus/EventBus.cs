using System.Collections.Generic;
using UnityEngine;

namespace DesignPatterns.EasyEventBus
{
    /// <summary>
    /// A simple static event bus implementation. Used together with the <see cref="EventBinding{T}"/> class.
    /// This class can be used to raise events of type <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T">The type of the event.</typeparam>
    public static class EventBus<T> where T : IEvent
    {
        private static readonly HashSet<IEventBinding<T>> Bindings = new();

        /// <summary>
        /// Registers a new event binding. Used to subscribe to events of type <typeparamref name="T"/>.
        /// </summary>
        /// <param name="binding">The registered binding.</param>
        public static void Register(EventBinding<T> binding)
        {
            Bindings.Add(binding);
        }

        /// <summary>
        /// Unregisters an event binding. Used to unsubscribe from events of type <typeparamref name="T"/>.
        /// </summary>
        /// <param name="binding">The binding to unregister.</param>
        public static void Unregister(EventBinding<T> binding)
        {
            Bindings.Remove(binding);
        }

        /// <summary>
        /// Raises an event of type <typeparamref name="T"/>, invoking it on all registered bindings where the event is contained.
        /// </summary>
        /// <param name="event"></param>
        public static void Raise(T @event)
        {
            foreach (var binding in Bindings)
            {
                binding.OnEvent.Invoke(@event);
                binding.OnEventNoArgs.Invoke();
            }
        }

        /// <summary>
        /// Clears all event bindings of type <typeparamref name="T"/>.
        /// </summary>
        public static void Clear()
        {
            Debug.Log($"Clearing {typeof(T).Name} bindings...");
            Bindings.Clear();
        }
    }
}