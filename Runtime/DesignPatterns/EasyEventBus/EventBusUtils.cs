using System;
using System.Collections.Generic;
using System.Linq;
using Core.Helpers;
using UnityEditor;
using UnityEngine;

namespace DesignPatterns.EasyEventBus
{
    /// <summary>
    /// Utils class for EventBus.
    /// </summary>
    public static class EventBusUtils
    {
        public static IReadOnlyList<Type> EventTypes { get; set; }
        public static IReadOnlyList<Type> EventBusTypes { get; set; }

#if UNITY_EDITOR
        public static PlayModeStateChange PlayModeState { get; set; }

        [InitializeOnLoadMethod]
        public static void InitializeEditor()
        {
            EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
        }

        private static void OnPlayModeStateChanged(PlayModeStateChange state)
        {
            PlayModeState = state;
            if (state == PlayModeStateChange.ExitingPlayMode) ClearAllBuses();
        }
#endif

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Initialize()
        {
            EventTypes = AssemblyHelper.GetTypes(typeof(IEvent)).ToList();
            EventBusTypes = InitializeAllBuses();
        }

        private static List<Type> InitializeAllBuses()
        {
            List<Type> eventBusTypes = new();

            var typeDef = typeof(EventBus<>);
            foreach (var eventType in EventTypes)
            {
                var busType = typeDef.MakeGenericType(eventType);
                eventBusTypes.Add(busType);
                Debug.Log($"Initialized EventBus<{eventType.Name}>");
            }

            return eventBusTypes;
        }

        public static void ClearAllBuses()
        {
            Debug.Log($"Clearing all buses...");
            foreach (var busType in EventBusTypes)
                if (ReflectionHelper.TryGetMethod(busType, "Clear", out var clearMethod))
                    clearMethod.Invoke(null, null);
        }
    }
}