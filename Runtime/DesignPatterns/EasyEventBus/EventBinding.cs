using System;

namespace DesignPatterns.EasyEventBus
{
    /// <summary>
    /// An <see cref="IEventBinding{T}"/> implementation that can be raised with or without parameters.
    /// Used together with the <see cref="EventBus{T}"/> static class.
    /// </summary>
    /// <typeparam name="T">The type of the event.</typeparam>
    public class EventBinding<T> : IEventBinding<T> where T : IEvent
    {
        private Action<T> _onEvent = _ => { };
        private Action _onEventNoArgs = () => { };

        Action<T> IEventBinding<T>.OnEvent
        {
            get => _onEvent;
            set => _onEvent = value;
        }

        Action IEventBinding<T>.OnEventNoArgs
        {
            get => _onEventNoArgs;
            set => _onEventNoArgs = value;
        }

        public EventBinding(Action<T> onEvent)
        {
            _onEvent = onEvent;
        }

        public EventBinding(Action onEventNoArgs)
        {
            _onEventNoArgs = onEventNoArgs;
        }

        public void Add(Action onEventNoArgs)
        {
            _onEventNoArgs += onEventNoArgs;
        }

        public void Remove(Action onEventNoArgs)
        {
            _onEventNoArgs -= onEventNoArgs;
        }

        public void Add(Action<T> onEvent)
        {
            _onEvent += onEvent;
        }

        public void Remove(Action<T> onEvent)
        {
            _onEvent -= onEvent;
        }
    }
}
