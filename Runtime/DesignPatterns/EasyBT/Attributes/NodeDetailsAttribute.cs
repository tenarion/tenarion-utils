using System;
using UnityEngine;

namespace DesignPatterns.EasyBT.Attributes
{
    /// <summary>
    /// Attribute to provide details about a node that will be used in the editor.
    /// </summary>
    public class NodeDetailsAttribute : Attribute
    {
        public string Name;
        public string CategoryPath;
        public string Description;
        public Color Color;
        public string IconPath;

        public NodeDetailsAttribute(string name = null, string categoryPath = null, string description = null,
            int color = 0x000000, string iconPath = null)
        {
            Name = name;
            CategoryPath = categoryPath;
            Color = color == 0x000000
                ? new Color32()
                : new Color32(
                    (byte)(color >> 16 & 0xFF),
                    (byte)(color >> 8 & 0xFF),
                    (byte)(color & 0xFF),
                    0xFF);
            Description = description;
            IconPath = iconPath;
        }
    }
}
