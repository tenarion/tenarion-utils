using UnityEngine;

namespace DesignPatterns.EasyBT
{
    [System.Serializable]
    public abstract class DecoratorNode : Node
    {
        [HideInInspector] [SerializeReference] public Node Child;
    }
}
