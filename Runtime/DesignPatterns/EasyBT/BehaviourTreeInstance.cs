using System.Collections.Generic;
using DesignPatterns.EasyBT.Actions;
using Unity.Profiling;
using UnityEngine;

namespace DesignPatterns.EasyBT
{
    [AddComponentMenu("Easy Utils/Behaviour Tree Instance")]
    public class BehaviourTreeInstance : MonoBehaviour
    {
        public enum TickModeEnum
        {
            None, // Use this update method to manually update the tree by calling ManualTick()
            FixedUpdate,
            Update,
            LateUpdate
        };

        public enum StartModeEnum
        {
            None, // Use this start method to manually start the tree by calling StartBehaviour()
            OnEnable,
            OnAwake,
            OnStart
        };

        // The main behaviour tree asset
        [Tooltip("BehaviourTree asset to instantiate during Awake")]
        public BehaviourTree BehaviourTree;

        [Tooltip("When to update this behaviour tree in the frame")]
        public TickModeEnum TickMode = TickModeEnum.Update;

        [Tooltip("When to start this behaviour tree")]
        public StartModeEnum StartMode = StartModeEnum.OnStart;

        [Tooltip("Run behaviour tree validation at startup (Can be disabled for release)")]
        public bool Validate = true;

        [Tooltip("Override / set blackboard key values for this behaviour tree instance")]
        public List<BlackboardKeyValuePair> BlackboardOverrides = new();

        public BehaviourTree RuntimeTree
        {
            get
            {
                if (_runtimeTree != null)
                    return _runtimeTree;
                else
                    return BehaviourTree;
            }
        }

        // Runtime tree instance
        private BehaviourTree _runtimeTree;

        // Storage container object to hold game object subsystems
        private Context _context;

        // Profile markers
        private static readonly ProfilerMarker ProfileUpdate = new("BehaviourTreeInstance.Update");

        // Tree state from last tick
        private Node.State _treeState = Node.State.Running;

        private bool _canUpdate;

        private void OnEnable()
        {
            if (StartMode == StartModeEnum.OnEnable) StartBehaviour(BehaviourTree);
        }

        private void Awake()
        {
            if (StartMode == StartModeEnum.OnAwake) StartBehaviour(BehaviourTree);
        }

        private void Start()
        {
            if (StartMode == StartModeEnum.OnStart) StartBehaviour(BehaviourTree);
        }

        private void ApplyBlackboardOverrides()
        {
            foreach (var pair in BlackboardOverrides)
            {
                // Find the key from the new behaviour tree instance
                var targetKey = _runtimeTree.Blackboard.Find(pair.Key.Name);
                var sourceKey = pair.Value;
                if (targetKey != null && sourceKey != null) targetKey.CopyFrom(sourceKey);
            }
        }

        private void InternalUpdate(float tickDelta)
        {
            if (!_runtimeTree || !_canUpdate) return;
            if (_treeState != Node.State.Running)
            {
                Debug.LogWarning(
                    $"[EASY BT] Behaviour tree {name} finished with a state of {_treeState.ToString().ToUpper()}. Call StartBehaviour() to restart this tree.");
                _canUpdate = false;
                return;
            }

            ProfileUpdate.Begin();
            _treeState = _runtimeTree.Tick(tickDelta);
            ProfileUpdate.End();
        }

        private void FixedUpdate()
        {
            if (TickMode == TickModeEnum.FixedUpdate) InternalUpdate(Time.fixedDeltaTime);
        }

        private void Update()
        {
            if (TickMode == TickModeEnum.Update) InternalUpdate(Time.deltaTime);
        }

        private void LateUpdate()
        {
            if (TickMode == TickModeEnum.LateUpdate) InternalUpdate(Time.deltaTime);
        }

        public void ManualTick(float tickDelta)
        {
            if (TickMode != TickModeEnum.None)
                Debug.LogWarning(
                    $"[EASY BT] Manually ticking the behaviour tree while in {TickMode} will cause duplicate updates");
            InternalUpdate(tickDelta);
        }

        public void StartBehaviour(BehaviourTree tree)
        {
            _treeState = Node.State.Running;
            var isValid = ValidateTree(tree);
            if (isValid)
            {
                InstantiateTree(tree);
                _canUpdate = true;
            }
            else
            {
                _runtimeTree = null;
            }
        }

        public void InstantiateTree(BehaviourTree tree)
        {
            _context = CreateBehaviourTreeContext();
            _runtimeTree = tree.Clone();
            _runtimeTree.Bind(_context);
            ApplyBlackboardOverrides();
        }

        private Context CreateBehaviourTreeContext()
        {
            return Context.CreateFromGameObject(gameObject);
        }

        private bool ValidateTree(BehaviourTree tree)
        {
            if (!tree)
            {
                Debug.LogWarning($"No BehaviourTree assigned to {name}, assign a behaviour tree in the inspector");
                return false;
            }

            var isValid = true;
            if (Validate)
            {
                string cyclePath;
                isValid = !IsRecursive(tree, out cyclePath);

                if (!isValid) Debug.LogError($"Failed to create recursive behaviour tree. Found cycle at: {cyclePath}");
            }

            return isValid;
        }

        private bool IsRecursive(BehaviourTree tree, out string cycle)
        {
            // Check if any of the subtree nodes and their decendents form a circular reference, which will cause a stack overflow.
            var treeStack = new List<string>();
            var referencedTrees = new HashSet<BehaviourTree>();

            var cycleFound = false;
            var cyclePath = "";

            System.Action<Node> traverse = null;
            traverse = node =>
            {
                if (!cycleFound)
                    if (node is SubTree subtree && subtree.TreeAsset != null)
                    {
                        treeStack.Add(subtree.TreeAsset.name);
                        if (referencedTrees.Contains(subtree.TreeAsset))
                        {
                            var index = 0;
                            foreach (var tree in treeStack)
                            {
                                index++;
                                if (index == treeStack.Count)
                                    cyclePath += $"{tree}";
                                else
                                    cyclePath += $"{tree} -> ";
                            }

                            cycleFound = true;
                        }
                        else
                        {
                            referencedTrees.Add(subtree.TreeAsset);
                            BehaviourTree.Traverse(subtree.TreeAsset.RootNode, traverse);
                            referencedTrees.Remove(subtree.TreeAsset);
                        }

                        treeStack.RemoveAt(treeStack.Count - 1);
                    }
            };
            treeStack.Add(tree.name);

            referencedTrees.Add(tree);
            BehaviourTree.Traverse(tree.RootNode, traverse);
            referencedTrees.Remove(tree);

            treeStack.RemoveAt(treeStack.Count - 1);
            cycle = cyclePath;
            return cycleFound;
        }

        private void OnDrawGizmosSelected()
        {
            if (!Application.isPlaying) return;

            if (!_runtimeTree) return;

            BehaviourTree.Traverse(_runtimeTree.RootNode, n =>
            {
                if (n.DrawGizmos) n.OnDrawGizmos();
            });
        }

        public BlackboardKey<T> FindBlackboardKey<T>(string keyName)
        {
            if (_runtimeTree) return _runtimeTree.Blackboard.Find<T>(keyName);
            return null;
        }

        public void SetBlackboardValue<T>(string keyName, T value)
        {
            if (_runtimeTree) _runtimeTree.Blackboard.SetValue(keyName, value);
        }

        public T GetBlackboardValue<T>(string keyName)
        {
            if (_runtimeTree) return _runtimeTree.Blackboard.GetValue<T>(keyName);
            return default;
        }
    }
}