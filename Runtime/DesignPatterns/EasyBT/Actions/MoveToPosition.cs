using DesignPatterns.EasyBT.Attributes;
using UnityEngine;

namespace DesignPatterns.EasyBT.Actions
{
    [System.Serializable, NodeDetails(
         categoryPath: "Movement",
         description: "Moves the agent to a specified position."
     )]
    public class MoveToPosition : ActionNode
    {
        [Tooltip("How fast to move")] public NodeProperty<float> Speed = new() { DefaultValue = 5.0f };

        [Tooltip("Stop within this distance of the target")]
        public NodeProperty<float> StoppingDistance = new() { DefaultValue = 0.1f };

        [Tooltip("Updates the agents rotation along the path")]
        public NodeProperty<bool> UpdateRotation = new() { DefaultValue = true };

        [Tooltip("Maximum acceleration when following the path")]
        public NodeProperty<float> Acceleration = new() { DefaultValue = 40.0f };

        [Tooltip("Returns success when the remaining distance is less than this amount")]
        public NodeProperty<float> Tolerance = new() { DefaultValue = 1.0f };

        public PositionType Type;

        [Tooltip("Target Position")]
        public NodeProperty<Vector3> TargetPosition = new() { DefaultValue = Vector3.zero };

        [Tooltip("Target Transform")] public NodeProperty<Transform> TargetTransform = new();

        protected override void OnStart()
        {
            if (Context.Agent == null) return;

            Context.Agent.stoppingDistance = StoppingDistance.Value;
            Context.Agent.speed = Speed.Value;
            Context.Agent.destination =
                Type == PositionType.Vector ? TargetPosition.Value : TargetTransform.Value.position;
            Context.Agent.updateRotation = UpdateRotation.Value;
            Context.Agent.acceleration = Acceleration.Value;
            Context.Agent.isStopped = false;
        }

        protected override void OnStop()
        {
            if (Context.Agent == null)
                return;
            if (!Context.Agent.enabled)
                return;
            if (Context.Agent.pathPending) Context.Agent.ResetPath();
            if (Context.Agent.remainingDistance > Tolerance.Value) Context.Agent.isStopped = true;
        }

        protected override State OnUpdate()
        {
            if (Context.Agent == null)
            {
                Debug.Log($"Game object {Context.GameObject.name} is missing NavMeshAgent component");
                return State.Failure;
            }

            if (!Context.Agent.enabled)
            {
                Debug.Log($"NavMeshAgent component on {Context.GameObject.name} was disabled");
                return State.Failure;
            }

            if (Context.Agent.pathPending)
                return State.Running;

            if (Context.Agent.remainingDistance < Tolerance.Value)
                return State.Success;

            if (Context.Agent.pathStatus == UnityEngine.AI.NavMeshPathStatus.PathInvalid)
                return State.Failure;

            return State.Running;
        }

        public override void OnDrawGizmos()
        {
            var agent = Context.Agent;
            var transform = Context.Transform;

            // Current velocity
            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, transform.position + agent.velocity);

            // Desired velocity
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, transform.position + agent.desiredVelocity);

            // Current path
            Gizmos.color = Color.black;
            var agentPath = agent.path;
            var prevCorner = transform.position;
            foreach (var corner in agentPath.corners)
            {
                Gizmos.DrawLine(prevCorner, corner);
                Gizmos.DrawSphere(corner, 0.1f);
                prevCorner = corner;
            }
        }

        public enum PositionType
        {
            Vector,
            Transform
        }
    }
}
