using DesignPatterns.EasyBT.Attributes;

namespace DesignPatterns.EasyBT.Actions
{
    [System.Serializable, NodeDetails(
         categoryPath: "Blackboard",
         description: "Sets a value on the blackboard."
         )]
    public class SetProperty : ActionNode
    {
        public BlackboardKeyValuePair Pair;

        protected override void OnStart()
        {
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate()
        {
            Pair.WriteValue();
            return State.Success;
        }
    }
}
