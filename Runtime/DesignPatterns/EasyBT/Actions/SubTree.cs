using DesignPatterns.EasyBT.Attributes;
using UnityEngine;

namespace DesignPatterns.EasyBT.Actions
{
    [System.Serializable, NodeDetails(
         categoryPath: "Other",
         description: "Runs a behaviour tree as a subtree."
     )]
    public class SubTree : ActionNode
    {
        [Tooltip("Behaviour tree asset to run as a subtree")]
        public BehaviourTree TreeAsset;

        [HideInInspector] public BehaviourTree TreeInstance;

        public override void OnInit()
        {
            if (!TreeAsset) return;

            TreeInstance = TreeAsset.Clone();
            TreeInstance.Bind(Context);
        }

        protected override void OnStart()
        {
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate()
        {
            return TreeInstance ? TreeInstance.Tick(Context.TickDelta) : State.Failure;
        }
    }
}
