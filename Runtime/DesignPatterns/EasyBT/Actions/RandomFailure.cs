using DesignPatterns.EasyBT.Attributes;
using UnityEngine;

namespace DesignPatterns.EasyBT.Actions
{
    [System.Serializable, NodeDetails(
         categoryPath: "Random",
         color: 0xFF7400,
         description: "Returns success or failure randomly, based on a chance of failure."
     )]
    public class RandomFailure : ActionNode
    {
        [Range(0, 1)] public float ChanceOfFailure = 0.5f;

        protected override void OnStart()
        {
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate()
        {
            var value = Random.value;
            return value > ChanceOfFailure ? State.Failure : State.Success;
        }
    }
}
