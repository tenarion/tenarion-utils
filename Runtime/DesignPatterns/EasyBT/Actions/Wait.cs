using DesignPatterns.EasyBT.Attributes;
using UnityEngine;

namespace DesignPatterns.EasyBT.Actions
{
    [System.Serializable, NodeDetails(
         categoryPath: "Time",
         description: "Waits for a specified duration."
     )]
    public class Wait : ActionNode
    {
        public float Duration = 1;
        private float _startTime;

        protected override void OnStart()
        {
            _startTime = Time.time;
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate()
        {
            return Time.time - _startTime > Duration ? State.Success : State.Running;
        }
    }
}
