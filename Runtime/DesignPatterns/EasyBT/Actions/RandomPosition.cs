using DesignPatterns.EasyBT.Attributes;
using UnityEngine;

namespace DesignPatterns.EasyBT.Actions
{
    [System.Serializable, NodeDetails(
         categoryPath: "Random",
         color: 0xFF7400,
            description: "Generates a random position within a specified range."
         )]
    public class RandomPosition : ActionNode
    {
        [Tooltip("Minimum bounds to generate point")]
        public Vector3 Min = Vector2.one * -10;

        [Tooltip("Maximum bounds to generate point")]
        public Vector3 Max = Vector2.one * 10;

        [Tooltip("Blackboard key to write the result to")]
        public NodeProperty<Vector3> Result;

        protected override void OnStart()
        {
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate()
        {
            var pos = new Vector3();
            pos.x = Random.Range(Min.x, Max.x);
            pos.y = Random.Range(Min.y, Max.y);
            pos.z = Random.Range(Min.z, Max.z);
            Result.Value = pos;
            return State.Success;
        }
    }
}
