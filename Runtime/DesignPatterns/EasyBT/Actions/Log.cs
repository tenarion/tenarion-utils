using DesignPatterns.EasyBT.Attributes;
using UnityEngine;

namespace DesignPatterns.EasyBT.Actions
{
    [System.Serializable, NodeDetails(
         categoryPath: "Debug",
         description:"Logs a message to the console."
         )]
    public class Log : ActionNode
    {
        public NodeProperty<string> Message;

        protected override void OnStart()
        {
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate()
        {
            Debug.Log($"{Message.Value}");
            return State.Success;
        }
    }
}
