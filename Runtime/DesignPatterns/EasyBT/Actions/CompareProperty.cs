using DesignPatterns.EasyBT.Attributes;

namespace DesignPatterns.EasyBT.Actions
{
    [System.Serializable, NodeDetails(
         categoryPath: "Blackboard",
         description:"Compares a blackboard value with another value and returns success if they are equal. Otherwise, it returns failure."
         )]
    public class CompareProperty : ActionNode
    {
        public BlackboardKeyValuePair Pair;

        protected override void OnStart()
        {
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate()
        {
            var source = Pair.Value;
            var destination = Pair.Key;

            if (source != null && destination != null)
                if (destination.Equals(source))
                    return State.Success;

            return State.Failure;
        }
    }
}
