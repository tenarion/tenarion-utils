using DesignPatterns.EasyBT.Attributes;
using UnityEngine;

namespace DesignPatterns.EasyBT.Actions
{
    [System.Serializable, NodeDetails(
         categoryPath: "Random",
         color: 0xFF7400,
         description: "Generates a random number within a specified range."
     )]
    public class RandomNumber : ActionNode
    {
        [Tooltip("The type of number to generate.")]
        public NumberType Type;

        public float Min;
        public float Max = 1f;

        [Tooltip("The result of the random number generation.")]
        public NodeProperty<float> Result;

        protected override void OnStart()
        {
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate()
        {
            Result.Value = Type == NumberType.Int ? Random.Range((int)Min, (int)Max) : Random.Range(Min, Max);
            return State.Success;
        }

        public enum NumberType
        {
            Int,
            Float
        }
    }
}
