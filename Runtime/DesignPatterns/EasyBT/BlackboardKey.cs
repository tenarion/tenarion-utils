using System;
using UnityEngine;

namespace DesignPatterns.EasyBT
{
    [System.Serializable]
    public abstract class BlackboardKey : ISerializationCallbackReceiver, ICloneable
    {
        public string Name;
        public System.Type UnderlyingType;
        public string TypeName;

        public BlackboardKey(System.Type underlyingType)
        {
            UnderlyingType = underlyingType;
            TypeName = UnderlyingType.FullName;
        }

        public void OnBeforeSerialize()
        {
            TypeName = UnderlyingType.AssemblyQualifiedName;
        }

        public void OnAfterDeserialize()
        {
            UnderlyingType = System.Type.GetType(TypeName);
        }

        public abstract void CopyFrom(BlackboardKey key);
        public abstract bool Equals(BlackboardKey key);

        public static BlackboardKey CreateKey(System.Type type)
        {
            return System.Activator.CreateInstance(type) as BlackboardKey;
        }

        public object Clone()
        {
            var clone = (BlackboardKey)MemberwiseClone();
            clone.Name = string.Copy(Name);
            clone.UnderlyingType = UnderlyingType;
            clone.TypeName = string.Copy(TypeName);
            return clone;
        }
    }

    [System.Serializable]
    public abstract class BlackboardKey<T> : BlackboardKey
    {
        public T Value;

        public BlackboardKey() : base(typeof(T))
        {
        }

        public override string ToString()
        {
            return $"{Name} : {Value}";
        }

        public override void CopyFrom(BlackboardKey key)
        {
            if (key.UnderlyingType != UnderlyingType) return;

            var other = key as BlackboardKey<T>;
            Value = other.Value;
        }

        public override bool Equals(BlackboardKey key)
        {
            if (key.UnderlyingType != UnderlyingType) return false;

            var other = key as BlackboardKey<T>;
            return Value.Equals(other.Value);
        }
    }
}
