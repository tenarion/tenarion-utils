using Core.Attributes;
using UnityEngine;

namespace DesignPatterns.EasyBT
{
    [System.Serializable]
    public abstract class Node
    {
        public enum State
        {
            Running,
            Failure,
            Success
        }

        [HideInInspector] public State NodeState = State.Running;
        [HideInInspector] public bool Started;
        [HideInInspector] public string Guid = System.Guid.NewGuid().ToString();
        [HideInInspector] public Vector2 Position;
        [HideInInspector] public Context Context;
        [HideInInspector] public Blackboard Blackboard;
        [HideInInspector] public bool HasBreakpoint;

        [Tooltip("When enabled, the nodes OnDrawGizmos will be invoked.")]
        public bool DrawGizmos;

        public virtual void OnInit()
        {
            // Nothing to do here
        }

        public State Update()
        {
            if (!Started)
            {
                OnStart();
                Started = true;
            }

            NodeState = OnUpdate();

            Context.TickResults[Guid] = NodeState;

            if (NodeState != State.Running)
            {
                OnStop();
                Started = false;
            }

            if (HasBreakpoint) Debug.DebugBreak();

            return NodeState;
        }

        public void Abort()
        {
            BehaviourTree.Traverse(this, node =>
            {
                node.Started = false;
                node.OnStop();
            });
        }

        public virtual void OnDrawGizmos()
        {
        }

        protected abstract void OnStart();
        protected abstract void OnStop();
        protected abstract State OnUpdate();

        protected virtual void Log(string message)
        {
            Debug.Log($"[{GetType()}]{message}");
        }
    }
}
