using UnityEngine;

namespace DesignPatterns.EasyBT
{
    // This is a special type to represent a key binding, with a value.
    // The BlackboardKeyValuePairPropertyDrawer takes care of updating the rendered value field
    // when the key type changes.
    [System.Serializable]
    public class BlackboardKeyValuePair
    {
        [SerializeReference] public BlackboardKey Key;
        [SerializeReference] public BlackboardKey Value;

        public void WriteValue()
        {
            if (Key != null && Value != null) Key.CopyFrom(Value);
        }
    }
}
