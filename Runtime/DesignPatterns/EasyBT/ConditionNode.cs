namespace DesignPatterns.EasyBT
{
    [System.Serializable]
    public abstract class ConditionNode : ActionNode
    {
        public bool Invert;

        protected override void OnStart()
        {
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate()
        {
            var isTrue = CheckCondition();
            if (Invert) isTrue = !isTrue;
            return isTrue ? State.Success : State.Failure;
        }

        protected abstract bool CheckCondition();
    }
}