using DesignPatterns.EasyBT.Attributes;
using UnityEngine;

namespace DesignPatterns.EasyBT.Composites
{
    [System.Serializable, NodeDetails(
         categoryPath: "Random",
         iconPath: "Packages/com.tenarion.easyutils/Editor/EasyBT/Resources/node_condition.png",
         description: "Selects a random child to run."
     )]
    public class RandomSelector : CompositeNode
    {
        private int _lastChildIndex;

        protected override void OnStart()
        {
            _lastChildIndex = Random.Range(0, Children.Count);
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate() => Children.Count == 0 ? State.Failure : Children[_lastChildIndex].Update();
    }
}
