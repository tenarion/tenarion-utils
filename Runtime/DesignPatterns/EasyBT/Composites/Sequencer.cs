using DesignPatterns.EasyBT.Attributes;

namespace DesignPatterns.EasyBT.Composites
{
    [System.Serializable,
     NodeDetails(
         description:
         "The Sequencer node will return success if all children return success. If any child returns failure, the Sequencer will return failure.")]
    public class Sequencer : CompositeNode
    {
        public bool SaveLastChild = true;
        private int _lastChildIndex;

        protected override void OnStart()
        {
            _lastChildIndex = 0;
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate()
        {
            var start = SaveLastChild ? _lastChildIndex : 0;
            for (var i = start; i < Children.Count; i++)
            {
                var childStatus = Children[i].Update();
                if (childStatus == State.Success) continue;

                _lastChildIndex = childStatus == State.Running ? i : 0;
                return childStatus;
            }

            return State.Success;
        }
    }
}
