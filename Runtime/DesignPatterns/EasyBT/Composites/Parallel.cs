using DesignPatterns.EasyBT.Attributes;

namespace DesignPatterns.EasyBT.Composites
{
    [System.Serializable, NodeDetails(
         description: "Runs all children in parallel, and returns success if a certain number of children succeed."
     )]
    public class Parallel : CompositeNode
    {
        public int SuccessThreshold = 1;

        protected override void OnStart()
        {
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate()
        {
            var childCount = Children.Count;

            var successCount = 0;
            var failureCount = 0;

            for (var i = 0; i < childCount; ++i)
            {
                var childState = Children[i].Update();
                if (childState == State.Success)
                    successCount++;
                else if (childState == State.Failure) failureCount++;
            }

            if (successCount >= SuccessThreshold) return State.Success;

            if (failureCount > childCount - SuccessThreshold) return State.Failure;

            return State.Running;
        }
    }
}
