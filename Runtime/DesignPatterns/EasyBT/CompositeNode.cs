using System.Collections.Generic;
using UnityEngine;

namespace DesignPatterns.EasyBT
{
    [System.Serializable]
    public abstract class CompositeNode : Node
    {
        [HideInInspector] [SerializeReference] public List<Node> Children = new();
    }
}