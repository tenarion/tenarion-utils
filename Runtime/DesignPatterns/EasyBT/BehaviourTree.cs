using System.Collections.Generic;
using UnityEngine;

namespace DesignPatterns.EasyBT
{
    [CreateAssetMenu(menuName = "Easy Utils/Behaviour Tree")]
    public class BehaviourTree : ScriptableObject
    {
        [SerializeReference] public RootNode RootNode;

        [SerializeReference] public List<Node> Nodes = new();

        public Blackboard Blackboard = new();

        public Context TreeContext;

        #region EditorProperties

        public Vector3 ViewPosition = new(600, 300);
        public Vector3 ViewScale = Vector3.one;

        #endregion

        public BehaviourTree()
        {
            RootNode = new RootNode();
            Nodes.Add(RootNode);
        }

        private void OnEnable()
        {
            // Validate the behaviour tree on load, removing all null children
            Nodes.RemoveAll(node => node == null);
            Traverse(RootNode, node =>
            {
                if (node is CompositeNode composite) composite.Children.RemoveAll(child => child == null);
            });
        }

        public Node.State Tick(float tickDelta)
        {
            TreeContext.TickDelta = tickDelta;
            TreeContext.TickResults.Clear();
            return RootNode.Update();
        }

        public static List<Node> GetChildren(Node parent)
        {
            var children = new List<Node>();

            if (parent is DecoratorNode decorator && decorator.Child != null) children.Add(decorator.Child);

            if (parent is RootNode rootNode && rootNode.Child != null) children.Add(rootNode.Child);

            if (parent is CompositeNode composite) return composite.Children;

            return children;
        }

        public static void Traverse(Node node, System.Action<Node> visiter)
        {
            if (node != null)
            {
                visiter.Invoke(node);
                var children = GetChildren(node);
                children.ForEach(n => Traverse(n, visiter));
            }
        }

        public BehaviourTree Clone()
        {
            var tree = Instantiate(this);
            return tree;
        }

        public void Bind(Context context)
        {
            TreeContext = context;
            Traverse(RootNode, node =>
            {
                node.Context = context;
                node.Blackboard = Blackboard;
                node.OnInit();
            });
        }
    }
}
