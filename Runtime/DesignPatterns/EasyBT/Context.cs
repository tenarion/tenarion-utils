using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace DesignPatterns.EasyBT
{
    // The context is a storage object for sharing common data between nodes in the tree.
    // Useful for caching components, game objects, or other data that is used by multiple nodes.
    public class Context
    {
        public GameObject GameObject;
        public Transform Transform;
        public Animator Animator;
        public Rigidbody Physics;
        public NavMeshAgent Agent;
        public SphereCollider SphereCollider;
        public BoxCollider BoxCollider;
        public CapsuleCollider CapsuleCollider;
        public CharacterController CharacterController;
        public Dictionary<string, Node.State> TickResults;
        public float TickDelta;

        public static Context CreateFromGameObject(GameObject gameObject)
        {
            // Fetch all commonly used components
            var context = new Context();
            context.GameObject = gameObject;
            context.Transform = gameObject.transform;
            context.Animator = gameObject.GetComponentInChildren<Animator>();
            context.Physics = gameObject.GetComponent<Rigidbody>();
            context.Agent = gameObject.GetComponent<NavMeshAgent>();
            context.SphereCollider = gameObject.GetComponent<SphereCollider>();
            context.BoxCollider = gameObject.GetComponent<BoxCollider>();
            context.CapsuleCollider = gameObject.GetComponent<CapsuleCollider>();
            context.CharacterController = gameObject.GetComponent<CharacterController>();
            context.TickResults = new Dictionary<string, Node.State>();
            return context;
        }

        public T GetComponent<T>() where T : Component
        {
            return GameObject.GetComponent<T>();
        }
    }
}