using DesignPatterns.EasyBT.Attributes;
using UnityEngine;

namespace DesignPatterns.EasyBT.Decorators
{
    [System.Serializable, NodeDetails(
         description: "Repeats the subtree a specified number of times."
     )]
    public class Repeat : DecoratorNode
    {
        [Tooltip("Restarts the subtree on success")]
        public bool RestartOnSuccess = true;

        [Tooltip("Restarts the subtree on failure")]
        public bool RestartOnFailure;

        [Tooltip("Maximum number of times the subtree will be repeated. Set to 0 to loop forever")]
        public int MaxRepeats;

        int _iterationCount;

        protected override void OnStart()
        {
            _iterationCount = 0;
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate()
        {
            if (Child == null)
            {
                return State.Failure;
            }

            switch (Child.Update())
            {
                case State.Running:
                    break;
                case State.Failure:
                    if (RestartOnFailure)
                    {
                        _iterationCount++;
                        if (_iterationCount >= MaxRepeats && MaxRepeats > 0)
                        {
                            return State.Failure;
                        }

                        return State.Running;
                    }

                    return State.Failure;
                case State.Success:
                    if (RestartOnSuccess)
                    {
                        _iterationCount++;
                        if (_iterationCount >= MaxRepeats && MaxRepeats > 0)
                        {
                            return State.Success;
                        }

                        return State.Running;
                    }

                    return State.Success;
            }

            return State.Running;
        }
    }
}
