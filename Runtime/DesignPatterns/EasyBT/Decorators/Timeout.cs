using DesignPatterns.EasyBT.Attributes;
using UnityEngine;

namespace DesignPatterns.EasyBT.Decorators
{
    [System.Serializable, NodeDetails(
         categoryPath: "Time",
         description: "Waits for a specified duration. If it runs out of time, it returns failure."
     )]
    public class Timeout : DecoratorNode
    {
        public float Duration = 1.0f;
        private float _startTime;

        protected override void OnStart()
        {
            _startTime = Time.time;
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate()
        {
            if (Child == null)
                return State.Failure;

            return Time.time - _startTime > Duration ? State.Failure : Child.Update();
        }
    }
}
