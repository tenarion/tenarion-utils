using DesignPatterns.EasyBT.Attributes;

namespace DesignPatterns.EasyBT.Decorators
{
    [System.Serializable, NodeDetails(
         description: "Returns success if the child fails, and failure if the child succeeds."
     )]
    public class Inverter : DecoratorNode
    {
        protected override void OnStart()
        {
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate()
        {
            if (Child == null)
                return State.Failure;

            return Child.Update() switch
            {
                State.Running => State.Running,
                State.Failure => State.Success,
                State.Success => State.Failure,
                _ => State.Failure
            };
        }
    }
}
