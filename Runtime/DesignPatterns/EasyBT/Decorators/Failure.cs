using DesignPatterns.EasyBT.Attributes;

namespace DesignPatterns.EasyBT.Decorators
{
    [System.Serializable, NodeDetails(
         description: "Always returns failure."
     )]
    public class Failure : DecoratorNode
    {
        protected override void OnStart()
        {
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate()
        {
            if (Child == null)
                return State.Failure;

            var state = Child.Update();
            return state == State.Success ? State.Failure : state;
        }
    }
}
