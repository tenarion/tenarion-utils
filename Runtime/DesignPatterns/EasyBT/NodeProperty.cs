using UnityEngine;

namespace DesignPatterns.EasyBT
{
    [System.Serializable]
    public class NodeProperty
    {
        [SerializeReference] public BlackboardKey Reference;
    }

    [System.Serializable]
    public class NodeProperty<T> : NodeProperty
    {
        public T DefaultValue;
        private BlackboardKey<T> _typedKey;

        private BlackboardKey<T> TypedKey
        {
            get
            {
                if (_typedKey == null && Reference != null) _typedKey = Reference as BlackboardKey<T>;
                return _typedKey;
            }
        }

        public T Value
        {
            set
            {
                if (TypedKey != null)
                    TypedKey.Value = value;
                else
                    DefaultValue = value;
            }
            get => TypedKey != null ? TypedKey.Value : DefaultValue;
        }

        public static implicit operator T(NodeProperty<T> property) => property.Value;
    }
}
