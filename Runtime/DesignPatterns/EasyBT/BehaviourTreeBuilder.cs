using System;
using System.Linq;
using DesignPatterns.EasyBT.Composites;
using UnityEditor;
using UnityEngine;

namespace DesignPatterns.EasyBT
{
    // This is a WIP class for generating behaviour trees dynamically at runtime.
    public class BehaviourTreeBuilder
    {
        public BehaviourTree Tree;

        public BehaviourTreeBuilder(string treeName)
        {
            Tree = ScriptableObject.CreateInstance<BehaviourTree>();
            Tree.name = treeName;
        }

        public T CreateNode<T>(params object[] args) where T : Node
        {
            var newNode = Activator.CreateInstance(typeof(T), args) as T;
            newNode.Guid = Guid.NewGuid().ToString();
            Tree.Nodes.Add(newNode);
            return newNode;
        }

        public void Selector(params Node[] nodes)
        {
            var selector = CreateNode<Selector>();
            selector.Children.AddRange(nodes);
            Tree.Nodes.AddRange(nodes);
        }

        private void LayoutNodes()
        {
            CalculatePositions(Tree.RootNode, Tree.RootNode.Position, 80.0f);
        }

        private float CalculatePositions(Node node, Vector2 position, float verticalSpacing)
        {
            node.Position = position;
            var currentX = position.x;

            var children = BehaviourTree.GetChildren(node);
            foreach (var child in children)
            {
                currentX += CalculateTreeWidth(child) / 2.0f;
                CalculatePositions(child, new Vector2(currentX, position.y + verticalSpacing), verticalSpacing);
                currentX += CalculateTreeWidth(child) / 2.0f;
            }

            return currentX;
        }

        private float CalculateTreeWidth(Node node)
        {
            var children = BehaviourTree.GetChildren(node);
            if (children.Count == 0) return 100.0f;

            var totalWidth = children.Sum(c => CalculateTreeWidth(c));
            return totalWidth;
        }

#if UNITY_EDITOR
        public void Save(string path)
        {
            if (Tree)
            {
                LayoutNodes();
                AssetDatabase.CreateAsset(Tree, $"{path}/{Tree.name}.asset");
                AssetDatabase.SaveAssets();
            }
        }
#endif
    }
}