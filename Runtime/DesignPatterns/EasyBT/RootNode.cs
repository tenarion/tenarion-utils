using DesignPatterns.EasyBT.Attributes;
using UnityEngine;

namespace DesignPatterns.EasyBT
{
    [System.Serializable, NodeDetails(description: "The root node of the behavior tree.")]
    public class RootNode : Node
    {
        [SerializeReference] [HideInInspector] public Node Child;

        protected override void OnStart()
        {
        }

        protected override void OnStop()
        {
        }

        protected override State OnUpdate()
        {
            return Child?.Update() ?? State.Failure;
        }
    }
}
