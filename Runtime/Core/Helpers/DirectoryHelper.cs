namespace Core.Helpers
{
    public static class DirectoryHelper
    {
        public static void CreateFolderHierarchy(string path)
        {
            var folders = path.Split('/');
            var currentPath = "Assets";
            foreach (var folder in folders)
            {
                if (!System.IO.Directory.Exists($"{currentPath}/{folder}"))
                    System.IO.Directory.CreateDirectory($"{currentPath}/{folder}");
                currentPath += $"/{folder}";
            }
        }

        public static void CreateFolderHierarchyIfNotExists(string path)
        {
            if (!System.IO.Directory.Exists(path))
                CreateFolderHierarchy(path);
        }
    }
}
