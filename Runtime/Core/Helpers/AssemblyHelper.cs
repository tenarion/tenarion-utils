using System;
using System.Collections.Generic;

namespace Core.Helpers
{
    public class AssemblyHelper
    {
        /// <summary>
        /// Enum that defines the specific predefined types of assemblies for navigation.
        /// </summary>
        private enum AssemblyType
        {
            AssemblyCSharp,
            AssemblyCSharpEditor,
            AssemblyCSharpEditorFirstPass,
            AssemblyCSharpFirstPass,
            AssemblyEasyUtils
        }

        /// <summary>
        /// Maps the assembly name to the corresponding AssemblyType.
        /// </summary>
        /// <param name="assemblyName">Name of the assembly.</param>
        /// <returns>AssemblyType corresponding to the assembly name, null if no match.</returns>
        private static AssemblyType? GetAssemblyType(string assemblyName)
        {
            return assemblyName switch
            {
                "Assembly-CSharp" => AssemblyType.AssemblyCSharp,
                "Assembly-CSharp-Editor" => AssemblyType.AssemblyCSharpEditor,
                "Assembly-CSharp-Editor-firstpass" => AssemblyType.AssemblyCSharpEditorFirstPass,
                "Assembly-CSharp-firstpass" => AssemblyType.AssemblyCSharpFirstPass,
                "Tenarion.EasyUtils" => AssemblyType.AssemblyEasyUtils,
                _ => null
            };
        }

        /// <summary>
        /// Method looks through a given assembly and adds types that fulfill a certain interface to the provided collection.
        /// </summary>
        /// <param name="assemblyTypes">Array of Type objects representing all the types in the assembly.</param>
        /// <param name="interfaceType">Type representing the interface to be checked against.</param>
        /// <param name="results">Collection of types where result should be added.</param>
        private static void AddTypesFromAssembly(Type[] assemblyTypes, Type interfaceType, ICollection<Type> results)
        {
            if (assemblyTypes == null) return;
            foreach (var type in assemblyTypes)
                if (type != interfaceType && interfaceType.IsAssignableFrom(type))
                    results.Add(type);
        }

        /// <summary>
        /// Gets all Types from all assemblies in the current AppDomain that implement the provided class or interface type.
        /// </summary>
        /// <param name="parentType">Parent type to get all the Types for.</param>
        /// <returns>List of Types implementing the provided type.</returns>
        public static IEnumerable<Type> GetTypes(Type parentType)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            var assemblyTypes = new Dictionary<AssemblyType, Type[]>();
            var types = new List<Type>();
            foreach (var assembly in assemblies)
            {
                var assemblyType = GetAssemblyType(assembly.GetName().Name);
                if (assemblyType == null) continue;
                assemblyTypes.Add((AssemblyType)assemblyType, assembly.GetTypes());
            }

            assemblyTypes.TryGetValue(AssemblyType.AssemblyCSharp, out var assemblyCSharpTypes);
            AddTypesFromAssembly(assemblyCSharpTypes, parentType, types);

            assemblyTypes.TryGetValue(AssemblyType.AssemblyCSharpFirstPass, out var assemblyCSharpFirstPassTypes);
            AddTypesFromAssembly(assemblyCSharpFirstPassTypes, parentType, types);

            assemblyTypes.TryGetValue(AssemblyType.AssemblyEasyUtils, out var assemblyEasyUtilsTypes);
            AddTypesFromAssembly(assemblyEasyUtilsTypes, parentType, types);

            return types;
        }
    }
}