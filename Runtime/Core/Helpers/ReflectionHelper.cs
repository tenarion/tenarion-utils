using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Core.Helpers
{
    public static class ReflectionHelper
    {
        public static T[] CreateChildInstances<T>(params object[] args)
        {
            var childTypes = GetChildTypes<T>();
            return childTypes.Select(childType =>
            {
                var constructors = childType.GetConstructors();

                var selectedConstructor = SelectConstructor(constructors, args);

                if (selectedConstructor == null)
                    // Handle the case where no suitable constructor is found.
                    throw new InvalidOperationException($"No suitable constructor found for {childType.Name}");


                var constructorParameters = selectedConstructor.GetParameters();

                var availableArgs = constructorParameters
                    .Select(parameter => Array.Find(args, arg => arg.GetType() == parameter.ParameterType))
                    .ToArray();

                return (T)Activator.CreateInstance(childType, availableArgs);
            }).ToArray();
        }

        public static bool TryGetMethod(Type type, string name, out MethodInfo methodInfo)
        {
            methodInfo = type.GetMethod(name);
            return methodInfo != null;
        }

        public static object CallGenericMethod(object targetObj, string genericMethodName, Type genericType,
            params object[] args)
        {
            if (targetObj == null)
            {
                Debug.LogError("Target obj is null!");
                return null;
            }

            if (!TryGetMethod(targetObj.GetType(), genericMethodName, out var methodInfo)) return null;
            var genericMethod = methodInfo.MakeGenericMethod(genericType);
            return genericMethod.Invoke(targetObj, args);
        }


        public static bool TryGetMethod<T>(string name, out MethodInfo methodInfo)
        {
            return TryGetMethod(typeof(T), name, out methodInfo);
        }

        public static Type FindYoungestChild(Type parentType)
        {
            var allTypes = AssemblyHelper.GetTypes(parentType);

            var youngestChild = allTypes
                .OrderByDescending(type => GetInheritanceDepth(parentType, type))
                .FirstOrDefault();

            return youngestChild;
        }

        private static int GetInheritanceDepth(Type parentType, Type targetType)
        {
            var depth = 0;
            var currentType = targetType;

            while (currentType != parentType && currentType.BaseType != null)
            {
                currentType = currentType.BaseType;
                depth++;
            }

            return depth;
        }

        public static IEnumerable<Type> GetChildTypes<T>()
        {
            return GetChildTypes(typeof(T));
        }

        public static IEnumerable<Type> GetChildTypes(Type type)
        {
            var childTypes = AssemblyHelper.GetTypes(type);
            return childTypes;
        }

        private static ConstructorInfo SelectConstructor(ConstructorInfo[] constructors, object[] args)
        {
            return constructors
                .OrderByDescending(constructor =>
                    constructor.GetParameters().Count(p => args.Any(arg => p.ParameterType == arg.GetType())))
                .FirstOrDefault();
        }
    }
}
