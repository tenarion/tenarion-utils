using UnityEditor;

namespace Core.Helpers
{
    public static class AssetDatabaseHelper
    {
        /// <summary>
        /// Creates a folder hierarchy in the Assets folder. The path should be relative to the Assets folder.
        /// </summary>
        /// <param name="path"></param>
        public static void CreateFolderHierarchy(string path)
        {
            var folders = path.Split('/');
            var currentPath = "Assets";
            foreach (var folder in folders)
            {
                if (!AssetDatabase.IsValidFolder($"{currentPath}/{folder}"))
                    AssetDatabase.CreateFolder(currentPath, folder);
                currentPath += $"/{folder}";
            }
        }

        /// <summary>
        /// Creates a folder hierarchy in the Assets folder if it does not already exist. The path should be relative to the Assets folder.
        /// </summary>
        /// <param name="path"></param>
        public static void CreateFolderHierarchyIfNotExists(string path)
        {
            if (!AssetDatabase.IsValidFolder(path))
                CreateFolderHierarchy(path);
        }
    }
}
