using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Helpers
{
    public class WeightedRandomPicker<T>
    {
        private readonly List<T> _items = new();
        private readonly List<int> _weights = new();
        private readonly Random _random = new();

        public void AddItem(T item, int weight)
        {
            if (weight < 0)
                throw new ArgumentException("Weight cannot be negative.", nameof(weight));

            _items.Add(item);
            _weights.Add(weight);
        }

        public T GetRandomItem()
        {
            if (_items.Count == 0)
                throw new InvalidOperationException("No items added to the picker.");

            var totalWeight = _weights.Sum();
            var randomValue = _random.Next(totalWeight);

            var weightSum = 0;
            for (var i = 0; i < _items.Count; i++)
            {
                weightSum += _weights[i];
                if (randomValue < weightSum) return _items[i];
            }

            // This should not be reached, but just in case.
            throw new InvalidOperationException("Failed to pick a random item.");
        }
    }
}