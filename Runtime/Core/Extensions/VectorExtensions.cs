using UnityEngine;

namespace Core.Extensions
{
    public static class VectorExtensions
    {
        public static Vector3 Direction(Vector3 a, Vector3 b)
        {
            return (b - a).normalized;
        }

        public static Vector2 Direction(Vector2 a, Vector2 b)
        {
            return (b - a).normalized;
        }
    }
}