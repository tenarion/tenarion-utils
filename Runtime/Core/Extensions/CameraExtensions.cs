#if DOTWEEN_AVAILABLE
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace Core.Extensions
{
    public static class CameraExtensions
    {
        private static TweenerCore<float, float, FloatOptions> camTween;

        public static void DoFov(this Camera cam, float targetFov, float transitionTime)
        {
            camTween?.Kill();
            camTween = cam.DOFieldOfView(targetFov, transitionTime).SetEase(Ease.OutQuad);
        }
    }

}
#endif
