using System.Text.RegularExpressions;

namespace Core.Extensions
{
    public static class StringExtensions
    {
        public static string WithoutBreakLines(this string str, bool keepSpacing = true)
        {
            return Regex.Replace(str, @"\r\n?|\n", keepSpacing ? " " : "");
        }

        public static string ToCamelCase(this string str)
        {
            if (string.IsNullOrEmpty(str)) return str;

            var words = SplitString(str);

            for (var i = 1; i < words.Length; i++)
                if (words[i].Length > 0)
                    words[i] = char.ToUpperInvariant(words[i][0]) + words[i][1..].ToLowerInvariant();

            words[0] = words[0].ToLowerInvariant();

            return string.Join("", words);
        }

        public static string ToPascalCase(this string str)
        {
            if (string.IsNullOrEmpty(str)) return str;

            var words = SplitString(str);

            for (var i = 0; i < words.Length; i++)
                if (words[i].Length > 0)
                    words[i] = char.ToUpperInvariant(words[i][0]) + words[i][1..].ToLowerInvariant();

            return string.Join("", words);
        }

        public static string ToSnakeCase(this string str)
        {
            if (string.IsNullOrEmpty(str)) return str;

            var words = SplitString(str);

            for (var i = 0; i < words.Length; i++)
                if (words[i].Length > 0)
                    words[i] = words[i].ToLowerInvariant();

            return string.Join("_", words);
        }

        public static string ToKebabCase(this string str)
        {
            if (string.IsNullOrEmpty(str)) return str;

            var words = SplitString(str);

            for (var i = 0; i < words.Length; i++)
                if (words[i].Length > 0)
                    words[i] = words[i].ToLowerInvariant();

            return string.Join("-", words);
        }

        public static string ToSentenceCase(this string str)
        {
            if (string.IsNullOrEmpty(str)) return str;

            var words = SplitString(str);

            for (var i = 0; i < words.Length; i++)
                if (words[i].Length > 0)
                    words[i] = char.ToUpperInvariant(words[i][0]) + words[i][1..].ToLowerInvariant();

            return string.Join(" ", words);
        }

        public static string ToTitleCase(this string str)
        {
            if (string.IsNullOrEmpty(str)) return str;

            var words = SplitString(str);

            for (var i = 0; i < words.Length; i++)
                if (words[i].Length > 0)
                    words[i] = char.ToUpperInvariant(words[i][0]) + words[i][1..];

            return string.Join(" ", words);
        }

        private static string[] SplitString(string str)
        {
            return Regex.Split(str, @"(?<!^)(?=[A-Z])|_| ");
        }
    }
}
