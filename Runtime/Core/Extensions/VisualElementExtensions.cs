using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace Core.Extensions
{
    public static class VisualElementExtensions
    {
        public static void Show(this VisualElement element)
        {
            element.style.display = DisplayStyle.Flex;
        }

        public static void Hide(this VisualElement element)
        {
            element.style.display = DisplayStyle.None;
        }

        public static void SetVisibilityByCondition(this VisualElement root, bool condition,
            IEnumerable<VisualElement> fieldsToShow,
            IEnumerable<VisualElement> fieldsToHide)
        {
            if (condition)
            {
                foreach (var field in fieldsToShow) field.Show();
                foreach (var field in fieldsToHide) field.Hide();
            }
            else
            {
                foreach (var field in fieldsToShow) field.Hide();
                foreach (var field in fieldsToHide) field.Show();
            }
        }

        public static void SetMargin(this VisualElement element, int top, int right, int bottom, int left)
        {
            element.style.marginTop = top;
            element.style.marginRight = right;
            element.style.marginBottom = bottom;
            element.style.marginLeft = left;
        }

        public static void SetMargin(this VisualElement element, int margin)
        {
            element.SetMargin(margin, margin, margin, margin);
        }

        public static void SetPadding(this VisualElement element, int top, int right, int bottom, int left)
        {
            element.style.paddingTop = top;
            element.style.paddingRight = right;
            element.style.paddingBottom = bottom;
            element.style.paddingLeft = left;
        }

        public static void SetPadding(this VisualElement element, int padding)
        {
            element.SetPadding(padding, padding, padding, padding);
        }

        public static void SetFlex(this VisualElement element, int grow, int shrink, int basis)
        {
            element.style.flexGrow = grow;
            element.style.flexShrink = shrink;
            element.style.flexBasis = basis;
        }

        public static void SetFlex(this VisualElement element, int grow, int shrink)
        {
            element.SetFlex(grow, shrink, 0);
        }

        public static void SetFlex(this VisualElement element, int grow)
        {
            element.SetFlex(grow, 0);
        }

        public static void SetRadius(this VisualElement element, int radius)
        {
            element.style.borderTopLeftRadius = radius;
            element.style.borderTopRightRadius = radius;
            element.style.borderBottomLeftRadius = radius;
            element.style.borderBottomRightRadius = radius;
        }

        public static void SetRadius(this VisualElement element, int topLeft, int topRight, int bottomLeft,
            int bottomRight)
        {
            element.style.borderTopLeftRadius = topLeft;
            element.style.borderTopRightRadius = topRight;
            element.style.borderBottomLeftRadius = bottomLeft;
            element.style.borderBottomRightRadius = bottomRight;
        }

        public static void SetBackgroundColor(this VisualElement element, float r, float g, float b, float a)
        {
            element.style.backgroundColor = new Color(r, g, b, a);
        }

        public static void SetBackgroundColor(this VisualElement element, float r, float g, float b)
        {
            element.style.backgroundColor = new Color(r, g, b);
        }

        public static void SetBackgroundColor(this VisualElement element, Color color)
        {
            element.style.backgroundColor = color;
        }

        public static void SetBorderWidth(this VisualElement element, int width)
        {
            element.style.borderTopWidth = width;
            element.style.borderRightWidth = width;
            element.style.borderBottomWidth = width;
            element.style.borderLeftWidth = width;
        }

        public static void SetBorderWidth(this VisualElement element, int top, int right, int bottom, int left)
        {
            element.style.borderTopWidth = top;
            element.style.borderRightWidth = right;
            element.style.borderBottomWidth = bottom;
            element.style.borderLeftWidth = left;
        }

        public static void SetBorderColor(this VisualElement element, float r, float g, float b, float a)
        {
            var color = new Color(r, g, b, a);
            element.style.borderLeftColor = color;
            element.style.borderRightColor = color;
            element.style.borderTopColor = color;
            element.style.borderBottomColor = color;
        }

        public static void SetBorderColor(this VisualElement element, float r, float g, float b)
        {
            var color = new Color(r, g, b);
            element.style.borderLeftColor = color;
            element.style.borderRightColor = color;
            element.style.borderTopColor = color;
            element.style.borderBottomColor = color;
        }

        public static void SetBorderColor(this VisualElement element, Color color)
        {
            element.style.borderLeftColor = color;
            element.style.borderRightColor = color;
            element.style.borderTopColor = color;
            element.style.borderBottomColor = color;
        }

        public static void SetBorderRadius(this VisualElement element, int radius)
        {
            element.style.borderTopLeftRadius = radius;
            element.style.borderTopRightRadius = radius;
            element.style.borderBottomLeftRadius = radius;
            element.style.borderBottomRightRadius = radius;
        }

        public static void SetBorderRadius(this VisualElement element, int topLeft, int topRight, int bottomLeft,
            int bottomRight)
        {
            element.style.borderTopLeftRadius = topLeft;
            element.style.borderTopRightRadius = topRight;
            element.style.borderBottomLeftRadius = bottomLeft;
            element.style.borderBottomRightRadius = bottomRight;
        }
    }
}