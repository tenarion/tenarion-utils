using Unity.Properties;

namespace Attributes
{
    public class DebugFieldAttribute : CreatePropertyAttribute
    {
        public string Title { get; }
        public bool ShowView { get; }
        public bool ShowGizmo { get; }

        public DebugFieldAttribute(string title, bool showView = true, bool showGizmo = false)
        {
            Title = title;
            ShowView = showView;
            ShowGizmo = showGizmo;
        }
    }
}
