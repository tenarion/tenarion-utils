using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

namespace EasyDebug
{
    /// <summary>
    /// A static class that provides easy logging with color and other features.
    /// </summary>
    public static class EasyLog
    {
        private static Dictionary<string, Color> _colorDictionary = new Dictionary<string, Color>()
        {
            { "%red%", UnityEngine.Color.red },
            { "%green%", UnityEngine.Color.green },
            { "%blue%", UnityEngine.Color.blue },
            { "%yellow%", UnityEngine.Color.yellow },
            { "%cyan%", UnityEngine.Color.cyan },
            { "%magenta%", UnityEngine.Color.magenta },
            { "%white%", UnityEngine.Color.white },
            { "%black%", UnityEngine.Color.black },
            { "%gray%", UnityEngine.Color.gray },
            { "%grey%", UnityEngine.Color.grey },
            { "%default%", new Color(0.824f, 0.824f, 0.824f) }
        };

        /// <summary>
        /// Logs a message that supports multiple colors. For adding a color, use the following format:
        /// <code>
        /// EasyLog.LogColor("%red%This is a red message.");
        /// EasyLog.LogColor("%ffff00%This is a yellow message.");
        /// EasyLog.LogColor("%#ff00ff%This is a magenta message. %default%Hey, I look normal now!");
        /// </code>
        /// </summary>
        /// <param name="message"></param>
        public static void Color(object message)
        {
            var msg = message.ToString();
            var resultMessage = ParseMessage(msg);
            Debug.Log(resultMessage);
        }

        private static string ParseMessage(string message)
        {
            var colorRegex = new Regex(@"(%(#?[\da-fA-F]{6})%|%[a-zA-Z]+%)");
            var matches = colorRegex.Matches(message);

            var lastIndex = 0;
            var result = "";
            var currentColor = "";

            foreach (Match match in matches)
            {
                var colorCode = match.Value;
                var text = message.Substring(lastIndex, match.Index - lastIndex);
                result += text;

                if (!string.IsNullOrEmpty(currentColor))
                {
                    result += "</color>";
                }

                if (!_colorDictionary.TryGetValue(colorCode, out var color))
                {
                    color = HexToColor(colorCode);
                }

                currentColor = $"<color=#{ColorUtility.ToHtmlStringRGB(color)}>";
                result += currentColor;
                lastIndex = match.Index + match.Length;
            }

            result += message[lastIndex..];
            if (!string.IsNullOrEmpty(currentColor))
            {
                result += "</color>";  // Close the last color tag
            }

            return result;
        }

        private static Color HexToColor(string hex)
        {
            var color = _colorDictionary["%default%"];

            if (hex.StartsWith("%#") && hex.EndsWith("%") && hex.Length == 9)
            {
                hex = hex.Substring(2, 6);
            }
            else if (hex.StartsWith("%") && hex.EndsWith("%") && hex.Length == 8)
            {
                hex = hex.Substring(1, 6);
            }

            return ColorUtility.TryParseHtmlString($"#{hex}", out var col) ? col : color;
        }

        public static void Color(object message, int color)
        {
            Debug.Log(
                $"<color=#{color:X2}>{message}</color>");
        }

        public static void Color(object message, Color color)
        {
            Debug.Log(
                $"<color=#{(byte)(color.r * 255f):X2}{(byte)(color.g * 255f):X2}{(byte)(color.b * 255f):X2}>{message}</color>");
        }

        public static void Color(object message, Color color, Object context)
        {
            Debug.Log(
                $"<color=#{(byte)(color.r * 255f):X2}{(byte)(color.g * 255f):X2}{(byte)(color.b * 255f):X2}>{message}</color>",
                context);
        }
    }
}
