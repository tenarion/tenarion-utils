using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using Attributes;
using Core.Extensions;
using DesignPatterns.EasySingleton;
using JetBrains.Annotations;
using Unity.Properties;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

namespace EasyDebug
{
    /// <summary>
    /// <para>A singleton that manages the debug fields on the game and displays them on a window.</para>
    /// <para>This is obligatory if you want to use the EasyDebug API.</para>
    /// </summary>
    public class DebugRuntime : Singleton<DebugRuntime>
    {
        private ObjectIDGenerator _idGenerator = new();

        private UIDocument _uiDocument;
        private VisualElement _root;
        private ScrollView _debugFieldsScrollView;

        private DebugFieldView _debugSpotlightText;
        // private GroupBox _loggerContainer;

        private readonly Dictionary<string, DebugFieldView> _bindings = new();
        private readonly List<DebugFieldView> _gizmos = new();

        public override void Awake()
        {
            base.Awake();
            _uiDocument = gameObject.AddComponent<UIDocument>();
            _uiDocument.panelSettings =
                Resources.Load<PanelSettings>("RuntimeDebug/DebugViewPanel");
            _uiDocument.visualTreeAsset =
                Resources.Load<VisualTreeAsset>("RuntimeDebug/RuntimeDebugView");

            _root = _uiDocument.rootVisualElement;
            _root.style.flexGrow = 1;

            _debugFieldsScrollView = _root.Q<ScrollView>("debug-fields-scroll");
            _debugSpotlightText = _root.Q<DebugFieldView>("debug-spotlight-text");
        }

        public void Start()
        {
            // _loggerContainer = _root.Q<GroupBox>("logger");

            // BindDefaultLogger();

            var types = System.AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => p.GetProperties().Any(prop =>
                    prop.GetCustomAttributes(typeof(DebugFieldAttribute), true).Length > 0) || p.GetFields()
                    .Any(field => field.GetCustomAttributes(typeof(DebugFieldAttribute), true).Length > 0));

            var unityTypes = types.Where(t => t.IsSubclassOf(typeof(Object)));

            // BindObjects(unityObjects);
            foreach (var unityType in unityTypes)
            {
                var unityObjects = FindObjectsByType(unityType, FindObjectsInactive.Include, FindObjectsSortMode.None);
                foreach (var unityObject in unityObjects)
                {
                    var t = unityObject as Transform ?? (unityObject as Component)?.transform;
                    BindObjectAttributes(unityObject, t);
                }
            }
        }

        private void BindObjectAttributes(object obj, Transform rootTransform, HashSet<object> visited = null,
            List<string> includePaths = null,
            List<string> excludePaths = null)
        {
            visited ??= new HashSet<object>();

            if (!visited.Add(obj))
                return;

            var properties = obj.GetType().GetProperties().Where(p =>
                p.PropertyType != typeof(Object) && !p.IsDefined(typeof(ObsoleteAttribute)));
            foreach (var property in properties)
            {
                var attribute = property.GetCustomAttribute<DebugFieldAttribute>(false);

                if (includePaths != null && !includePaths.Contains(property.Name)) continue;
                if (excludePaths != null && excludePaths.Contains(property.Name)) continue;

                if (attribute != null)
                    Bind(obj, property.Name, attribute.Title, attribute.ShowView, attribute.ShowGizmo, rootTransform);

                if (property.PropertyType.IsClass)
                {
                    var debugClassAttribute = property.GetCustomAttribute<DebugClassAttribute>(false);

                    if (debugClassAttribute == null ||
                        debugClassAttribute.Assignment == DebugFieldAssignment.None) continue;

                    if (debugClassAttribute.Assignment == DebugFieldAssignment.Include)
                    {
                        includePaths ??= new List<string>();
                        includePaths.AddRange(debugClassAttribute.FieldPaths);
                    }

                    if (debugClassAttribute.Assignment == DebugFieldAssignment.Exclude)
                    {
                        excludePaths ??= new List<string>();
                        excludePaths.AddRange(debugClassAttribute.FieldPaths);
                    }

                    var propertyInstance = property.GetValue(obj);
                    if (propertyInstance == null) continue;
                    BindObjectAttributes(propertyInstance, rootTransform, visited, includePaths, excludePaths);
                }
            }

            var fields = obj.GetType().GetFields()
                .Where(f => f.FieldType != typeof(Object) && !f.IsDefined(typeof(ObsoleteAttribute)));
            foreach (var field in fields)
            {
                var attribute = field.GetCustomAttribute<DebugFieldAttribute>(false);
                if (attribute != null)
                    Bind(obj, field.Name, attribute.Title, attribute.ShowView, attribute.ShowGizmo, rootTransform);

                if (field.FieldType.IsClass)
                {
                    var debugClassAttribute = field.GetCustomAttribute<DebugClassAttribute>(false);

                    if (debugClassAttribute == null ||
                        debugClassAttribute.Assignment == DebugFieldAssignment.None) continue;

                    if (debugClassAttribute.Assignment == DebugFieldAssignment.Include)
                    {
                        includePaths ??= new List<string>();
                        includePaths.AddRange(debugClassAttribute.FieldPaths);
                    }

                    if (debugClassAttribute.Assignment == DebugFieldAssignment.Exclude)
                    {
                        excludePaths ??= new List<string>();
                        excludePaths.AddRange(debugClassAttribute.FieldPaths);
                    }

                    var fieldInstance = field.GetValue(obj);
                    if (fieldInstance == null) continue;
                    BindObjectAttributes(fieldInstance, rootTransform, visited, includePaths, excludePaths);
                }
            }
        }

        // private void BindDefaultLogger()
        // {
        //     var loggerScrollView = _loggerContainer.Q<ScrollView>("logger-scroll");
        //     Application.logMessageReceived += (condition, trace, type) =>
        //     {
        //         loggerScrollView.Add(new Label(condition + " " + trace + " " + type));
        //     };
        // }

        /// <summary>
        /// Binds an object to a new debug field.
        /// </summary>
        /// <param name="obj">The object containing the field you want to bind.</param>
        /// <param name="title">The title of the debug field</param>
        /// <param name="fieldPath">The path of the field you want to bind. Typically this is the nameof().</param>
        /// <example>
        /// This only will work if you add the attribute <see cref="Unity.Properties.CreatePropertyAttribute"/> to the property you want to bind, which in this scenario is <c>Health</c>.
        /// It would look like this:
        ///   <code>
        ///     public class Player
        ///     {
        ///         [CreateProperty]
        ///         public int Health { get; set; }
        ///     }
        ///     ...
        ///     var player = new Player();
        ///     player.Health = 50;
        ///     DebugRuntime.Instance.Bind(player, "Title Health", nameof(player.Health));
        ///   </code>
        /// </example>
        public void Bind(object obj, string fieldPath, string title) => Bind(obj, fieldPath, title, true, false, null);

        /// <inheritdoc cref="Bind(object,string,string)"/>
        /// <param name="obj"></param>
        /// <param name="fieldPath"></param>
        /// <param name="title"></param>
        /// <param name="showView">Whether show this field on the debug window or not.</param>
        /// <param name="showGizmo"></param>
        /// <param name="rootTransform"></param>
        private void Bind(object obj, string fieldPath, string title, bool showView, bool showGizmo,
            Transform rootTransform)
        {
            var debugFieldView = new DebugFieldView(title, fieldPath, rootTransform)
            {
                dataSource = obj
            };

            var fieldId = GetFieldId(obj, fieldPath);

            if (showView) _debugFieldsScrollView.Add(debugFieldView);
            if (showGizmo) _gizmos.Add(debugFieldView);
            _bindings.Add(fieldId, debugFieldView);
        }


        /// <inheritdoc cref="Bind(object,string,string,bool)"/>
        /// <param name="t">The GameObject that will show the gizmo. If null, the same object bound will be used if it is a GameObject or a Monobehaviour.</param>
        public void BindGizmo(object obj, string fieldPath, string title, bool showView = false,
            Transform t = null)
        {
            if (t == null)
            {
                t = obj switch
                {
                    MonoBehaviour mono => mono.transform,
                    Transform trans => trans,
                    _ => throw new ArgumentException(
                        "The object must be a GameObject or a MonoBehaviour to show the gizmo on it.")
                };
            }

            Bind(obj, fieldPath, title, showView, true, t);
        }


        private void OnDrawGizmos()
        {
            foreach (var debugFieldView in _gizmos)
            {
                var textSize = HandleUtility.GetHandleSize(debugFieldView.RootTransform.transform.position) * 0.1f;
                Handles.Label(debugFieldView.RootTransform.transform.position,
                    $"{debugFieldView.Title} -> {debugFieldView.Value}");
            }
        }

        private string GetFieldId(object obj, string fieldPath) => _idGenerator.GetId(obj, out _) + $".{fieldPath}";

        /// <summary>
        /// Unbinds a debug field from an object.
        /// </summary>
        /// <param name="obj">The object containing the field you want to unbind.</param>
        /// <param name="fieldPath">The path of the field you want to unbind. Typically this is the nameof().</param>
        public void Unbind(object obj, string fieldPath)
        {
            var fieldId = GetFieldId(obj, fieldPath);
            var debugFieldView = _bindings[fieldId];

            if (debugFieldView == null)
            {
                Debug.LogWarning($"The debug field with the path {fieldPath} is not bound.");
                return;
            }


            _debugFieldsScrollView.Remove(debugFieldView);
            _bindings.Remove(fieldId);
            _gizmos.Remove(debugFieldView);
        }

        public void Show()
        {
            _root.Show();
        }

        public void Hide()
        {
            _root.Hide();
        }

        public void Toggle()
        {
            _root.style.display =
                _root.style.display == DisplayStyle.None
                    ? DisplayStyle.Flex
                    : DisplayStyle.None;
        }

        public struct DebugData
        {
            public string Title { get; set; }
            public object Value { get; set; }
        }

        public void Spotlight(string title, string text)
        {
            _debugSpotlightText.Title = title;
            _debugSpotlightText.Value = text;
        }
    }
}
