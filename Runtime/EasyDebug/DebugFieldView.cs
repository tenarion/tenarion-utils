#if DOTWEEN
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
#endif

using System;
using Core.Extensions;
using UnityEngine;
using Unity.Properties;
using UnityEngine.UIElements;

namespace EasyDebug
{
    [UxmlElement]
    internal partial class DebugFieldView : VisualElement
    {
        private Label _title;
        private Label _value;

        public delegate void ValueChangedHandler(string newValue);

        public event ValueChangedHandler ValueChanged;


#if DOTWEEN
        private TweenerCore<Color, Color, ColorOptions> _valueTween;
#endif

        /// <summary>
        /// The upmost parent GameObject of the object being debugged.
        /// </summary>
        internal Transform RootTransform { get; }

        /// <summary>
        /// The field path of the field being debugged.
        /// </summary>
        internal string CurrentFieldPath { get; }

        public string Title
        {
            get => _title.text;
            set => _title.text = value;
        }

        public string Value
        {
            get => _value.text;
            set => _value.text = value;
        }
        
        internal DebugFieldView(string title, string fieldPath,
            Transform rootTransform) : this()
        {
            RootTransform = rootTransform;
            CurrentFieldPath = fieldPath;
            _title.text = title;
            _value.SetBinding("text", new DataBinding
            {
                dataSourcePath = new PropertyPath(fieldPath),
                bindingMode = BindingMode.ToTarget
            });

            _value.RegisterValueChangedCallback(OnValueChanged);
        }

        public DebugFieldView()
        {
            var visualTree = Resources.Load<VisualTreeAsset>("DebugField/DebugFieldView");
            visualTree.CloneTree(this);
            _title = this.Q<Label>("title");
            _value = this.Q<Label>("value");
        }

        private void OnValueChanged(ChangeEvent<string> evt)
        {
            if (evt.previousValue == evt.newValue) return;

            ValueChanged?.Invoke(evt.newValue);
            Debug.Log("AAAAAAA");

            if (string.IsNullOrEmpty(evt.newValue))
            {
                _value.Hide();
                return;
            }

            _value.Show();

#if DOTWEEN
            _value.style.color = Color.green;
            _valueTween?.Kill();
            _valueTween = DOTween.To(() => _value.style.color.value, x => _value.style.color = x,
                    Color.white, 1f)
                .SetEase(Ease.OutExpo);
#endif
        }
    }
}
