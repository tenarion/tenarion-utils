using Unity.Properties;

namespace Attributes
{
    /// <summary>
    /// A <see cref="DebugFieldAttribute"/> won't be automatically initialized by the <see cref="EasyDebug.DebugRuntime"/> inside a non-Unity object unless this attribute is present in the class field.
    /// </summary>
    public class DebugClassAttribute : CreatePropertyAttribute
    {
        /// <summary>
        /// The assignment type of the debug field. This can be <see cref="DebugFieldAssignment.None"/>, <see cref="DebugFieldAssignment.All"/>, <see cref="DebugFieldAssignment.Include"/>, or <see cref="DebugFieldAssignment.Exclude"/>.
        /// </summary>
        public DebugFieldAssignment Assignment { get; }

        /// <summary>
        /// The field paths to include or exclude. This is only used if <see cref="Assignment"/> is <see cref="DebugFieldAssignment.Include"/> or <see cref="DebugFieldAssignment.Exclude"/>.
        /// </summary>
        public string[] FieldPaths { get; }

        public DebugClassAttribute(DebugFieldAssignment debugFieldAssignment,
            params string[] fieldPaths)
        {
            Assignment = debugFieldAssignment;
            FieldPaths = fieldPaths;
        }
    }

    public enum DebugFieldAssignment
    {
        /// <summary>
        /// Explicitly states that no assignment should be made.
        /// </summary>
        None,
        /// <summary>
        /// Includes all fields.
        /// </summary>
        All,
        /// <summary>
        /// Includes the specified fields in <see cref="DebugClassAttribute.FieldPaths"/>.
        /// </summary>
        Include,
        /// <summary>
        /// Excludes the specified fields in <see cref="DebugClassAttribute.FieldPaths"/>.
        /// </summary>
        Exclude
    }
}
