#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using Core.Helpers;
using UnityEngine;

namespace EasySave
{
    public class EasySaveSettings : ScriptableObject
    {
        public const string EasySaveSettingsPath = "Assets/Resources/EasyUtils/EasySaveSettings.asset";

        public string SaveFilePath = "EasySave/save.json";
        public string SettingsFilePath = "EasySave/preferences.json";
        public bool EnableAutoSave;
        public float AutoSaveTimer = 10;

        public static EasySaveSettings GetOrCreateSettings()
        {
#if UNITY_EDITOR
            var settings = AssetDatabase.LoadAssetAtPath<EasySaveSettings>(EasySaveSettingsPath);
            if (settings == null)
            {
                //if folder doesn't exist create it
                AssetDatabaseHelper.CreateFolderHierarchyIfNotExists("Resources/EasyUtils");


                settings = CreateInstance<EasySaveSettings>();
                AssetDatabase.CreateAsset(settings, EasySaveSettingsPath);
                AssetDatabase.SaveAssets();
            }

            return settings;
#else
            var settings = UnityEngine.Resources.Load<EasySaveSettings>("EasyUtils/EasySaveSettings");
            return settings;
#endif
        }

#if UNITY_EDITOR
        public static SerializedObject GetSerializedSettings()
        {
            return new SerializedObject(GetOrCreateSettings());
        }
#endif
    }
}
