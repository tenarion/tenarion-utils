using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace EasySave
{
    /// <summary>
    /// A class that represents a collection of settings wrapped in categories.
    /// </summary>
    [Serializable]
    public class EasySettings
    {
        public List<EasySettingsCategory> Categories;

        public EasySettings(List<EasySettingsCategory> categories)
        {
            Categories = categories;
        }

        public EasySettings()
        {
            Categories = new List<EasySettingsCategory>();
        }

        public EasySettingsCategory AddCategory(EasySettingsCategory category)
        {
            Categories.Add(category);
            return category;
        }

        public EasySettingsCategory AddCategory(string name)
        {
            var category = new EasySettingsCategory(name);
            Categories.Add(category);
            return category;
        }

        public void RemoveCategory(EasySettingsCategory category)
        {
            Categories.Remove(category);
        }

        public void RemoveCategory(string name)
        {
            Categories.Remove(Categories.Find(x => x.Name == name));
        }

        public EasySettingsCategory GetCategory(string name)
        {
            return Categories.Find(x => x.Name == name);
        }

        public EasySetting GetSetting(string categoryName, string settingName)
        {
            return GetCategory(categoryName)?.GetSetting(settingName);
        }

        public void SetSetting<T>(string categoryName, string settingName, T value)
        {
            GetCategory(categoryName)?.SetSetting(settingName, value);
        }

        public void RemoveSetting(string categoryName, string settingName)
        {
            GetCategory(categoryName)?.RemoveSetting(GetSetting(categoryName, settingName));
        }

        public EasySetting GetSetting(string settingName)
        {
            return Categories
                .Select(category => category.GetSetting(settingName)).FirstOrDefault(setting => setting != null);
        }

        public T GetSetting<T>(string categoryName, string settingName)
        {
            var setting = GetSetting(categoryName, settingName);
            return setting.GetValue<T>();
        }

        public T GetSetting<T>(string settingName)
        {
            var setting = GetSetting(settingName);
            return setting.GetValue<T>();
        }

        public bool TryGetSetting<T>(string categoryName, string settingName, out T value)
        {
            var setting = GetSetting(categoryName, settingName);
            return setting.TryGetValue(out value);
        }

        public bool TryGetSetting<T>(string settingName, out T value)
        {
            var setting = GetSetting(settingName);
            return setting.TryGetValue(out value);
        }

        public bool TryGetSetting(string categoryName, string settingName, out EasySetting setting)
        {
            setting = GetSetting(categoryName, settingName);
            return setting != null;
        }

        public bool TryGetSetting(string settingName, out EasySetting setting)
        {
            setting = GetSetting(settingName);
            return setting != null;
        }

        public bool IsEmpty()
        {
            return Categories.Count == 0;
        }

        public bool IsEmpty(string categoryName)
        {
            return GetCategory(categoryName).IsEmpty();
        }

        public Dictionary<string, Dictionary<string, object>> ToDictionary()
        {
            return Categories.ToDictionary(
                category => category.Name,
                category => category.ToDictionary()
            );
        }

        //builder

        public class Builder
        {
            private EasySettings _settings = new();

            public BuilderCategory AddCategory(EasySettingsCategory category)
            {
                _settings.AddCategory(category);
                return new BuilderCategory(category, this);
            }

            public BuilderCategory AddCategory(string name)
            {
                var category = new EasySettingsCategory(name);
                _settings.AddCategory(category);
                return new BuilderCategory(category, this);
            }

            public class BuilderCategory
            {
                private Builder _originalBuilder;
                private EasySettingsCategory _category;

                public BuilderCategory(EasySettingsCategory category, Builder originalBuilder)
                {
                    _category = category;
                    _originalBuilder = originalBuilder;
                }

                public BuilderCategory SetSetting<T>(string name, T value)
                {
                    _category.SetSetting(name, value);
                    return this;
                }

                public Builder Wrap()
                {
                    return _originalBuilder;
                }
            }

            public EasySettings Build()
            {
                return _settings;
            }
        }

        public static EasySettings FromDictionary(Dictionary<string, Dictionary<string, object>> dictionary)
        {
            var settings = new EasySettings();
            foreach (var kvp in dictionary)
            {
                var category = new EasySettingsCategory(kvp.Key);
                foreach (var setting in kvp.Value) category.SetSetting(setting.Key, setting.Value);

                settings.AddCategory(category);
            }

            return settings;
        }
    }

    [Serializable]
    public class EasySetting
    {
        public string Name;
        [SerializeField] private object _value;

        public object Value
        {
            get => _value;
            private set
            {
                _value = value;
                OnValueChanged?.Invoke(value);
            }
        }

        public event Action<object> OnValueChanged;

        public EasySetting(string name, object value)
        {
            Name = name;
            Value = value;
        }

        public void SetValueWithoutNotify<T>(T value)
        {
            if (value is JObject jObject) _value = Deserialize<T>(jObject);
            else _value = value;
        }

        public T GetValue<T>()
        {
            return Value switch
            {
                T value => value,
                JObject jObject => Deserialize<T>(jObject),
                _ => default
            };
        }

        public void SetValue<T>(T value)
        {
            SetValueWithoutNotify(value);
            OnValueChanged?.Invoke(value);
        }

        public bool TryGetValue<T>(out T value)
        {
            value = GetValue<T>();
            return value != null;
        }

        private T Deserialize<T>(JObject value)
        {
            return JsonConvert.DeserializeObject<T>(value.ToString(Formatting.Indented));
        }

        public override string ToString()
        {
            return $"{Name}: {Value}";
        }
    }

    [Serializable]
    public class EasySettingsCategory
    {
        public string Name;
        public List<EasySetting> Settings;

        public EasySettingsCategory(string name, List<EasySetting> settings)
        {
            Name = name;
            Settings = settings;
        }

        public EasySettingsCategory(string name)
        {
            Name = name;
            Settings = new List<EasySetting>();
        }

        public void SetSetting<T>(string name, T value)
        {
            var setting = Settings.FirstOrDefault(x => x != null && x.Name == name);
            if (setting != null) setting.SetValue(value);
            else Settings.Add(new EasySetting(name, value));
        }

        public void RemoveSetting(EasySetting setting)
        {
            Settings.Remove(setting);
        }

        /// <summary>
        /// Get a setting by name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public EasySetting GetSetting(string name)
        {
            return Settings.FirstOrDefault(x => x.Name == name);
        }

        /// <summary>
        /// Get a setting value by name and cast it to type T.
        /// </summary>
        /// <param name="name"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>Value casted to T or default</returns>
        public T GetSetting<T>(string name)
        {
            var setting = GetSetting(name);
            return setting.GetValue<T>();
        }

        public bool TryGetSetting<T>(string name, out T value)
        {
            var setting = GetSetting(name);
            return setting.TryGetValue(out value);
        }

        public bool TryGetSetting(string name, out EasySetting setting)
        {
            setting = GetSetting(name);
            return setting != null;
        }

        public bool IsEmpty()
        {
            return Settings.Count == 0;
        }

        public Dictionary<string, object> ToDictionary()
        {
            return Settings.ToDictionary(setting => setting.Name, setting => setting.Value);
        }
    }
}