using System;
using EasyIdentification;
using UnityEngine;

namespace EasySave
{
    public interface ISaveable : IIdentifiable
    {
        object SaveData { get; }
        void LoadState(string jsonData);
    }
}