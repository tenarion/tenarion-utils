using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DesignPatterns.EasySingleton;
using EasyIdentification;
using Newtonsoft.Json;
using UnityEngine;

namespace EasySave
{
    [Serializable]
    public class SaveManager : Singleton<SaveManager>
    {
        #region Variables

        public List<IdManager.SaveableData> SavedUIDs = new();
        public AutoSaver AutoSave = new();
        private Dictionary<string, object> _savedStatesMemory;
        private EasySettings _settings;

        public bool Verbose;

        public delegate void SaveHandler(bool isAutoSave);

        public SaveHandler OnSaveBegin;
        public SaveHandler OnSaveComplete;

        public Action OnLoadBegin;
        public Action OnLoadComplete;

        public Action OnQuickSave;
        public Action OnQuickLoad;

        private EasySaveSettings GlobalSettings => EasySaveSettings.GetOrCreateSettings();
        private string _formattedSavePath;
        private string _formattedSettingsPath;

        #endregion

        public override void Awake()
        {
            base.Awake();
            _formattedSavePath = FormatPath(GlobalSettings.SaveFilePath);
            _formattedSettingsPath = FormatPath(GlobalSettings.SettingsFilePath);
        }

        private void Start()
        {
            if (IdManager.Instance == null)
            {
                Debug.LogError("[EASY SAVE] IdManager is null. Make sure you have an IdManager in your scene.");
                return;
            }

            SavedUIDs = IdManager.Instance.SavedIds.Keys
                .Where(x => IdManager.Instance.SavedIds[x] is ISaveable)
                .ToList();
        }


        public void SaveGame(bool isAutoSave = false, bool append = false)
        {
            IEnumerator SaveGameCoroutine()
            {
                var currentStates = GetCurrentStates();

                var keyValuePairs = new List<KeyValuePair<string, object>>();
                foreach (var kvp in currentStates) keyValuePairs.Add(kvp);

                var json = string.Empty;

                if (append)
                {
                    var savedStates = GetSavedStates(_formattedSavePath);

                    foreach (var kvp in keyValuePairs) savedStates[kvp.Key] = kvp.Value;

                    json = JsonConvert.SerializeObject(savedStates.ToList(), Formatting.Indented,
                        new JsonSerializerSettings
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
                }
                else
                {
                    json = JsonConvert.SerializeObject(keyValuePairs, Formatting.Indented,
                        new JsonSerializerSettings
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
                }

                Directory.CreateDirectory(Path.GetDirectoryName(_formattedSavePath) ?? string.Empty);
                File.WriteAllText(_formattedSavePath, json);
                if (Verbose) print($"[EASY SAVE] Finished saving in {Time.deltaTime}s");
                OnSaveComplete?.Invoke(isAutoSave);
                yield return null;
            }

            OnSaveBegin?.Invoke(isAutoSave);
            StartCoroutine(SaveGameCoroutine());
        }

        private string FormatPath(string path) => Application.persistentDataPath + "/" + path;

        public void ClearSave()
        {
            File.Delete(_formattedSavePath);
        }

        private Dictionary<string, object> GetSavedStates(string path)
        {
            return File.Exists(path)
                ? JsonConvert.DeserializeObject<List<KeyValuePair<string, object>>>(File.ReadAllText(path))
                    .ToDictionary(kvp => kvp.Key, kvp => kvp.Value)
                : new Dictionary<string, object>();
        }

        public void LoadGame()
        {
            LoadGame(_formattedSavePath);
        }

        public void LoadGame(string path)
        {
            IEnumerator LoadGameCoroutine()
            {
                var savedStates = GetSavedStates(path);

                var saveables = FindObjectsByType<MonoBehaviour>(FindObjectsInactive.Include, FindObjectsSortMode.None)
                    .OfType<ISaveable>();
                foreach (var saveable in saveables)
                    if (savedStates.ContainsKey(saveable.Id))
                        saveable.LoadState(savedStates[saveable.Id].ToString());

                if (Verbose) print($"[EASY SAVE] Load completed in {Time.deltaTime}s");
                OnLoadComplete?.Invoke();
                yield return null;
            }

            OnLoadBegin?.Invoke();
            StartCoroutine(LoadGameCoroutine());
        }

        public void LoadGame(Dictionary<string, object> savedStates)
        {
            IEnumerator LoadGameCoroutine()
            {
                var behaviours = FindObjectsByType<MonoBehaviour>(FindObjectsInactive.Include, FindObjectsSortMode.None)
                    .OfType<ISaveable>();
                foreach (var behaviour in behaviours)
                    if (savedStates.ContainsKey(behaviour.Id))
                        behaviour.LoadState(JsonConvert.SerializeObject(savedStates[behaviour.Id], Formatting.Indented,
                            new JsonSerializerSettings
                            {
                                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                            }));

                if (Verbose) print($"[EASY SAVE] Quick load completed in {Time.deltaTime}s");
                OnQuickLoad?.Invoke();
                yield return null;
            }

            StartCoroutine(LoadGameCoroutine());
        }

        public void QuickSave()
        {
            _savedStatesMemory = GetCurrentStates();
            if (Verbose) print($"[EASY SAVE] Quick save completed in {Time.deltaTime}s");
            OnQuickSave?.Invoke();
        }

        public void QuickLoad()
        {
            LoadGame(_savedStatesMemory);
        }

        #region Autosave

        //Exposing the Autosave methods for UnityEvents

        public void StartAutoSave()
        {
            AutoSave.Start();
        }

        public void StopAutoSave()
        {
            AutoSave.Stop();
        }

        #endregion

        #region Settings

        public void SaveSettings(EasySettings settings)
        {
            var json = JsonConvert.SerializeObject(settings.ToDictionary(), Formatting.Indented);
            Directory.CreateDirectory(Path.GetDirectoryName(_formattedSettingsPath) ?? string.Empty);
            File.WriteAllText(_formattedSettingsPath, json);
            if (Verbose) print("[EASY SAVE] Settings saved");
        }

        /// <summary>
        /// Loads the settings from the settings file. If the settings are already loaded, it returns the cached settings.
        /// If you want to force a reload, use <see cref="ReloadSettings"/> instead.
        /// </summary>
        /// <returns></returns>
        public EasySettings LoadSettings()
        {
            if (Verbose) print("[EASY SAVE] Settings loaded");
            if (_settings == null)
                try
                {
                    var dictionary =
                        JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, object>>>(
                            File.ReadAllText(_formattedSettingsPath));
                    _settings = EasySettings.FromDictionary(dictionary);
                }
                catch (Exception e)
                {
                    Debug.LogWarning("[EASY SAVE] Settings file not found. Creating a new one.");
                    _settings = new EasySettings();
                }

            return _settings;
        }

        /// <summary>
        /// Reloads the settings from the settings file and returns the new settings.
        /// </summary>
        /// <returns></returns>
        public EasySettings ReloadSettings()
        {
            _settings = null;
            return LoadSettings();
        }

        #endregion

        #region Utilities

        private Dictionary<string, object> GetCurrentStates()
        {
            var savedStates = new Dictionary<string, object>();
            var saveables = FindObjectsByType<MonoBehaviour>(FindObjectsInactive.Include, FindObjectsSortMode.None)
                .OfType<ISaveable>();

            foreach (var saveable in saveables) savedStates[saveable.Id] = saveable.SaveData;

            return savedStates;
        }

        #endregion
    }
}
