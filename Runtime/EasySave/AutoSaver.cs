using System;
using System.Collections;
using UnityEngine;

namespace EasySave
{
    [Serializable]
    public class AutoSaver
    {
        private EasySaveSettings Settings => EasySaveSettings.GetOrCreateSettings();
        private Coroutine _autoSaveCoroutine;

        private IEnumerator AutoSaveCoroutine()
        {
            yield return new WaitForSeconds(Settings.AutoSaveTimer * 60);
            SaveManager.Instance.SaveGame(true);
        }

        public void Start()
        {
            if (_autoSaveCoroutine != null) return;
            _autoSaveCoroutine = SaveManager.Instance.StartCoroutine(AutoSaveCoroutine());
        }

        public void Stop()
        {
            if (_autoSaveCoroutine == null) return;
            SaveManager.Instance.StopCoroutine(_autoSaveCoroutine);
            _autoSaveCoroutine = null;
        }
    }
}