using System.Collections.Generic;
using EasyWFC.General;
using EasyWFC.General.Generation;

namespace EasyWFC.Nodes
{
    public interface IWfcNode
    {
        public string Id { get; }
        // public List<WfcNeighbour> Neighbours { get; set; }
    }
}