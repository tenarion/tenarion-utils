using EasyWFC.General;
using EasyWFC.General.Generation;
using UnityEngine;

namespace EasyWFC.Nodes.Templates
{
    [CreateAssetMenu(menuName = "Easy Utils/WFC/Node Template", fileName = "New WFC Node Template")]
    public class WfcNodeTemplate : ScriptableObject
    {
        // public WfcNeighbours Up;
        // public WfcNeighbours Down;
        // public WfcNeighbours Left;
        // public WfcNeighbours Right;
        // public WfcNeighbours Forward;
        // public WfcNeighbours Backward;
    }
}