namespace EasyWFC.Nodes.Generation
{
    public interface IWfcCell
    {
        public bool IsCollapsed { get; }
        public IWfcNode[] NodeOptions { get; }
        public int Entropy { get; }
        public void UpdateCell(IWfcNode[] nodes);
        public void Collapse(IWfcNode selectedTile);
    }
}