using System;

namespace EasyWFC.Nodes.Generation
{
    public class WfcCell : IWfcCell
    {
        public bool IsCollapsed { get; set; }
        public WfcNode[] NodeOptions { get; set; }
        IWfcNode[] IWfcCell.NodeOptions => NodeOptions;
        public int Entropy => NodeOptions.Length;

        public WfcCell(bool isCollapsed, WfcNode[] nodeOptions)
        {
            IsCollapsed = isCollapsed;
            NodeOptions = nodeOptions;
        }

        public void UpdateCell(IWfcNode[] nodes)
        {
            NodeOptions = Array.ConvertAll(nodes, node => (WfcNode)node);
        }

        public void Collapse(IWfcNode selectedTile)
        {
            IsCollapsed = true;
            NodeOptions = new[] { (WfcNode)selectedTile };
        }
    }
}