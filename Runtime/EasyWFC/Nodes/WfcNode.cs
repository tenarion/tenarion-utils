using System;
using System.Collections.Generic;
using EasyWFC.General.Generation;
using EasyWFC.General.Rules;
using EasyWFC.Nodes.Templates;
using UnityEngine;

namespace EasyWFC.Nodes
{
    [CreateAssetMenu(menuName = "Easy Utils/WFC/Node", fileName = "New WFC Node")]
    [Serializable]
    public class WfcNode : ScriptableObject, IWfcNode
    {
        public string Id { get; } = Guid.NewGuid().ToString();
        public string Name = "Node";
        public float Weight = 1f;

        public GameObject Prefab;

        // public Vector3 TopEntrancePosition;
        // public Vector3 RightEntrancePosition;
        // public Vector3 BottomEntrancePosition;
        // public Vector3 LeftEntrancePosition;
        public bool UseTemplate;

        public WfcNodeTemplate Template;
        public List<WfcNeighbour> Neighbours = new();
        public List<WfcRule> Rules = new();
    }
}
