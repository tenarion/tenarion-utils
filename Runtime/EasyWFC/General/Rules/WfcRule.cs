using System;
using System.Collections.Generic;
using System.Linq;
using EasyWFC.General.Conditions;
using EasyWFC.General.Generation;
using EasyWFC.Nodes;
using UnityEngine;

namespace EasyWFC.General.Rules
{
    [Serializable]
    public class WfcRule
    {
        public WfcNode CurrentNode;
        [field: SerializeField] public List<WfcConditionBlock> ConditionBlocks { get; private set; } = new();


        public WfcRule(WfcNode currentNode)
        {
            CurrentNode = currentNode;
        }

        public bool Evaluate(WfcSnapshot snapshot)
        {
            if (ConditionBlocks.Count == 0)
                return true;

            var result = ConditionBlocks[0].Evaluate(snapshot);
            for (var i = 1; i < ConditionBlocks.Count; i++)
            {
                var previousConditionBlock = ConditionBlocks[i - 1];
                var nextConditionBlock = ConditionBlocks[i];
                switch (previousConditionBlock.LogicGate)
                {
                    case LogicGate.And:
                        result &= nextConditionBlock.Evaluate(snapshot);
                        break;
                    case LogicGate.Or:
                        result |= nextConditionBlock.Evaluate(snapshot);
                        break;
                    case LogicGate.Xor:
                        result ^= nextConditionBlock.Evaluate(snapshot);
                        break;
                    case LogicGate.Nand:
                        result &= !nextConditionBlock.Evaluate(snapshot);
                        break;
                    case LogicGate.Nor:
                        result |= !nextConditionBlock.Evaluate(snapshot);
                        break;
                    case LogicGate.Xnor:
                        result ^= !nextConditionBlock.Evaluate(snapshot);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }


            return result;
        }
    }
}