namespace EasyWFC.General.Conditions
{
    public enum LogicGate
    {
        And,
        Or,
        Xor,
        Nand,
        Nor,
        Xnor
    }
}