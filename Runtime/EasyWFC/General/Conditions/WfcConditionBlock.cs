using System;
using System.Collections.Generic;
using EasyWFC.General.Generation;
using EasyWFC.Nodes;
using UnityEngine;

namespace EasyWFC.General.Conditions
{
    [Serializable]
    public class WfcConditionBlock
    {
        public WfcNode CurrentNode;
        [SerializeReference] public List<WfcCondition> Conditions = new();

        public WfcConditionBlock(WfcNode currentNode)
        {
            CurrentNode = currentNode;
        }

        [field: SerializeField] public LogicGate LogicGate { get; set; }

        public bool Evaluate(WfcSnapshot snapshot)
        {
            if (Conditions.Count == 0)
                return true;

            var result = Conditions[0].Evaluate(snapshot);
            for (var i = 1; i < Conditions.Count; i++)
            {
                var previousCondition = Conditions[i - 1];
                var nextCondition = Conditions[i];
                switch (previousCondition.LogicGate)
                {
                    case LogicGate.And:
                        result &= nextCondition.Evaluate(snapshot);
                        break;
                    case LogicGate.Or:
                        result |= nextCondition.Evaluate(snapshot);
                        break;
                    case LogicGate.Xor:
                        result ^= nextCondition.Evaluate(snapshot);
                        break;
                    case LogicGate.Nand:
                        result &= !nextCondition.Evaluate(snapshot);
                        break;
                    case LogicGate.Nor:
                        result |= !nextCondition.Evaluate(snapshot);
                        break;
                    case LogicGate.Xnor:
                        result ^= !nextCondition.Evaluate(snapshot);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return result;
        }
    }
}