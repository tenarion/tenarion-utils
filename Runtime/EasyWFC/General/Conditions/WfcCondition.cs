using System;
using EasyWFC.General.Generation;
using EasyWFC.Nodes;
using UnityEngine.UIElements;

namespace EasyWFC.General.Conditions
{
    [Serializable]
    public abstract class WfcCondition
    {
        public abstract string Name { get; set; }
        public abstract WfcNode TargetNeighbour { get; set; }
        public WfcNode CurrentNode;
        public abstract bool Evaluate(WfcSnapshot snapshot);
        public abstract LogicGate LogicGate { get; set; }
        public abstract VisualElement CreateVisualElement();
        public abstract object GetData();
        public abstract void SetData(object data);

        public void SetDataFromEvent<T>(ChangeEvent<T> evt)
        {
            SetData(evt.newValue);
        }

        public WfcCondition(WfcNode currentNode)
        {
            CurrentNode = currentNode;
        }
    }

    [Serializable]
    public abstract class WfcCondition<TData> : WfcCondition
    {
        public abstract TData Data { get; set; }

        public override object GetData()
        {
            return Data;
        }

        public override void SetData(object data)
        {
            Data = (TData)data;
        }

        protected WfcCondition(WfcNode currentNode) : base(currentNode)
        {
        }
    }
}