using System;
using System.Collections.Generic;
using System.Linq;
using Core.Helpers;
using EasyWFC.Nodes;
using EasyWFC.Nodes.Generation;
using UnityEngine;

namespace EasyWFC.General.Generation
{
    public class WfcGenerator : IWfcGenerator<WfcNode, WfcCell>
    {
        public List<WfcNode> Nodes { get; private set; }
        public List<WfcCell> Cells { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        public WfcNode[] Output { get; private set; }
        private int _iterations;
        private int _maxIterations = 100;
        private int _currentIteration;

        public WfcGenerator()
        {
        }

        public WfcGenerator(IEnumerable<WfcNode> nodes, int width, int height)
        {
            Nodes = nodes.ToList();
            Width = width;
            Height = height;
        }

        public void SetSource(IEnumerable<WfcNode> nodes, int width, int height)
        {
            Nodes = nodes.ToList();
            Width = width;
            Height = height;
        }

        public void Generate(List<WfcCell> cells)
        {
            InitializeSuperposition(cells);
            HeuristicPick();
        }

        public void Generate(List<WfcCell> cells, out WfcNode[] output)
        {
            Generate(cells);
            output = Output;
        }

        public void Generate(out WfcNode[] output)
        {
            if (_currentIteration++ > _maxIterations)
            {
                MonoBehaviour.print("Max iterations reached");
                output = null;
                _currentIteration = 0;
                return;
            }

            try
            {
                var cells = new List<WfcCell>();
                for (var i = 0; i < Width * Height; i++)
                    cells.Add(new WfcCell(false, Nodes.ConvertAll(node => node).ToArray()));

                InitializeSuperposition(cells);
                HeuristicPick();
                output = Output;
                MonoBehaviour.print($"Finished in {_currentIteration} iterations");
                _currentIteration = 0;
            }
            catch (Exception _)
            {
                Generate(out output);
            }
        }

        public void InitializeSuperposition(List<WfcCell> cells)
        {
            _iterations = 0;
            Cells = cells.ToList();
            Output = new WfcNode[Width * Height];
        }

        public void HeuristicPick()
        {
            while (_iterations < Width * Height)
            {
                var tempCells = new List<WfcCell>(Cells.FindAll(c => !c.IsCollapsed));
                tempCells.Sort((a, b) => a.Entropy - b.Entropy);

                var stopIndex = tempCells.FindIndex(cell => cell.Entropy != tempCells[0].Entropy);
                Debug.Log(tempCells[0].Entropy);
                if (stopIndex != -1) tempCells.RemoveRange(stopIndex, tempCells.Count - stopIndex);
                Collapse(tempCells);
            }
        }

        public void Collapse(List<WfcCell> tempCells)
        {
            var randIndex = UnityEngine.Random.Range(0, tempCells.Count);

            var cellToCollapse = tempCells[randIndex];
            var cellToCollapseIndex = Cells.IndexOf(cellToCollapse);
            var x = cellToCollapseIndex % Width;
            var y = cellToCollapseIndex / Width;

            var snapshot = new WfcSnapshot(cellToCollapse, Cells, y > 0 ? Cells[x + (y - 1) * Width] : null,
                x < Width - 1 ? Cells[x + 1 + y * Width] : null, y < Height - 1 ? Cells[x + (y + 1) * Width] : null,
                x > 0 ? Cells[x - 1 + y * Width] : null);

            var newOptions = cellToCollapse.NodeOptions.ToList();
            newOptions.RemoveAll(option => option.Rules.Any(rule => !rule.Evaluate(snapshot)));
            cellToCollapse.UpdateCell(newOptions.ToArray());

            var weightedRandomPicker = new WeightedRandomPicker<WfcNode>();
            foreach (var node in cellToCollapse.NodeOptions)
                weightedRandomPicker.AddItem(node, (int)(node.Weight * 100));

            var selectedTile = weightedRandomPicker.GetRandomItem();
            cellToCollapse.Collapse(selectedTile);

            var endingTile = cellToCollapse.NodeOptions[0];

            Output[cellToCollapseIndex] = endingTile;
            UpdateGeneration(cellToCollapseIndex);
        }

        private IEnumerable<int> GetNeighborsIndexes(int index)
        {
            var neighbors = new List<int>();

            var x = index % Width;
            var y = index / Width;

            if (y > 0) neighbors.Add(x + (y - 1) * Width);
            if (x < Width - 1) neighbors.Add(x + 1 + y * Width);
            if (y < Height - 1) neighbors.Add(x + (y + 1) * Width);
            if (x > 0) neighbors.Add(x - 1 + y * Width);

            return neighbors;
        }

        public void UpdateGeneration(int collapsedCellIndex)
        {
            // WFC STEPS (EXAMPLE UP CELL)
            // - Current Cell -> Cell that we are collapsing.
            // - Up Cell -> Cell at the top of Current Cell
            // - validOptions -> For each possible option at Up Cell, take all the neighbours of each option at the "Top" that are allowed and save them on validOptions.
            //
            //     currentCellOptions = [Sand,Water,Dirt] (right now all of them are available, this is obtained from "Nodes", which contains all the nodes of the WFC.)
            // currentCell = [Sand,Water, Dirt] (current possible options on the cell, right now all are available as well.)
            // upCell = [Dirt] (the only available option at the up cell)
            //
            // validOptions = upCell[Dirt].ForEach(node => node.Neighbours.Where(neighbour => neighbour.AllowedPosition(Top))).Flatten() (from upCell we take the neighbours at the top of each available option. In this case, we only take the top neighbours of Dirt, which is also Dirt.)
            //
            // validOptions = [Dirt] (from Dirt, in this case dirt can only have dirt at top)
            //
            // currentCellOptions.RemoveAll(option => !validOptions.Contains(option)) (we remove all the options that are not valid for the current cell)
            //
            // currentCellOptions = [Dirt] (we finish with currentCell only being Dirt, and it is correct because Dirt can only have Dirt at top, which is the only available option on upCell)
            //
            // currentCell.UpdateCell(currentCellOptions) (we update the current cell with the new options, in this case, we only have one option, which is Dirt.)

            var newGenerationCells = new List<WfcCell>(Cells);
            var neighborIndexes = GetNeighborsIndexes(collapsedCellIndex);

            //Update the neighbors of the collapsed cell with the new options.

            System.Threading.Tasks.Parallel.ForEach(neighborIndexes, neighborIndex =>
            {
                var currentCell = Cells[neighborIndex];
                if (currentCell.IsCollapsed)
                {
                    newGenerationCells[neighborIndex] = currentCell;
                    return;
                }

                var options = new List<WfcNode>(currentCell.NodeOptions);
                var x = neighborIndex % Width;
                var y = neighborIndex / Width;

                // WfcCell up = null, down = null, left = null, right = null;

                if (y > 0)
                {
                    var up = Cells[x + (y - 1) * Width];
                    var validOptions = up.NodeOptions.SelectMany(possibleOption =>
                        possibleOption.Neighbours.Where(neighbour =>
                                neighbour.GetAllowedPosition(WfcNeighbourPosition.Bottom))
                            .Select(neighbour => neighbour.Node)).ToList();

                    options = options.Intersect(validOptions).ToList();

                    // var snapshot = new WfcSnapshot(currentCell, Cells, up, down, left, right);
                    // options.RemoveAll(option =>
                    //     option.Rules.Any(rule => !rule.Evaluate(snapshot)));
                }

                if (x < Width - 1)
                {
                    var right = Cells[x + 1 + y * Width];
                    var validOptions = right.NodeOptions.SelectMany(possibleOption =>
                            possibleOption.Neighbours.Where(neighbor =>
                                neighbor.GetAllowedPosition(WfcNeighbourPosition.Left)))
                        .Select(neighbor => neighbor.Node)
                        .ToList();

                    options = options.Intersect(validOptions).ToList();

                    // var snapshot = new WfcSnapshot(currentCell, Cells, up, down, left, right);
                    // options.RemoveAll(option =>
                    //     option.Rules.Any(rule => !rule.Evaluate(snapshot)));
                }

                if (y < Height - 1)
                {
                    var down = Cells[x + (y + 1) * Width];
                    var validOptions = down.NodeOptions.SelectMany(possibleOptions =>
                            possibleOptions.Neighbours.Where(neighbor =>
                                neighbor.GetAllowedPosition(WfcNeighbourPosition.Top)))
                        .Select(neighbor => neighbor.Node)
                        .ToList();

                    options = options.Intersect(validOptions).ToList();

                    // var snapshot = new WfcSnapshot(currentCell, Cells, up, down, left, right);
                    // options.RemoveAll(option =>
                    //     option.Rules.Any(rule => !rule.Evaluate(snapshot)));
                }

                if (x > 0)
                {
                    var left = Cells[x - 1 + y * Width];
                    var validOptions = left.NodeOptions.SelectMany(possibleOptions =>
                            possibleOptions.Neighbours.Where(neighbor =>
                                neighbor.GetAllowedPosition(WfcNeighbourPosition.Right)))
                        .Select(neighbor => neighbor.Node)
                        .ToList();

                    options = options.Intersect(validOptions).ToList();

                    // var snapshot = new WfcSnapshot(currentCell, Cells, up, down, left, right);
                    // options.RemoveAll(option =>
                    //     option.Rules.Any(rule => !rule.Evaluate(snapshot)));
                }

                newGenerationCells[neighborIndex].UpdateCell(options.ToArray());
            });

            Cells = newGenerationCells;
            _iterations++;
        }
    }
}