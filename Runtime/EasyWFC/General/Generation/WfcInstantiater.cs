using System;
using System.Collections.Generic;
using System.Linq;
using EasyWFC.Nodes;
using UnityEngine;
using Object = UnityEngine.Object;

namespace EasyWFC.General.Generation
{
    public class WfcInstantiater
    {
        private WfcNode[] _source;
        private int _width;
        private List<GameObject> _instantiatedObjects = new();

        public void InstantiateNodes()
        {
            if (_source == null)
            {
                Debug.LogError("Source is null. Cannot instantiate nodes.");
                return;
            }

            if (_source.Length == 0)
            {
                Debug.LogError("Source is empty. Cannot instantiate nodes.");
                return;
            }

            if (_width == 0)
            {
                Debug.LogError("Width is 0. Cannot instantiate nodes.");
                return;
            }

            float xOffset = 0;
            float zOffset = 0;

            for (var i = 0; i < _source.Length; i++)
            {
                var node = _source[i];

                if (node == null)
                {
                    Debug.Log($"Node at index {i} is null. Skipping.");
                    continue;
                }

                var x = xOffset;
                var y = 0;
                var z = zOffset;

                var gameObject = Object.Instantiate(node.Prefab, new Vector3(x, y, z), Quaternion.identity);
                _instantiatedObjects.Add(gameObject);

                // var children = gameObject.GetComponentsInChildren<Transform>();
                // var roomRenderer = children.First(transform => transform.name == "Room").GetComponent<Renderer>();
                // var upRef = children.First(transform => transform.name == "upRef");
                // var downRef = children.First(transform => transform.name == "downRef");
                // var leftRef = children.First(transform => transform.name == "leftRef");
                // var rightRef = children.First(transform => transform.name == "rightRef");

                // var size = roomRenderer != null ? roomRenderer.bounds.size : Vector3.one;
                var size = Vector3.one;
                xOffset += size.x;
                if ((i + 1) % _width == 0)
                {
                    xOffset = 0;
                    zOffset += size.z;
                }
            }
        }

        public void SetSource(WfcNode[] source, int width)
        {
            _source = source;
            _width = width;
        }

        public void SetSource(IWfcNode[] source, int width)
        {
            _source = Array.ConvertAll(source, node => (WfcNode)node);
            _width = width;
        }

        public void Clear()
        {
            foreach (var instantiatedObject in _instantiatedObjects) Object.Destroy(instantiatedObject);
            _instantiatedObjects.Clear();
        }
    }
}