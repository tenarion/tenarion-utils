using System;
using System.Collections.Generic;
using System.Linq;
using AYellowpaper.SerializedCollections;
using EasyWFC.General.Rules;
using EasyWFC.Nodes;
using UnityEditor;
using UnityEngine;

namespace EasyWFC.General.Generation
{
    [Serializable]
    public class WfcNeighbour
    {
        public WfcNode Node;
        public SerializedDictionary<WfcNeighbourPosition, bool> AllowedPositions = new();

        public void SetAllowedPosition(WfcNeighbourPosition position, bool value)
        {
            AllowedPositions[position] = value;
        }

        public bool GetAllowedPosition(WfcNeighbourPosition position)
        {
            return AllowedPositions.ContainsKey(position) && AllowedPositions[position];
        }
    }
}