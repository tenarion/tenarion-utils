namespace EasyWFC.General.Generation
{
    public enum WfcNeighbourPosition
    {
        None,
        Top,
        Bottom,
        Left,
        Right
    }
}