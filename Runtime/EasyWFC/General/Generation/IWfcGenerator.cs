using System.Collections.Generic;
using EasyWFC.Nodes;
using EasyWFC.Nodes.Generation;

namespace EasyWFC.General.Generation
{
    public interface IWfcGenerator<T1, T2> where T1 : IWfcNode where T2 : IWfcCell
    {
        public List<T1> Nodes { get; }
        public List<T2> Cells { get; }
        public int Width { get; }
        public int Height { get; }
        public T1[] Output { get; }
        void Generate(List<T2> cells);
        void InitializeSuperposition(List<T2> cells);
        void HeuristicPick();
        void Collapse(List<T2> tempCells);
        void UpdateGeneration(int collapsedCellIndex);
    }
}