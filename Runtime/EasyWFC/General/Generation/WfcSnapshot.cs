using System.Collections.Generic;
using System.Linq;
using EasyWFC.Nodes;
using EasyWFC.Nodes.Generation;

namespace EasyWFC.General.Generation
{
    /// <summary>
    /// A snapshot of the current state of the WFC.
    /// </summary>
    public class WfcSnapshot
    {
        public WfcCell CurrentCell { get; }
        public List<WfcCell> Cells { get; }
        public WfcCell Up { get; }
        public WfcCell Down { get; }
        public WfcCell Left { get; }
        public WfcCell Right { get; }

        public WfcSnapshot(WfcCell currentCell, List<WfcCell> cells, WfcCell up, WfcCell down, WfcCell left,
            WfcCell right)
        {
            CurrentCell = currentCell;
            Cells = cells;
            Up = up;
            Down = down;
            Left = left;
            Right = right;
        }

        public int GlobalNumOfOccurrences(WfcNode node, bool collapsedOnly = false)
        {
            var occurrences = collapsedOnly
                ? Cells.Count(cell => cell.IsCollapsed && cell.NodeOptions[0] == node)
                : Cells.Count(cell => cell.NodeOptions.Contains(node));
            return occurrences;
        }

        public int NumOfOccurrences(WfcNode node, bool collapsedOnly = false)
        {
            var upOccurrences =
                Up != null && (collapsedOnly
                    ? Up.IsCollapsed && Up.NodeOptions[0] == node
                    : Up.NodeOptions.Contains(node))
                    ? 1
                    : 0;
            var downOccurrences = Down != null && (collapsedOnly
                ? Down.IsCollapsed && Down.NodeOptions[0] == node
                : Down.NodeOptions.Contains(node))
                ? 1
                : 0;
            var leftOccurrences = Left != null && (collapsedOnly
                ? Left.IsCollapsed && Left.NodeOptions[0] == node
                : Left.NodeOptions.Contains(node))
                ? 1
                : 0;
            var rightOccurrences = Right != null && (collapsedOnly
                ? Right.IsCollapsed && Right.NodeOptions[0] == node
                : Right.NodeOptions.Contains(node))
                ? 1
                : 0;

            var totalOccurrences = upOccurrences + downOccurrences + leftOccurrences + rightOccurrences;
            return totalOccurrences;
        }


        public bool CellContains(WfcNeighbourPosition position, WfcNode node)
        {
            var cell = CellAt(position);
            return cell != null && cell.NodeOptions.Contains(node);
        }

        public bool CellIs(WfcNeighbourPosition position, WfcNode node)
        {
            var cell = CellAt(position);
            return cell is { IsCollapsed: true } && cell.NodeOptions[0] == node;
        }

        public WfcCell CellAt(WfcNeighbourPosition data)
        {
            return data switch
            {
                WfcNeighbourPosition.Top => Up,
                WfcNeighbourPosition.Right => Right,
                WfcNeighbourPosition.Bottom => Down,
                WfcNeighbourPosition.Left => Left,
                _ => null
            };
        }
    }
}
