﻿#include <UnityCG.cginc>
#define TAU UNITY_TWO_PI

float blendColor(float src, float dst, float A, float B, bool sum)
{
    if (sum) return src * A + dst * B;
    return src * A - dst * B;
}

float2 blendColor(float2 src, float2 dst, float A, float B, bool sum)
{
    if (sum) return src * A + dst * B;
    return src * A - dst * B;
}

float3 blendColor(float3 src, float3 dst, float A, float B, bool sum)
{
    if (sum) return src * A + dst * B;
    return src * A - dst * B;
}

float4 blendColor(float4 src, float4 dst, float A, float B, bool sum)
{
    if (sum) return src * A + dst * B;
    return src * A - dst * B;
}

float2 toPolar(float2 cartesian, float radialScale, float lenghtScale)
{
    float distance = length(cartesian) * radialScale;
    float angle = atan2(cartesian.y, cartesian.x) * lenghtScale;
    return float2(distance, angle / TAU);
}

float inverseLerp(float a, float b, float t)
{
    if (a != b)
        return saturate((t - a) / (b - a));
    return 0.0;
}

float2 inverseLerp(float2 a, float2 b, float t)
{
    if (a.x != b.x && a.y != b.y)
        return saturate((t - a) / (b - a));
    return 0.0;
}

float3 inverseLerp(float3 a, float3 b, float t)
{
    if (a.x != b.x && a.y != b.y && a.z != b.z)
        return saturate((t - a) / (b - a));
    return 0.0;
}

float4 inverseLerp(float4 a, float4 b, float t)
{
    if (a.x != b.x && a.y != b.y && a.z != b.z && a.w != b.w)
        return saturate((t - a) / (b - a));
    return 0.0;
}

float normalizeBetween(float x, float minVal, float maxVal)
{
    return saturate((x - minVal) / (maxVal - minVal));
}

float2 rotateUV(float2 uv, float angle, float2 center)
{
    // Translate the UV coordinate to the origin
    uv -= center;

    // Perform the rotation
    float2 rotatedUV;
    rotatedUV.x = uv.x * cos(angle) - uv.y * sin(angle);
    rotatedUV.y = uv.x * sin(angle) + uv.y * cos(angle);

    // Translate the UV coordinate back to its original position
    rotatedUV += center;

    return rotatedUV;
}
