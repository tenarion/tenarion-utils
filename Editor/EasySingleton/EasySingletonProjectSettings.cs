using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using DesignPatterns.EasySingleton;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasySingleton
{
    [InitializeOnLoad]
    internal class EasySingletonProjectSettings
    {
        static EasySingletonProjectSettings()
        {
            var settings = EasySingletonSettings.GetOrCreateSettings();
            var types = TypeCache.GetTypesDerivedFrom(typeof(ISingleton)).Where(type => !type.IsAbstract);

            var newSingletons = types.Where(type => !settings.Singletons.Select(s => s.TypeName).Contains(type.Name));
            var invalidSingletons = settings.Singletons
                .Where(s => !types.Select(type => type.Name).Contains(s.TypeName)).ToList();
            settings.Singletons.AddRange(newSingletons.Select(s => new SingletonData(s, false, false)));
            settings.Singletons.RemoveAll(invalidSingletons.Contains);
            EditorUtility.SetDirty(settings);
            AssetDatabase.SaveAssets();
        }

        [SettingsProvider]
        public static SettingsProvider CreateMyCustomSettingsProvider()
        {
            var provider =
                new SettingsProvider("Project/Easy Utils/EasySingletonProjectSettings", SettingsScope.Project)
                {
                    label = "Easy Singleton",
                    activateHandler = (searchContext, rootElement) =>
                    {
                        var settings = EasySingletonSettings.GetOrCreateSettings();

                        var title = new Label
                        {
                            text = "Easy Singleton Settings"
                        };

                        rootElement.Add(title);

                        var singletonList = new ListView(settings.Singletons, 20)
                        {
                            makeItem = () => new SingletonDataElement(),
                            bindItem = (element, i) =>
                            {
                                var singletonData = settings.Singletons[i];
                                var typeName = element.Q<Label>("type-name");
                                var createOnLoad = element.Q<Toggle>("create-on-load");
                                var createOnGet = element.Q<Toggle>("create-on-get");

                                typeName.text = singletonData.TypeName;
                                createOnLoad.value = singletonData.CreateOnLoad;
                                createOnGet.value = singletonData.CreateOnGet;

                                typeName.RegisterValueChangedCallback(evt =>
                                {
                                    singletonData.TypeName = evt.newValue;
                                    EditorUtility.SetDirty(settings);
                                    AssetDatabase.SaveAssets();
                                });

                                createOnLoad.RegisterValueChangedCallback(evt =>
                                {
                                    singletonData.CreateOnLoad = evt.newValue;
                                    EditorUtility.SetDirty(settings);
                                    AssetDatabase.SaveAssets();
                                });

                                createOnGet.RegisterValueChangedCallback(evt =>
                                {
                                    singletonData.CreateOnGet = evt.newValue;
                                    EditorUtility.SetDirty(settings);
                                    AssetDatabase.SaveAssets();
                                });
                            },
                            showBoundCollectionSize = false,
                            showAlternatingRowBackgrounds = AlternatingRowBackground.All,
                            reorderable = true,
                            reorderMode = ListViewReorderMode.Animated
                        };

                        singletonList.SetMargin(8);
                        singletonList.SetBorderWidth(2);
                        singletonList.SetBorderColor(new Color(0.25f, 0.25f, 0.25f));
                        singletonList.SetBorderRadius(4);

                        rootElement.Add(singletonList);
                    },

                    // Populate the search keywords to enable smart search filtering and label highlighting:
                    keywords = new[] { "Easy Singleton" }
                };

            return provider;
        }
    }

    internal class SingletonDataElement : VisualElement
    {
        public SingletonDataElement()
        {
            var typeName = new Label("Type Name") { name = "type-name" };
            var createOnLoad = new Toggle("Create On Load") { name = "create-on-load" };
            var createOnGet = new Toggle("Create On Get") { name = "create-on-get" };

            style.flexDirection = FlexDirection.Row;
            typeName.style.unityFontStyleAndWeight = FontStyle.Bold;
            typeName.style.marginRight = 10;
            typeName.style.width = 200;

            Add(typeName);
            Add(createOnLoad);
            Add(createOnGet);
        }
    }
}
