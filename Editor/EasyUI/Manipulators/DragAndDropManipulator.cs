using UnityEditor;
using UnityEngine.UIElements;

namespace Editor.EasyUI.Manipulators
{
    public class DragAndDropManipulator<T, TU> : MouseManipulator where T : VisualElement where TU : class
    {
        protected override void RegisterCallbacksOnTarget()
        {
            target.RegisterCallback<DragUpdatedEvent>(OnDragUpdated);
            target.RegisterCallback<DragPerformEvent>(OnDragPerform);
        }

        protected override void UnregisterCallbacksFromTarget()
        {
            target.UnregisterCallback<DragUpdatedEvent>(OnDragUpdated);
            target.UnregisterCallback<DragPerformEvent>(OnDragPerform);
        }

        private void OnDragUpdated(DragUpdatedEvent evt)
        {
            DragAndDrop.visualMode = DragAndDropVisualMode.Generic;
        }

        private void OnDragPerform(DragPerformEvent evt)
        {
            var targetView = target as T ?? target.Q<T>();
            if (targetView == null) return;
            var draggedObjects = DragAndDrop.objectReferences;
            HandleData(targetView, System.Array.ConvertAll(draggedObjects, x => x as TU));
        }

        protected virtual void HandleData(T targetView, TU[] data)
        {
            targetView.userData = data;
        }
    }
}