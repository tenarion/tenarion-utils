using EasyUI.Timeline;
using UnityEditor;

namespace Editor.EasyUI.Timeline
{
    [CustomEditor(typeof(UIVisibilityClip))]
    public class UIVisibilityClipEditor : UnityEditor.Editor
    {
        private SerializedProperty _visible;

        private void OnEnable()
        {
            _visible = serializedObject.FindProperty("Template.Visible");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.PropertyField(_visible);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
