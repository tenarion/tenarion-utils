using EasyUI.Timeline;
using UnityEditor;

namespace Editor.EasyUI.Timeline
{
    [CustomEditor(typeof(UIScaleClip))]
    public class UIScaleClipEditor : UnityEditor.Editor
    {
        private SerializedProperty _scale;

        private void OnEnable()
        {
            _scale = serializedObject.FindProperty("Template.Scale");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.PropertyField(_scale);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
