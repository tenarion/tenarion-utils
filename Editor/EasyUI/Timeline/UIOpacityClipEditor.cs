using EasyUI.Timeline;
using UnityEditor;

namespace Editor.EasyUI.Timeline
{
    [CustomEditor(typeof(UIOpacityClip))]
    public class UIOpacityClipEditor : UnityEditor.Editor
    {
        private SerializedProperty _opacity;

        private void OnEnable()
        {
            _opacity = serializedObject.FindProperty("Template.Opacity");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.PropertyField(_opacity);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
