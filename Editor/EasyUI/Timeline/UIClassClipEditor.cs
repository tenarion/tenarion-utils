using EasyUI.Timeline;
using UnityEditor;

namespace Editor.EasyUI.Timeline
{
    [CustomEditor(typeof(UIClassClip))]
    public class UIClassClipEditor : UnityEditor.Editor
    {
        private SerializedProperty _className;

        private void OnEnable()
        {
            _className = serializedObject.FindProperty("Template.ClassName");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.PropertyField(_className);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
