using EasyUI.Timeline;
using UnityEditor;

namespace Editor.EasyUI.Timeline
{
    [CustomEditor(typeof(UIRotationClip))]
    public class UIRotationClipEditor : UnityEditor.Editor
    {
        private SerializedProperty _rotation;

        private void OnEnable()
        {
            _rotation = serializedObject.FindProperty("Template.Rotation");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.PropertyField(_rotation);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
