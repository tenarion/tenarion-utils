using EasyUI.Timeline;
using UnityEditor;

namespace Editor.EasyUI.Timeline
{
    [CustomEditor(typeof(UIDisplayClip))]
    public class UIDisplayClipEditor : UnityEditor.Editor
    {
        private SerializedProperty _display;

        private void OnEnable()
        {
            _display = serializedObject.FindProperty("Template.Display");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.PropertyField(_display);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
