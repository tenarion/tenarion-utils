using EasyUI.Timeline;
using UnityEditor;

namespace Editor.EasyUI.Timeline
{
    [CustomEditor(typeof(UIPositionClip))]
    public class UIPositionClipEditor : UnityEditor.Editor
    {
        private SerializedProperty _position;

        private void OnEnable()
        {
            _position = serializedObject.FindProperty("Template.Position");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.PropertyField(_position);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
