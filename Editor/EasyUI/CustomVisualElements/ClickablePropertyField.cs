using System;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Editor.EasyUI.CustomVisualElements
{
    public class ClickablePropertyField : PropertyField
    {
        public ClickablePropertyField(SerializedProperty property, Action callback) : base(property)
        {
            RegisterCallback<MouseDownEvent>(evt =>
            {
                callback?.Invoke();
                evt.StopPropagation();
            });
        }
    }
}