using System;
using System.Linq;
using EasyUI.Interfaces;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using GUIContent = UnityEngine.GUIContent;

namespace Editor.EasyUI.CustomVisualElements
{
    /// <summary>
    /// The visual element responsible for creating a menu trough reflection.
    /// </summary>
    /// <typeparam name="T1">The type of the options to be selected.</typeparam>
    /// <typeparam name="T2">The type the selected option will return when selected.</typeparam>
    public class ReflectionTypeSelector<T1> : VisualElement
    {
        public event Action<Type> OnItemSelected;

        private VisualElement _presentationElement;
        private Button _selectionButton;

        public ReflectionTypeSelector()
        {
            _presentationElement = new VisualElement
            {
                style =
                {
                    flexDirection = FlexDirection.Column
                }
            };

            var label = new Label
            {
                text = $"{typeof(T1).Name} selector",
                style =
                {
                    alignSelf = Align.Center
                }
            };

            _selectionButton = new Button
            {
                text = $"Select new {typeof(T1).Name}",
                style =
                {
                    alignSelf = Align.Center
                }
            };

            // Create a new GenericMenu.

            _selectionButton.clicked += () =>
            {
                var menu = new GenericMenu();

                // Get all types that derive from WfcCondition.
                var optionTypes = AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(assembly => assembly.GetTypes())
                    .Where(type => type.IsSubclassOf(typeof(T1)) && !type.IsAbstract);

                // Add an item for each condition type.
                foreach (var optionType in optionTypes)
                {
                    var category = "Uncategorized";
                    var attributes = Attribute.GetCustomAttributes(optionType);
                    foreach (var attribute in attributes)
                        if (attribute is ReflectionTypeCategoryAttribute typeCategory &&
                            !string.IsNullOrEmpty(typeCategory.CategoryPath))
                            category = typeCategory.CategoryPath;
                    menu.AddItem(new GUIContent($"{category}/{optionType.Name}"), false,
                        () => { OnItemSelected?.Invoke(optionType); });
                }

                // Show the menu under the button.
                menu.DropDown(_selectionButton.worldBound);
            };

            _presentationElement.Add(label);
            _presentationElement.Add(_selectionButton);
            Add(_presentationElement);
        }


        /// <summary>
        /// This constructor gives you full control to define how the selector is visualized.
        /// </summary>
        /// <param name="presentationElement">The <see cref="VisualElement"/> to be displayed.</param>
        /// <param name="selectionButton">The <see cref="Button"/> that will display the menu on press.</param>
        /// <remarks><paramref name="selectionButton"/> will include functionality automatically.
        /// <paramref name="presentationElement"/> should also contain <paramref name="selectionButton"/>, otherwise the button won't be accesible.</remarks>
        public ReflectionTypeSelector(VisualElement presentationElement, Button selectionButton)
        {
            // Create a new GenericMenu.
            _presentationElement = presentationElement;
            _selectionButton = selectionButton;

            _selectionButton.clicked += () =>
            {
                var menu = new GenericMenu();

                // Get all types that derive from WfcCondition.
                var optionTypes = AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(assembly => assembly.GetTypes())
                    .Where(type => type.IsSubclassOf(typeof(T1)) && !type.IsAbstract);

                // Add an item for each condition type.
                foreach (var optionType in optionTypes)
                {
                    var category = "Uncategorized";
                    var attributes = Attribute.GetCustomAttributes(optionType);
                    foreach (var attribute in attributes)
                        if (attribute is ReflectionTypeCategoryAttribute typeCategory &&
                            !string.IsNullOrEmpty(typeCategory.CategoryPath))
                            category = typeCategory.CategoryPath;

                    menu.AddItem(new GUIContent($"{category}/{optionType.Name}"), false,
                        () => { OnItemSelected?.Invoke(optionType); });

                    // Show the menu under the button.
                    menu.DropDown(_selectionButton.worldBound);
                }
            };
            Add(_presentationElement);
        }

        public void RefreshVisualDisplay()
        {
            Clear();
            Add(_presentationElement);
        }
    }
}