using System.Collections.Generic;
using System.Linq;
using EasyIdentification;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Editor.EasyIdentification
{
    [InitializeOnLoad]
    public static class IdHierarchyMonitor
    {
        private static List<IIdentifiable> _lastIdentifiables = new();
        private static IdManager _idManager;

        static IdHierarchyMonitor()
        {
            EditorApplication.quitting += OnEditorQuitting;
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void OnSceneLoad()
        {
            EditorApplication.hierarchyChanged -= OnHierarchyChanged;
            EditorApplication.hierarchyChanged += OnHierarchyChanged;
            _idManager = Object.FindFirstObjectByType<IdManager>(FindObjectsInactive.Include);
            if (_idManager == null)
                Debug.LogWarning("[ID HIERARCHY MONITOR] No IdManager found in the scene. Monitoring disabled.");
            else if (!_idManager.MonitorHierarchy) Debug.Log("[ID HIERARCHY MONITOR] Monitoring disabled.");
            else Debug.Log("[ID HIERARCHY MONITOR] Monitoring enabled.");
            _lastIdentifiables = Object
                .FindObjectsByType<MonoBehaviour>(FindObjectsInactive.Include, FindObjectsSortMode.None)
                .OfType<IIdentifiable>().ToList();
            _idManager = Object.FindFirstObjectByType<IdManager>(FindObjectsInactive.Include);
            _lastIdentifiables = Object
                .FindObjectsByType<MonoBehaviour>(FindObjectsInactive.Include, FindObjectsSortMode.None)
                .OfType<IIdentifiable>().ToList();
        }

        private static void OnEditorQuitting()
        {
            EditorApplication.hierarchyChanged -= OnHierarchyChanged;
            EditorApplication.quitting -= OnEditorQuitting;
        }

        private static void OnHierarchyChanged()
        {
            if (Application.isPlaying) return;
            if (!_idManager)
            {
                _idManager = Object.FindFirstObjectByType<IdManager>(FindObjectsInactive.Include);
                if (_idManager == null) return;
                Debug.Log("[ID HIERARCHY MONITOR] IdManager found. Monitoring enabled.");
            }

            if (!_idManager.MonitorHierarchy) return;

            var identifiables = Object
                .FindObjectsByType<MonoBehaviour>(FindObjectsInactive.Include, FindObjectsSortMode.None)
                .OfType<IIdentifiable>().ToList();
            _lastIdentifiables = _lastIdentifiables.Where(x => x != null).ToList();
            var newIdentifiables = identifiables.Except(_lastIdentifiables).ToList();
            var removedIdentifiables = _lastIdentifiables.Except(identifiables).ToList();

            if (removedIdentifiables.Count > 0 && _idManager.Verbose)
                Debug.Log(
                    $"[ID HIERARCHY MONITOR] Removed identifiables: {string.Join(", ", removedIdentifiables.Select(x => x.Id))}");

            foreach (var identifiable in newIdentifiables)
            {
                var behaviour = identifiable as MonoBehaviour;

                if (_lastIdentifiables.Any(x => x.Id == identifiable.Id)) identifiable.Id = string.Empty;
                _idManager.RegisterId(identifiable);
                IdManagerEditorUtilities.SetDirty(behaviour);
            }

            foreach (var identifiable in removedIdentifiables) _idManager.EraseId(identifiable);

            if (newIdentifiables.Count > 0 && _idManager.Verbose)
                Debug.Log(
                    $"[ID HIERARCHY MONITOR] New identifiables: {string.Join(", ", newIdentifiables.Select(x => x.Id))}");


            _lastIdentifiables = identifiables.ToList();
        }
    }
}
