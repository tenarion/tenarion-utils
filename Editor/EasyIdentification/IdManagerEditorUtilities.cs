using UnityEditor;
using UnityEngine;

namespace Editor.EasyIdentification
{
    public static class IdManagerEditorUtilities
    {
        public static void SetDirty(Component component)
        {
            EditorUtility.SetDirty(component);
            if (PrefabUtility.IsPartOfPrefabInstance(component))
                PrefabUtility.RecordPrefabInstancePropertyModifications(component);
        }
    }
}