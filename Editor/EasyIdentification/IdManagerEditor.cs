using System.Linq;
using EasyIdentification;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace Editor.EasyIdentification
{
    [CustomEditor(typeof(IdManager))]
    public class IdManagerEditor : UnityEditor.Editor
    {
        private VisualTreeAsset _visualTreeAsset;
        private VisualTreeAsset _idItemAsset;

        public override VisualElement CreateInspectorGUI()
        {
            _visualTreeAsset = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(
                "Packages/com.tenarion.easyutils/Editor/EasyIdentification/Resources/IdManagerUxml.uxml");
            _idItemAsset =
                AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(
                    "Packages/com.tenarion.easyutils/Editor/EasyIdentification/Resources/IdListItem.uxml");
            var root = _visualTreeAsset.CloneTree();
            var idManager = (IdManager)target;

            var idList = root.Q<ListView>("id-list");
            var generateIdsButton = root.Q<Button>("generate-button");
            var eraseIdsButton = root.Q<Button>("erase-button");
            var debugEraseIdsButton = root.Q<Button>("debug-erase-button");

            idList.itemsSource = idManager.SavedIds.ToList();
            idList.makeItem = () => _idItemAsset.CloneTree();
            idList.bindItem = (element, i) =>
            {
                var item = idManager.SavedIds.Keys.ToList()[i];
                var idItem = element.Q<Label>("id-label");
                var nameItem = element.Q<Label>("name-label");
                idItem.text = item.Id;
                nameItem.text = item.Name;
            };

            idList.selectionChanged += _ =>
            {
                var selectedObject = idManager.SavedIds.Keys.ToList()[idList.selectedIndex];
                var behaviours = FindObjectsByType<MonoBehaviour>(FindObjectsInactive.Include, FindObjectsSortMode.None);
                foreach (var behaviour in behaviours)
                {
                    if (behaviour is not IIdentifiable identifiable) continue;
                    if (identifiable.Id != selectedObject.Id) continue;

                    var gameObject = behaviour.gameObject;
                    Selection.activeTransform = gameObject.transform;
                    SceneView.lastActiveSceneView.FrameSelected();
                }
            };

            generateIdsButton.clicked += () =>
            {
                var registeredBehaviours = idManager.RegisterIds();
                if (idList.itemsSource != null)
                    idList.itemsSource = idManager.SavedIds.ToList();
                idList.RefreshItems();

                foreach (var behaviour in registeredBehaviours) IdManagerEditorUtilities.SetDirty(behaviour);
                EditorSceneManager.SaveScene(SceneManager.GetActiveScene());
                EditorUtility.SetDirty(idManager);
            };

            eraseIdsButton.clicked += () =>
            {
                var removedSaveables = idManager.EraseIds();
                if (idList.itemsSource != null)
                    idList.itemsSource = idManager.SavedIds.ToList();
                idList.RefreshItems();

                foreach (var behaviour in removedSaveables) IdManagerEditorUtilities.SetDirty(behaviour);

                EditorSceneManager.SaveScene(SceneManager.GetActiveScene());
                EditorUtility.SetDirty(idManager);
            };

            debugEraseIdsButton.clicked += () =>
            {
                var removedSaveables = idManager.EraseIds_Debug();
                idList.itemsSource.Clear();
                idList.RefreshItems();

                foreach (var behaviour in removedSaveables) IdManagerEditorUtilities.SetDirty(behaviour);

                EditorSceneManager.SaveScene(SceneManager.GetActiveScene());
                EditorUtility.SetDirty(idManager);
            };


            return root;
        }

        public override bool UseDefaultMargins()
        {
            return false;
        }
    }
}
