using EasyIdentification;
using UnityEditor;
using UnityEngine;

namespace Editor.EasyIdentification
{
    [InitializeOnLoad]
    public static class IdContextMenu
    {
        private static IdManager _idManager;

        static IdContextMenu()
        {
            TryGetIdManager();
        }

        [MenuItem("CONTEXT/MonoBehaviour/Assign Id")]
        public static void AssignId(MenuCommand menuCommand)
        {
            TryGetIdManager();
            if (menuCommand.context is not IIdentifiable identifiable) return;
            _idManager.RegisterId(identifiable);
            IdManagerEditorUtilities.SetDirty(identifiable as Component);
        }

        [MenuItem("CONTEXT/MonoBehaviour/Erase Id")]
        public static void EraseId(MenuCommand menuCommand)
        {
            TryGetIdManager();
            if (menuCommand.context is not IIdentifiable identifiable) return;
            _idManager.EraseId(identifiable);
            IdManagerEditorUtilities.SetDirty(identifiable as Component);
        }

        [MenuItem("CONTEXT/MonoBehaviour/Assign Id", true)]
        public static bool ValidateAssignId(MenuCommand menuCommand)
        {
            return menuCommand.context is IIdentifiable identifiable && string.IsNullOrEmpty(identifiable.Id);
        }

        [MenuItem("CONTEXT/MonoBehaviour/Erase Id", true)]
        public static bool ValidateEraseId(MenuCommand menuCommand)
        {
            return menuCommand.context is IIdentifiable identifiable && !string.IsNullOrEmpty(identifiable.Id) &&
                   !identifiable.LockId;
        }

        private static void TryGetIdManager()
        {
            if (_idManager != null) return;
            _idManager = Object.FindFirstObjectByType<IdManager>(FindObjectsInactive.Include);
            if (_idManager == null)
                Debug.LogWarning("[ID CONTEXT MENU] No IdManager found in the scene. Context menu disabled.");
            else Debug.Log("[ID CONTEXT MENU] IdManager found. Context menu enabled.");
        }
    }
}
