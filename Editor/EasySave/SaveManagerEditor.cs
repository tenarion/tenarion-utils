using System.Linq;
using EasyIdentification;
using EasySave;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasySave
{
    [CustomEditor(typeof(SaveManager))]
    public class SaveManagerEditor : UnityEditor.Editor
    {
        private ListView _registeredSavableList;
        private int _selectedIndex;

        public override VisualElement CreateInspectorGUI()
        {
            var root = new VisualElement();
            var saveManager = (SaveManager)target;

            var layout = new VisualElement
            {
                style =
                {
                    flexDirection = FlexDirection.Column,
                    justifyContent = Justify.SpaceBetween
                }
            };

            var topLayout = new VisualElement
            {
                style =
                {
                    flexDirection = FlexDirection.Row,
                    justifyContent = Justify.Center
                }
            };

            var buttonLayout = new VisualElement
            {
                style =
                {
                    flexDirection = FlexDirection.Row,
                    justifyContent = Justify.SpaceBetween,
                    alignSelf = Align.Center
                }
            };

            _registeredSavableList = new ListView
            {
                itemsSource = saveManager.SavedUIDs.ToList(),
                makeItem = () => new SavableVisualElement(),
                bindItem = (element, i) => (element as SavableVisualElement)!.SetValues(saveManager.SavedUIDs[i]),
                fixedItemHeight = 15,
                reorderable = true,
                reorderMode = ListViewReorderMode.Animated
            };

            _registeredSavableList.selectionChanged += objects =>
            {
                var selectedObject = saveManager.SavedUIDs[
                    saveManager.SavedUIDs.IndexOf((IdManager.SaveableData)objects.First())];
                var behaviours = FindObjectsByType<MonoBehaviour>(FindObjectsInactive.Include, FindObjectsSortMode.None);
                foreach (var behaviour in behaviours)
                {
                    if (!behaviour.TryGetComponent(out ISaveable saveable)) continue;
                    if (saveable.Id != selectedObject.Id) continue;

                    var gameObject = behaviour.gameObject;
                    Selection.activeTransform = gameObject.transform;
                    SceneView.lastActiveSceneView.FrameSelected();
                }
            };

            var verboseCheck = new Toggle("Verbose")
            {
                value = saveManager.Verbose,
                style =
                {
                    flexDirection = FlexDirection.Column
                }
            };

            verboseCheck.RegisterValueChangedCallback(evt =>
            {
                saveManager.Verbose = evt.newValue;
                EditorUtility.SetDirty(saveManager);
            });

            topLayout.Add(verboseCheck);

            layout.Add(topLayout);
            if (saveManager.SavedUIDs.Count > 0) layout.Add(_registeredSavableList);

            root.Add(layout);

            return root;
        }
    }
}
