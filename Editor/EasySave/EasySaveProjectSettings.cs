using System.Collections.Generic;
using EasySave;
using Editor.EasyUI.CustomVisualElements;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;

namespace Editor.EasySave
{
    internal static class EasySaveSettingsProvider
    {
        [SettingsProvider]
        public static SettingsProvider CreateMyCustomSettingsProvider()
        {
            // First parameter is the path in the Settings window.
            // Second parameter is the scope of this setting: it only appears in the Settings window for the Project scope.
            var provider = new SettingsProvider("Project/Easy Utils/EasySaveProjectSettings", SettingsScope.Project)
            {
                label = "Easy Save",
                // activateHandler is called when the user clicks on the Settings item in the Settings window.
                activateHandler = (searchContext, rootElement) =>
                {
                    var settings = EasySaveSettings.GetSerializedSettings();

                    var logo = new Image()
                    {
                        scaleMode = ScaleMode.ScaleToFit,
                        image = AssetDatabase.LoadAssetAtPath(
                            "Packages/com.tenarion.easyutils/Editor/EasySave/Resources/EasySaveLogo.png",
                            typeof(Texture2D)) as Texture2D,
                        style =
                        {
                            maxHeight = 150,
                            marginTop = new StyleLength(new Length(5, LengthUnit.Percent))
                        }
                    };

                    var properties = new VisualElement()
                    {
                        style =
                        {
                            flexDirection = FlexDirection.Row,
                            marginLeft = new StyleLength(new Length(5, LengthUnit.Percent)),
                            marginRight = new StyleLength(new Length(5, LengthUnit.Percent))
                        }
                    };
                    properties.AddToClassList("property-list");


                    var saveFilePath = new ClickablePropertyField(settings.FindProperty("SaveFilePath"),
                        () => { Selection.SetActiveObjectWithContext(settings.targetObject, settings.targetObject); });
                    var pathSelect = new Button(() =>
                    {
                        var path = EditorUtility.OpenFolderPanel("Load png Textures", "", "");
                        //edit current saveFilePath value with new path
                        settings.FindProperty("SaveFilePath").stringValue = path;
                        settings.ApplyModifiedProperties();
                    })
                    {
                        text = "Select Path"
                    };

                    var configFilePath = new ClickablePropertyField(settings.FindProperty("SettingsFilePath"),
                        () => { Selection.SetActiveObjectWithContext(settings.targetObject, settings.targetObject); });
                    var configPathSelect = new Button(() =>
                    {
                        var path = EditorUtility.OpenFolderPanel("Load png Textures", "", "");
                        //edit current saveFilePath value with new path
                        settings.FindProperty("SettingsFilePath").stringValue = path;
                        settings.ApplyModifiedProperties();
                    })
                    {
                        text = "Select Path"
                    };

                    var autoSaveEnabled = new Toggle("Enabled")
                    {
                        value = settings.FindProperty("EnableAutoSave").boolValue
                    };

                    autoSaveEnabled.RegisterValueChangedCallback(evt =>
                    {
                        settings.FindProperty("EnableAutoSave").boolValue = evt.newValue;
                        settings.ApplyModifiedProperties();
                    });

                    var autoSaveTimer = new FloatField("Time between autosaves")
                    {
                        value = settings.FindProperty("AutoSaveTimer").floatValue
                    };

                    autoSaveTimer.RegisterValueChangedCallback(evt =>
                    {
                        settings.FindProperty("AutoSaveTimer").floatValue = evt.newValue;
                        settings.ApplyModifiedProperties();
                    });

                    var autoSaveContainer = new VisualElement
                    {
                        style =
                        {
                            flexDirection = FlexDirection.Row
                        }
                    };

                    var saveContainer = new VisualElement();
                    var configContainer = new VisualElement();

                    saveContainer.Add(saveFilePath);
                    saveContainer.Add(pathSelect);

                    autoSaveContainer.Add(autoSaveEnabled);
                    autoSaveContainer.Add(autoSaveTimer);

                    configContainer.Add(configFilePath);
                    configContainer.Add(configPathSelect);

                    properties.Add(saveContainer);
                    properties.Add(configContainer);

                    var container = new VisualElement
                    {
                        style =
                        {
                            flexDirection = FlexDirection.Column,
                            justifyContent = Justify.Center,
                            height = new StyleLength(new Length(100, LengthUnit.Percent))
                        }
                    };

                    var innerContainer = new VisualElement
                    {
                        style =
                        {
                            flexDirection = FlexDirection.Column,
                            justifyContent = Justify.SpaceBetween,
                            height = new StyleLength(new Length(80, LengthUnit.Percent))
                        }
                    };


                    innerContainer.Add(logo);
                    innerContainer.Add(autoSaveContainer);
                    innerContainer.Add(properties);

                    container.Add(innerContainer);


                    rootElement.Add(container);
                    rootElement.Bind(settings);
                },

                // Populate the search keywords to enable smart search filtering and label highlighting:
                keywords = new HashSet<string>(new[] { "Number", "Some String" })
            };

            return provider;
        }
    }
}
