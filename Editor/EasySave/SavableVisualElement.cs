using System;
using EasyIdentification;
using UnityEngine.UIElements;

namespace Editor.EasySave
{
    public class SavableVisualElement : VisualElement
    {
        public IdManager.SaveableData SaveableData;

        // private Label _typeLabel = new ("Class");
        private Label _nameLabel = new("Name");
        private Label _idLabel = new("ID");

        public SavableVisualElement()
        {
            var layout = new VisualElement
            {
                style =
                {
                    flexDirection = FlexDirection.Row,
                    justifyContent = Justify.SpaceBetween,
                    width = new StyleLength(new Length(80, LengthUnit.Percent)),
                    alignSelf = Align.Center
                }
            };

            // layout.Add(_typeLabel);
            layout.Add(_nameLabel);
            layout.Add(_idLabel);

            Add(layout);
        }

        public SavableVisualElement(IdManager.SaveableData saveableData) : this()
        {
            SetValues(saveableData);
        }

        public void SetValues(IdManager.SaveableData saveableData)
        {
            SaveableData = saveableData;
            _idLabel.text = saveableData.Id.Substring(0, Math.Min(saveableData.Id.Length, 10)) + "...";
            _nameLabel.text = saveableData.Name;
            // _typeLabel.text = saveableData.Type.ToString();
        }
    }
}
