using System;
using DesignPatterns.EasyBT;
using DesignPatterns.EasyBT.Actions;
using Unity.Profiling;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyBT
{
    public class BehaviourTreeEditorWindow : EditorWindow
    {
        [Serializable]
        public class PendingScriptCreate
        {
            public bool PendingCreate;
            public string ScriptName = "";
            public string SourceGuid = "";
            public bool IsSourceParent;
            public Vector2 NodePosition;

            public void Reset()
            {
                PendingCreate = false;
                ScriptName = "";
                SourceGuid = "";
                IsSourceParent = false;
                NodePosition = Vector2.zero;
            }
        }

        public class BehaviourTreeEditorAssetModificationProcessor : AssetModificationProcessor
        {
            private static AssetDeleteResult OnWillDeleteAsset(string path, RemoveAssetOptions opt)
            {
                if (HasOpenInstances<BehaviourTreeEditorWindow>())
                {
                    var wnd = GetWindow<BehaviourTreeEditorWindow>();
                    wnd.ClearIfSelected(path);
                }

                return AssetDeleteResult.DidNotDelete;
            }
        }

        private static readonly ProfilerMarker EditorUpdate = new("BehaviourTree.EditorUpdate");
        public static BehaviourTreeEditorWindow Instance;
        public BehaviourTreeProjectSettings Settings;
        public VisualTreeAsset BehaviourTreeXml;
        public VisualTreeAsset NodeXml;
        public StyleSheet BehaviourTreeStyle;
        public TextAsset ScriptTemplateActionNode;
        public TextAsset ScriptTemplateConditionNode;
        public TextAsset ScriptTemplateCompositeNode;
        public TextAsset ScriptTemplateDecoratorNode;

        public BehaviourTreeView TreeView;
        public InspectorView InspectorView;
        public BlackboardView BlackboardView;
        public OverlayView OverlayView;
        public ToolbarMenu ToolbarMenu;
        public Label VersionLabel;
        public NewScriptDialogView NewScriptDialog;
        public ToolbarBreadcrumbs Breadcrumbs;

        [SerializeField] public PendingScriptCreate PendingScriptCreation = new();

        [HideInInspector] public BehaviourTree Tree;
        public SerializedBehaviourTree Serializer;

        [MenuItem("Easy Utils/AI/Behaviour Tree Editor")]
        public static void OpenWindow()
        {
            var wnd = GetWindow<BehaviourTreeEditorWindow>();
            wnd.titleContent = new GUIContent("BehaviourTreeEditor");
            wnd.minSize = new Vector2(800, 600);
        }

        public static void OpenWindow(BehaviourTree tree)
        {
            var wnd = GetWindow<BehaviourTreeEditorWindow>();
            wnd.titleContent = new GUIContent("BehaviourTreeEditor");
            wnd.minSize = new Vector2(800, 600);
            wnd.SelectNewTree(tree);
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceId, int line)
        {
            if (Selection.activeObject is BehaviourTree)
            {
                OpenWindow(Selection.activeObject as BehaviourTree);
                return true;
            }

            return false;
        }

        public void CreateGUI()
        {
            Instance = this;
            Settings = BehaviourTreeProjectSettings.GetOrCreateSettings();

            // Each editor window contains a root VisualElement object
            var root = rootVisualElement;

            // Import UXML
            var visualTree = BehaviourTreeXml;
            visualTree.CloneTree(root);

            // A stylesheet can be added to a VisualElement.
            // The style will be applied to the VisualElement and all of its children.
            var styleSheet = BehaviourTreeStyle;
            root.styleSheets.Add(styleSheet);

            // Main treeview
            TreeView = root.Q<BehaviourTreeView>();

            var treeViewWrapper = root.Q<VisualElement>("tree-view-wrapper");

            InspectorView = new InspectorView();
            var inspectorPanel = FloatingPanel.Create(InspectorView, treeViewWrapper, "Inspector", FloatingPanel.DefaultPosition.TopRight);
            inspectorPanel.IsCollapsable = true;
            treeViewWrapper.Add(inspectorPanel);

            BlackboardView = new BlackboardView();
            var blackboardPanel = FloatingPanel.Create(BlackboardView, treeViewWrapper, "Blackboard");
            blackboardPanel.IsCollapsable = true;
            treeViewWrapper.Add(blackboardPanel);

            ToolbarMenu = root.Q<ToolbarMenu>();
            OverlayView = root.Q<OverlayView>("OverlayView");
            NewScriptDialog = root.Q<NewScriptDialogView>("NewScriptDialogView");
            Breadcrumbs = root.Q<ToolbarBreadcrumbs>("breadcrumbs");
            VersionLabel = root.Q<Label>("Version");

            TreeView.styleSheets.Add(BehaviourTreeStyle);

            // Toolbar assets menu
            ToolbarMenu.RegisterCallback<MouseEnterEvent>(evt =>
            {
                // Refresh the menu options just before it's opened (on mouse enter)
                ToolbarMenu.menu.MenuItems().Clear();
                var behaviourTrees = EditorUtility.GetAssetPaths<BehaviourTree>();
                behaviourTrees.ForEach(path =>
                {
                    var fileName = System.IO.Path.GetFileName(path);
                    ToolbarMenu.menu.AppendAction($"{fileName}", a =>
                    {
                        var tree = AssetDatabase.LoadAssetAtPath<BehaviourTree>(path);
                        SelectNewTree(tree);
                    });
                });
                if (EditorApplication.isPlaying)
                {
                    ToolbarMenu.menu.AppendSeparator();

                    var behaviourTreeInstances =
                        UnityEngine.Resources.FindObjectsOfTypeAll(typeof(BehaviourTreeInstance));
                    foreach (var instance in behaviourTreeInstances)
                    {
                        var behaviourTreeInstance = instance as BehaviourTreeInstance;
                        var gameObject = behaviourTreeInstance.gameObject;
                        if (behaviourTreeInstance != null && gameObject.scene != null && gameObject.scene.name != null)
                        {
                            ToolbarMenu.menu.AppendAction(
                                $"{gameObject.name} [{behaviourTreeInstance.BehaviourTree.name}]", a =>
                                {
                                    SelectNewTree(behaviourTreeInstance.RuntimeTree);
                                    Selection.activeObject = gameObject;
                                });
                        }
                    }
                }

                ToolbarMenu.menu.AppendSeparator();
                ToolbarMenu.menu.AppendAction("New Tree...", a => OnToolbarNewAsset());
            });

            // Version label
            var packageManifest = EditorUtility.GetPackageManifest();
            if (packageManifest != null)
            {
                VersionLabel.text = $"v {packageManifest.Version}";
            }

            TreeView.OnNodeSelected -= OnNodeSelectionChanged;
            TreeView.OnNodeSelected += OnNodeSelectionChanged;

            // Overlay view
            OverlayView.OnTreeSelected -= SelectTree;
            OverlayView.OnTreeSelected += SelectTree;

            // New Script Dialog
            NewScriptDialog.style.visibility = Visibility.Hidden;

            if (Serializer == null)
            {
                OverlayView.Show();
            }
            else
            {
                SelectTree(Serializer.Tree);
            }

            // Create new node for any scripts just created coming back from a compile.
            if (PendingScriptCreation != null && PendingScriptCreation.PendingCreate)
            {
                CreatePendingScriptNode();
            }
        }

        private void CreatePendingScriptNode()
        {
            // #TODO: Unify this with CreateNodeWindow.CreateNode

            var source = TreeView.GetNodeByGuid(PendingScriptCreation.SourceGuid) as NodeView;
            var nodeType = Type.GetType($"{PendingScriptCreation.ScriptName}, Assembly-CSharp");
            if (nodeType != null)
            {
                NodeView createdNode;
                if (source != null)
                {
                    createdNode = PendingScriptCreation.IsSourceParent
                        ? TreeView.CreateNode(nodeType, PendingScriptCreation.NodePosition, source)
                        : TreeView.CreateNodeWithChild(nodeType, PendingScriptCreation.NodePosition, source);
                }
                else
                {
                    createdNode = TreeView.CreateNode(nodeType, PendingScriptCreation.NodePosition, null);
                }

                TreeView.SelectNode(createdNode);
            }

            PendingScriptCreation.Reset();
        }

        private void OnUndoRedo()
        {
            if (Tree != null)
            {
                Serializer.SerializedObject.Update();
                TreeView.PopulateView(Serializer);
            }
        }

        private void OnEnable()
        {
            Undo.undoRedoPerformed -= OnUndoRedo;
            Undo.undoRedoPerformed += OnUndoRedo;

            EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
        }

        private void OnDisable()
        {
            EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
        }

        private void OnPlayModeStateChanged(PlayModeStateChange obj)
        {
            switch (obj)
            {
                case PlayModeStateChange.EnteredEditMode:
                    EditorApplication.delayCall += OnSelectionChange;
                    break;
                case PlayModeStateChange.ExitingEditMode:
                    break;
                case PlayModeStateChange.EnteredPlayMode:
                    EditorApplication.delayCall += OnSelectionChange;
                    break;
                case PlayModeStateChange.ExitingPlayMode:
                    InspectorView?.ClearView();
                    break;
            }
        }

        private void OnSelectionChange()
        {
            if (Selection.activeGameObject)
            {
                var runner = Selection.activeGameObject.GetComponent<BehaviourTreeInstance>();
                if (runner)
                {
                    SelectNewTree(runner.RuntimeTree);
                }
            }
        }

        private void SelectNewTree(BehaviourTree tree)
        {
            ClearBreadcrumbs();
            SelectTree(tree);
        }

        private void SelectTree(BehaviourTree newTree)
        {
            // If tree view is null the window is probably unfocused
            if (TreeView == null)
            {
                return;
            }

            if (!newTree)
            {
                ClearSelection();
                return;
            }

            if (newTree != Tree)
            {
                ClearSelection();
            }

            Tree = newTree;
            Serializer = new SerializedBehaviourTree(newTree);

            var childCount = Breadcrumbs.childCount;
            Breadcrumbs.PushItem($"{Serializer.Tree.name}", () => PopToSubtree(childCount, newTree));

            OverlayView?.Hide();
            TreeView?.PopulateView(Serializer);
            BlackboardView?.Bind(Serializer);
        }

        private void ClearSelection()
        {
            Tree = null;
            Serializer = null;
            InspectorView?.ClearView();
            TreeView?.ClearView();
            BlackboardView?.ClearView();
            OverlayView?.Show();
        }

        private void ClearIfSelected(string path)
        {
            if (Serializer == null)
            {
                return;
            }

            if (AssetDatabase.GetAssetPath(Serializer.Tree) == path)
            {
                // Need to delay because this is called from a will delete asset callback
                EditorApplication.delayCall += () => { SelectTree(null); };
            }
        }

        private void OnNodeSelectionChanged(NodeView node)
        {
            InspectorView.UpdateSelection(Serializer, node);
        }

        private void OnInspectorUpdate()
        {
            if (Application.isPlaying)
            {
                EditorUpdate.Begin();
                TreeView?.UpdateNodeStates();
                EditorUpdate.End();
            }
        }

        private void OnToolbarNewAsset()
        {
            var tree = EditorUtility.CreateNewTree();
            if (tree)
            {
                SelectNewTree(tree);
            }
        }

        public void PushSubTreeView(SubTree subtreeNode)
        {
            if (subtreeNode.TreeAsset != null)
            {
                if (Application.isPlaying)
                {
                    SelectTree(subtreeNode.TreeInstance);
                }
                else
                {
                    SelectTree(subtreeNode.TreeAsset);
                }
            }
            else
            {
                Debug.LogError("Invalid subtree assigned. Assign a a behaviour tree to the tree asset field");
            }
        }

        public void PopToSubtree(int depth, BehaviourTree tree)
        {
            while (Breadcrumbs != null && Breadcrumbs.childCount > depth)
            {
                Breadcrumbs.PopItem();
            }

            if (tree)
            {
                SelectTree(tree);
            }
        }

        public void ClearBreadcrumbs()
        {
            PopToSubtree(0, null);
        }
    }
}
