using Editor.EasyBT.Manipulators;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyBT
{
    public class FloatingPanel : VisualElement
    {
        internal VisualElement Content { get; }
        private DefaultPosition _defaultPosition;
        private bool _showCloseButton;
        private bool _isCollapsable;

        public string Title
        {
            get => _panelTitle.text;
            set => _panelTitle.text = value;
        }

        public bool IsCollapsable
        {
            get => _isCollapsable;
            set
            {
                _isCollapsable = value;
                SetCollapsable();
            }
        }

        private readonly VisualElement _panelAppBar;
        private readonly Label _panelTitle;

        private const float DefaultHeight = 300f;
        private float _expandedHeight;

        public FloatingPanel(VisualElement content, VisualElement view, string name, DefaultPosition defaultPosition,
            bool showCloseButton)
        {
            styleSheets.Add(AssetDatabase.LoadAssetAtPath<StyleSheet>(
                "Packages/com.tenarion.easyutils/Editor/EasyBT/UIBuilder/FloatingPanelStyle.uss"));
            AssetDatabase
                .LoadAssetAtPath<VisualTreeAsset>(
                    "Packages/com.tenarion.easyutils/Editor/EasyBT/UIBuilder/FloatingPanelView.uxml").CloneTree(this);

            Content = content;
            this.name = name;

            this.AddManipulator(new FloatingPanelManipulator(view));

            _defaultPosition = defaultPosition;
            _showCloseButton = showCloseButton;

            _panelAppBar = this.Q<VisualElement>("FloatingPanelAppBar");
            _panelAppBar.pickingMode = PickingMode.Position;

            _panelTitle = _panelAppBar.Q<Label>("FloatingPanelTitle");

            SetPositionFromDefaultPosition();
            style.width = style.minWidth;
            style.height = DefaultHeight;

            if (content.parent != null) content.RemoveFromHierarchy();

            this.Q<VisualElement>("PanelContent").Add(Content);
            RegisterCallback<GeometryChangedEvent>(OnGeometryChangedEvent);
        }

        public static FloatingPanel Create(VisualElement content, VisualElement view, string name,
            DefaultPosition defaultPosition = DefaultPosition.TopLeft, bool showCloseButton = false)
        {
            var panel = new FloatingPanel(content, view, name, defaultPosition, showCloseButton);
            return panel;
        }

        private void SetCollapsable()
        {
            if (_isCollapsable) _panelAppBar.RegisterCallback<PointerUpEvent>(OnTitleBarPointerDown);
            else
            {
                if (ClassListContains("CollapsedPanel")) ExpandPanel();
                _panelAppBar.UnregisterCallback<PointerUpEvent>(OnTitleBarPointerDown);
            }
        }

        private void OnTitleBarPointerDown(PointerUpEvent evt)
        {
            if (IsCollapsable && evt.button == 0)
            {
                if (evt.target is Button)
                {
                    return;
                }

                if (!ClassListContains("CollapsedPanel"))
                {
                    CollapsePanel();
                }
                else
                {
                    ExpandPanel();
                }
            }
        }

        internal void CollapsePanel()
        {
            RemoveFromClassList("ExpandedPanel");
            AddToClassList("CollapsedPanel");
            // this.Q<Icon>("CollapseIcon").iconName = "caret-up";
            _expandedHeight = resolvedStyle.height;
            style.height = _panelAppBar.resolvedStyle.height;

            // bool inEditorContext = panel.contextType == ContextType.Editor;
            // GraphPrefsUtility.SetBool(IsCollapsedPrefsKey, true, inEditorContext);
        }

        internal void ExpandPanel()
        {
            RemoveFromClassList("CollapsedPanel");
            AddToClassList("ExpandedPanel");
            // this.Q<Icon>("CollapseIcon").iconName = "caret-down";
            if (_expandedHeight != 0)
            {
                style.height = _expandedHeight;
            }

            // bool inEditorContext = panel.contextType == ContextType.Editor;
            // GraphPrefsUtility.SetBool(IsCollapsedPrefsKey, false, inEditorContext);
        }

        public enum DefaultPosition
        {
            TopLeft,
            TopRight,
            BottomLeft,
            BottomRight
        }

        internal void PreventCollapsingThisFrame()
        {
            _panelAppBar.UnregisterCallback<PointerUpEvent>(OnTitleBarPointerDown);
            schedule.Execute(() => _panelAppBar.RegisterCallback<PointerUpEvent>(OnTitleBarPointerDown));
        }

        private void OnGeometryChangedEvent(GeometryChangedEvent evt)
        {
            ClampPositionWithinParent();
            ClampSizeWithinParentSize();
        }

        private void SetPositionFromDefaultPosition()
        {
            transform.position = _defaultPosition switch
            {
                DefaultPosition.TopLeft => new Vector3(0, 0, 0),
                DefaultPosition.TopRight => new Vector3(float.PositiveInfinity, 0, 0),
                DefaultPosition.BottomLeft => new Vector3(0, float.PositiveInfinity, 0),
                DefaultPosition.BottomRight => new Vector3(float.PositiveInfinity, float.PositiveInfinity, 0),
                _ => transform.position
            };
        }

        internal void ClampSizeWithinParentSize()
        {
            if (parent == null)
            {
                return;
            }

            style.width = Mathf.Clamp(resolvedStyle.width, resolvedStyle.width, parent.worldBound.width);
            style.height = Mathf.Clamp(resolvedStyle.height, resolvedStyle.height, parent.worldBound.height);
        }

        internal void ClampPositionWithinParent()
        {
            if (parent == null)
            {
                return;
            }

            Vector3 position = transform.position;
            transform.position = new Vector2(
                Mathf.Clamp(position.x, 0, parent.worldBound.width - resolvedStyle.width),
                Mathf.Clamp(position.y, 0, parent.worldBound.height - resolvedStyle.height));
        }
    }
}
