using Core.Helpers;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyBT
{
    // Create a new type of Settings Asset.
    public class BehaviourTreeProjectSettings : ScriptableObject
    {
        public const string BehaviourTreeProjectSettingsPath =
            "Assets/Resources/EasyUtils/BehaviourTreeProjectSettings.asset";

        [Header("Asset Settings")] [Tooltip("Folder where new tree assets will be created. (Must begin with 'Assets')")]
        public string NewTreePath = "Assets/";

        [Tooltip("Folder where new node scripts will be created. (Must begin with 'Assets')")]
        public string NewNodePath = "Assets/";

        [Tooltip("Custom script template to use when creating action nodes")]
        public TextAsset ScriptTemplateActionNode;

        [Tooltip("Custom script template to use when creating condition nodes")]
        public TextAsset ScriptTemplateConditionNode;

        [Tooltip("Custom script template to use when creating composite nodes")]
        public TextAsset ScriptTemplateCompositeNode;

        [Tooltip("Custom script template to use when creating decorator nodes")]
        public TextAsset ScriptTemplateDecoratorNode;

        [Header("Node Canvas Settings")] [Tooltip("Horizontal grid size nodes will snap to")]
        public int GridSnapSizeX = 15;

        [Tooltip("Vertical grid size nodes will snap to")]
        public int GridSnapSizeY = 15;

        [Tooltip(
            "If enabled, selecting a node will automatically add all it's children to the selection. If disabled, hold control to select entire node hierarchy")]
        public bool AutoSelectNodeHierarchy;

        internal static BehaviourTreeProjectSettings GetOrCreateSettings()
        {
            var settings = AssetDatabase.LoadAssetAtPath<BehaviourTreeProjectSettings>(
                BehaviourTreeProjectSettingsPath);
            if (settings == null)
            {
                AssetDatabaseHelper.CreateFolderHierarchyIfNotExists("Resources/EasyUtils");

                settings = CreateInstance<BehaviourTreeProjectSettings>();
                AssetDatabase.CreateAsset(settings, "Assets/Resources/EasyUtils/EasyBehaviourTreeSettings.asset");
                AssetDatabase.SaveAssets();
            }

            return settings;
        }

        internal static SerializedObject GetSerializedSettings()
        {
            return new SerializedObject(GetOrCreateSettings());
        }
    }

    // Register a SettingsProvider using UIElements for the drawing framework:
    internal static class MyCustomSettingsUIElementsRegister
    {
        [SettingsProvider]
        public static SettingsProvider CreateMyCustomSettingsProvider()
        {
            // First parameter is the path in the Settings window.
            // Second parameter is the scope of this setting: it only appears in the Settings window for the Project scope.
            var provider =
                new SettingsProvider("Project/Easy Utils/BehaviourTreeProjectSettings", SettingsScope.Project)
                {
                    label = "Easy Behaviour Tree",
                    // activateHandler is called when the user clicks on the Settings item in the Settings window.
                    activateHandler = (searchContext, rootElement) =>
                    {
                        var settings = BehaviourTreeProjectSettings.GetSerializedSettings();

                        // rootElement is a VisualElement. If you add any children to it, the OnGUI function
                        // isn't called because the SettingsProvider uses the UIElements drawing framework.
                        var title = new Label
                        {
                            text = "Behaviour Tree Settings"
                        };
                        title.AddToClassList("title");
                        rootElement.Add(title);

                        var properties = new VisualElement()
                        {
                            style =
                            {
                                flexDirection = FlexDirection.Column
                            }
                        };
                        properties.AddToClassList("property-list");
                        rootElement.Add(properties);

                        properties.Add(new InspectorElement(settings));

                        rootElement.Bind(settings);
                    }
                };

            return provider;
        }
    }
}
