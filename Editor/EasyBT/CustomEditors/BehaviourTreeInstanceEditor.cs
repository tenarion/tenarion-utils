using System.Linq;
using DesignPatterns.EasyBT;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyBT.CustomEditors
{
    [CustomEditor(typeof(BehaviourTreeInstance))]
    public class BehaviourTreeInstanceEditor : UnityEditor.Editor
    {
        public override VisualElement CreateInspectorGUI()
        {
            var container = new VisualElement();

            var treeField = new PropertyField();
            treeField.bindingPath = nameof(BehaviourTreeInstance.BehaviourTree);

            var validateField = new PropertyField();
            validateField.bindingPath = nameof(BehaviourTreeInstance.Validate);

            var tickMode = new PropertyField();
            tickMode.bindingPath = nameof(BehaviourTreeInstance.TickMode);

            var startMode = new PropertyField();
            startMode.bindingPath = nameof(BehaviourTreeInstance.StartMode);

            var publicKeys = new ListView
            {
                bindingPath = nameof(BehaviourTreeInstance.BlackboardOverrides),
                makeItem = () => new PropertyField(),
                showAddRemoveFooter = true,
                showAlternatingRowBackgrounds = AlternatingRowBackground.All,
                showBoundCollectionSize = true,
                fixedItemHeight = 50,
                reorderable = true,
                reorderMode = ListViewReorderMode.Animated
            };

            publicKeys.itemsAdded += items =>
            {
                foreach (var index in items)
                {
                    var source = (target as BehaviourTreeInstance)?.BlackboardOverrides;
                    source?.RemoveAt(index);
                    BlackboardKeyValuePair item;
                    if (source is { Count: > 0 })
                    {
                        var last = source?.Last();
                        item = new BlackboardKeyValuePair
                        {
                            Key = last.Key,
                            Value = last.Value.Clone() as BlackboardKey
                        };
                    }
                    else
                        item = new BlackboardKeyValuePair();

                    source?.Insert(index, item);
                }
            };

            var startButton = new Button(() =>
            {
                if (!Application.isPlaying) return;
                var instance = target as BehaviourTreeInstance;
                instance.StartBehaviour(instance.BehaviourTree);
            })
            {
                text = "Start Behaviour"
            };

            container.Add(treeField);
            container.Add(tickMode);
            container.Add(startMode);
            container.Add(validateField);
            container.Add(publicKeys);
            container.Add(startButton);

            return container;
        }
    }
}
