using System;
using Core.Extensions;
using DesignPatterns.EasyBT;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Editor.EasyBT.PropertyDrawers
{
    [CustomPropertyDrawer(typeof(NodeProperty<>))]
    public class GenericNodePropertyPropertyDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var tree = property.serializedObject.targetObject as BehaviourTree;

            var genericTypes = fieldInfo.FieldType.GenericTypeArguments;
            var propertyType = genericTypes[0];

            var reference = property.FindPropertyRelative("Reference");

            var label = new Label();
            label.AddToClassList("unity-base-field__label");
            label.AddToClassList("unity-property-field__label");
            label.AddToClassList("unity-property-field");
            label.text = property.displayName;

            var defaultValueField = new PropertyField
            {
                label = "",
                style =
                {
                    flexGrow = 1.0f
                },
                bindingPath = nameof(NodeProperty<int>.DefaultValue)
            };

            var dropdown = new PopupField<BlackboardKey>
            {
                label = "",
                formatListItemCallback = FormatItem,
                formatSelectedValueCallback = FormatSelectedItem,
                value = reference.managedReferenceValue as BlackboardKey,
                tooltip = "Bind value to a BlackboardKey",
                style =
                {
                    flexGrow = 1.0f
                }
            };
            dropdown.RegisterCallback<MouseEnterEvent>(evt =>
            {
                dropdown.choices.Clear();
                foreach (var key in tree.Blackboard.Keys)
                {
                    if (propertyType.IsAssignableFrom(key.UnderlyingType))
                    {
                        dropdown.choices.Add(key);
                    }
                }

                dropdown.choices.Add(null);

                dropdown.choices.Sort((left, right) =>
                {
                    if (left == null)
                    {
                        return -1;
                    }

                    if (right == null)
                    {
                        return 1;
                    }

                    return string.Compare(left.Name, right.Name, StringComparison.Ordinal);
                });
            });

            dropdown.RegisterCallback<ChangeEvent<BlackboardKey>>(evt =>
            {
                var newKey = evt.newValue;
                reference.managedReferenceValue = newKey;
                BehaviourTreeEditorWindow.Instance.Serializer.ApplyChanges();

                if (evt.newValue == null)
                {
                    defaultValueField.Show();
                    dropdown.style.flexGrow = 0.0f;
                }
                else
                {
                    defaultValueField.Hide();
                    dropdown.style.flexGrow = 1.0f;
                }
            });

            defaultValueField.style.display = dropdown.value == null ? DisplayStyle.Flex : DisplayStyle.None;
            dropdown.style.flexGrow = dropdown.value == null ? 0.0f : 1.0f;

            var container = new VisualElement();
            container.AddToClassList("unity-base-field");
            container.AddToClassList("node-property-field");
            container.style.flexDirection = FlexDirection.Row;
            container.Add(label);
            container.Add(defaultValueField);
            container.Add(dropdown);

            return container;
        }

        private string FormatItem(BlackboardKey item)
        {
            if (item == null)
            {
                return "[Inline]";
            }

            return item.Name;
        }

        private string FormatSelectedItem(BlackboardKey item)
        {
            if (item == null)
            {
                return "";
            }

            return item.Name;
        }
    }

    [CustomPropertyDrawer(typeof(NodeProperty))]
    public class NodePropertyPropertyDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var tree = property.serializedObject.targetObject as BehaviourTree;

            var reference = property.FindPropertyRelative("Reference");

            var dropdown = new PopupField<BlackboardKey>
            {
                label = property.displayName,
                formatListItemCallback = FormatItem,
                formatSelectedValueCallback = FormatItem,
                value = reference.managedReferenceValue as BlackboardKey
            };

            dropdown.RegisterCallback<MouseEnterEvent>(evt =>
            {
                dropdown.choices.Clear();
                foreach (var key in tree.Blackboard.Keys)
                {
                    dropdown.choices.Add(key);
                }

                dropdown.choices.Sort((left, right) => string.Compare(left.Name, right.Name, StringComparison.Ordinal));
            });

            dropdown.RegisterCallback<ChangeEvent<BlackboardKey>>(evt =>
            {
                var newKey = evt.newValue;
                reference.managedReferenceValue = newKey;
                BehaviourTreeEditorWindow.Instance.Serializer.ApplyChanges();
            });
            return dropdown;
        }

        private string FormatItem(BlackboardKey item)
        {
            if (item == null)
            {
                return "(null)";
            }

            return item.Name;
        }
    }
}
