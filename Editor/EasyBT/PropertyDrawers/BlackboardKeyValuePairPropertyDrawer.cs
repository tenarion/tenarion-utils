using DesignPatterns.EasyBT;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyBT.PropertyDrawers
{
    [CustomPropertyDrawer(typeof(BlackboardKeyValuePair))]
    public class BlackboardKeyValuePairPropertyDrawer : PropertyDrawer
    {
        private VisualElement _pairContainer;

        private BehaviourTree GetBehaviourTree(SerializedProperty property)
        {
            switch (property.serializedObject.targetObject)
            {
                case BehaviourTree tree:
                    return tree;
                case BehaviourTreeInstance instance:
                    return instance.RuntimeTree;
                default:
                    Debug.LogError("Could not find behaviour tree this is referencing");
                    return null;
            }
        }

        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var first = property.FindPropertyRelative(nameof(BlackboardKeyValuePair.Key));
            var second = property.FindPropertyRelative(nameof(BlackboardKeyValuePair.Value));

            var dropdown = new PopupField<BlackboardKey>
            {
                label = first.displayName,
                formatListItemCallback = FormatItem,
                formatSelectedValueCallback = FormatItem,
                value = first.managedReferenceValue as BlackboardKey
            };

            var tree = GetBehaviourTree(property);
            dropdown.RegisterCallback<MouseEnterEvent>(evt =>
            {
                dropdown.choices.Clear();
                foreach (var key in tree.Blackboard.Keys)
                {
                    dropdown.choices.Add(key);
                }
            });

            dropdown.RegisterCallback<ChangeEvent<BlackboardKey>>(evt =>
            {
                var newKey = evt.newValue;
                first.managedReferenceValue = newKey;
                property.serializedObject.ApplyModifiedProperties();

                if (_pairContainer.childCount > 1)
                {
                    _pairContainer.RemoveAt(1);
                }

                if (second.managedReferenceValue == null ||
                    second.managedReferenceValue.GetType() != dropdown.value.GetType())
                {
                    second.managedReferenceValue = BlackboardKey.CreateKey(dropdown.value.GetType());
                    second.serializedObject.ApplyModifiedProperties();
                }

                var field = new PropertyField
                {
                    label = second.displayName
                };
                field.BindProperty(second.FindPropertyRelative(nameof(BlackboardKey<object>.Value)));
                _pairContainer.Add(field);
            });

            _pairContainer = new VisualElement();
            _pairContainer.Add(dropdown);

            if (dropdown.value != null)
            {
                if (second.managedReferenceValue == null ||
                    first.managedReferenceValue.GetType() != second.managedReferenceValue.GetType())
                {
                    second.managedReferenceValue = BlackboardKey.CreateKey(dropdown.value.GetType());
                    second.serializedObject.ApplyModifiedProperties();
                }

                var field = new PropertyField
                {
                    label = second.displayName,
                    bindingPath = nameof(BlackboardKey<object>.Value)
                };
                _pairContainer.Add(field);
            }

            return _pairContainer;
        }

        private string FormatItem(BlackboardKey item)
        {
            return item == null ? "(null)" : item.Name;
        }
    }
}
