using Core.Extensions;
using DesignPatterns.EasyBT;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Editor.EasyBT.PropertyDrawers {

    [CustomPropertyDrawer(typeof(BlackboardKey))]
    public class BlackboardKeyPropertyDrawer : PropertyDrawer {
    
        public override VisualElement CreatePropertyGUI(SerializedProperty property) {
            if (property.propertyType == SerializedPropertyType.ArraySize) return null;

            var keyName = new Label();
            var renameField = new TextField();
            var keyValue = new PropertyField();
            var container = new VisualElement();

            keyName.bindingPath = nameof(BlackboardKey.Name);
            keyName.AddToClassList("unity-base-field__label");

            renameField.Hide();
            renameField.bindingPath = nameof(BlackboardKey.Name);
            renameField.RegisterCallback<BlurEvent>(evt => {
                keyValue.Show();
                keyName.Show();
                renameField.Hide();
            });

            keyValue.label = "";
            keyValue.style.flexGrow = 1.0f;
            keyValue.bindingPath = nameof(BlackboardKey<object>.Value);

            container.AddManipulator(new ContextualMenuManipulator(evt => {
                evt.menu.AppendAction("Delete", x => BehaviourTreeEditorWindow.Instance.Serializer.DeleteBlackboardKey(property.displayName), DropdownMenuAction.AlwaysEnabled);
            }));
            container.style.flexDirection = FlexDirection.Row;
            container.Add(keyName);
            container.Add(renameField);
            container.Add(keyValue);

            keyName.RegisterCallback<MouseDownEvent>(evt =>
            {
                if (evt.clickCount != 2) return;

                renameField.value = keyName.text;
                renameField.Show();
                renameField.Focus();

                keyValue.Hide();
                keyName.Hide();
            });


            return container;
        }
    }
}
