using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyBT.Manipulators
{
    public class AddNodeManipulator : Manipulator
    {
        BehaviourTreeView Target => target as BehaviourTreeView;
        protected override void RegisterCallbacksOnTarget()
        {
            Target.RegisterCallback<KeyDownEvent>(OnKeyDown);
        }

        protected override void UnregisterCallbacksFromTarget()
        {
            Target.UnregisterCallback<KeyDownEvent>(OnKeyDown);
        }

        private void OnKeyDown(KeyDownEvent evt)
        {
            if (evt.keyCode == KeyCode.Space)
            {
                OnAddNode(evt);
                // evt.StopImmediatePropagation();
            }
        }

        private void OnAddNode(KeyDownEvent evt)
        {
            CreateNodeWindow.Show(evt.originalMousePosition, null);
        }
    }
}
