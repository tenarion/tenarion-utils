using System;
using System.Collections.Generic;
using DesignPatterns.EasyBT;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyBT
{
    [UxmlElement]
    public partial class OverlayView : VisualElement
    {
        public Action<BehaviourTree> OnTreeSelected;

        private Button _createButton;
        private VisualElement _listViewContainer;
        private MultiColumnListView _projectListView;

        private string _nameColumn = "Name";
        private string _pathColumn = "Path";
        private List<string> _assetPaths;

        private Column CreateColumn(string name)
        {
            var column = new Column
            {
                name = name,
                title = name,
                width = 100.0f,
                stretchable = true
            };
            return column;
        }

        private MultiColumnListView CreateListView()
        {
            var listView = new MultiColumnListView
            {
                showBorder = true,
                showAlternatingRowBackgrounds = AlternatingRowBackground.ContentOnly,
                fixedItemHeight = 30.0f,
                showBoundCollectionSize = false,
                showAddRemoveFooter = false,
                reorderable = false,
                itemsSource = _assetPaths
            };

            listView.columns.Add(CreateColumn(_nameColumn));
            listView.columns.Add(CreateColumn(_pathColumn));

            listView.columns[_nameColumn].makeCell = () => new Label();
            listView.columns[_pathColumn].makeCell = () => new Label();

            listView.columns[_nameColumn].bindCell = BindName;
            listView.columns[_pathColumn].bindCell = BindPath;

            return listView;
        }

        private void BindName(VisualElement element, int index)
        {
            var label = element as Label;
            label.style.unityTextAlign = TextAnchor.MiddleLeft;
            var fileName = System.IO.Path.GetFileNameWithoutExtension(_assetPaths[index]);
            label.text = fileName;
        }

        private void BindPath(VisualElement element, int index)
        {
            var label = element as Label;
            label.style.unityTextAlign = TextAnchor.MiddleLeft;
            label.text = _assetPaths[index];
        }

        public void Show()
        {
            // Hidden in UIBuilder while editing..
            style.visibility = Visibility.Visible;

            // Configure fields
            _createButton = this.Q<Button>("CreateButton");
            _listViewContainer = this.Q<VisualElement>("ListViewContainer");

            // Find all behaviour tree assets
            _assetPaths = EditorUtility.GetAssetPaths<BehaviourTree>();
            _assetPaths.Sort();

            // Configure create asset button
            _createButton.clicked -= OnCreateAsset;
            _createButton.clicked += OnCreateAsset;

            _projectListView = CreateListView();
            _listViewContainer.Clear();
            _listViewContainer.Add(_projectListView);
            _projectListView.selectionChanged += OnSelectionChanged;
        }

        private void OnSelectionChanged(IEnumerable<object> obj)
        {
            OnOpenAsset();
        }

        public void Hide()
        {
            style.visibility = Visibility.Hidden;
        }

        public string ToMenuFormat(string one)
        {
            // Using the slash creates submenus...
            return one.Replace("/", "|");
        }

        public string ToAssetFormat(string one)
        {
            // Using the slash creates submenus...
            return one.Replace("|", "/");
        }

        private void OnOpenAsset()
        {
            var path = _assetPaths[_projectListView.selectedIndex];

            var tree = AssetDatabase.LoadAssetAtPath<BehaviourTree>(path);
            if (tree)
            {
                TreeSelected(tree);
                style.visibility = Visibility.Hidden;
            }
        }

        private void OnCreateAsset()
        {
            var tree = EditorUtility.CreateNewTree();
            if (tree)
            {
                TreeSelected(tree);
                style.visibility = Visibility.Hidden;
            }
        }

        private void TreeSelected(BehaviourTree tree)
        {
            OnTreeSelected.Invoke(tree);
        }
    }
}
