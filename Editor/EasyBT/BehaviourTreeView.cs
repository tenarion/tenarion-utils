using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DesignPatterns.EasyBT;
using Editor.EasyBT.Manipulators;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;
using Debug = UnityEngine.Debug;
using Node = DesignPatterns.EasyBT.Node;

namespace Editor.EasyBT
{
    [UxmlElement]
    public partial class BehaviourTreeView : GraphView
    {
        public Action<NodeView> OnNodeSelected;

        protected override bool canCopySelection => true;

        protected override bool canCutSelection => false; // Cut not supported right now

        protected override bool canPaste => true;

        protected override bool canDuplicateSelection => true;

        protected override bool canDeleteSelection => true;

        private SerializedBehaviourTree _serializer;

        private bool _dontUpdateModel;

        [Serializable]
        private class CopyPasteData
        {
            public List<string> NodeGuids = new();

            public void AddGraphElements(IEnumerable<GraphElement> elementsToCopy)
            {
                foreach (var element in elementsToCopy)
                {
                    var nodeView = element as NodeView;
                    if (nodeView != null && nodeView.Node is not RootNode)
                    {
                        NodeGuids.Add(nodeView.Node.Guid);
                    }
                }
            }
        }

        private class EdgeToCreate
        {
            public NodeView Parent;
            public NodeView Child;
        };

        public BehaviourTreeView()
        {
            Insert(0, new GridBackground());

            this.AddManipulator(new ContentZoomer());
            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new HierarchySelector());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());
            this.AddManipulator(new AddNodeManipulator());

            // Perform Copy
            serializeGraphElements = items =>
            {
                var copyPasteData = new CopyPasteData();
                copyPasteData.AddGraphElements(items);
                var data = JsonUtility.ToJson(copyPasteData);
                return data;
            };

            // Perform Paste
            unserializeAndPaste = (operationName, data) =>
            {
                _serializer.BeginBatch();

                ClearSelection();

                var copyPasteData = JsonUtility.FromJson<CopyPasteData>(data);
                var oldToNewMapping = new Dictionary<string, string>();

                // Gather all nodes to copy
                var nodesToCopy = new List<NodeView>();
                foreach (var nodeGuid in copyPasteData.NodeGuids)
                {
                    var nodeView = FindNodeView(nodeGuid);
                    nodesToCopy.Add(nodeView);
                }

                // Gather all edges to create
                var edgesToCreate = new List<EdgeToCreate>();
                foreach (var nodeGuid in copyPasteData.NodeGuids)
                {
                    var nodeView = FindNodeView(nodeGuid);
                    var nodesParent = nodeView.NodeParent;
                    if (nodesToCopy.Contains(nodesParent))
                    {
                        var newEdge = new EdgeToCreate
                        {
                            Parent = nodesParent,
                            Child = nodeView
                        };
                        edgesToCreate.Add(newEdge);
                    }
                }

                // Copy all nodes
                foreach (var nodeView in nodesToCopy)
                {
                    var newNode = _serializer.CreateNode(nodeView.Node.GetType(),
                        nodeView.Node.Position + Vector2.one * 50);
                    var newNodeView = CreateNodeView(newNode);
                    AddToSelection(newNodeView);

                    // Map old to new guids so edges can be cloned.
                    oldToNewMapping[nodeView.Node.Guid] = newNode.Guid;
                }

                // Copy all edges
                foreach (var edge in edgesToCreate)
                {
                    var oldParent = edge.Parent;
                    var oldChild = edge.Child;

                    // These should already have been created.
                    var newParent = FindNodeView(oldToNewMapping[oldParent.Node.Guid]);
                    var newChild = FindNodeView(oldToNewMapping[oldChild.Node.Guid]);

                    _serializer.AddChild(newParent.Node, newChild.Node);
                    AddChild(newParent, newChild);
                }

                // Save changes
                _serializer.EndBatch();
            };

            // Enable copy paste always?
            canPasteSerializedData = data => { return true; };

            viewTransformChanged += OnViewTransformChanged;
        }

        private void OnViewTransformChanged(GraphView graphView)
        {
            var position = contentViewContainer.transform.position;
            var scale = contentViewContainer.transform.scale;
            _serializer.SetViewTransform(position, scale);
        }

        public NodeView FindNodeView(Node node)
        {
            return GetNodeByGuid(node.Guid) as NodeView;
        }

        public NodeView FindNodeView(string guid)
        {
            return GetNodeByGuid(guid) as NodeView;
        }

        public void ClearView()
        {
            graphViewChanged -= OnGraphViewChanged;
            DeleteElements(graphElements.ToList());
            graphViewChanged += OnGraphViewChanged;
        }

        public void PopulateView(SerializedBehaviourTree tree)
        {
            _serializer = tree;

            ClearView();

            Debug.Assert(_serializer.Tree.RootNode != null);

            // Creates node view
            _serializer.Tree.Nodes.ForEach(n => CreateNodeView(n));

            // Create edges
            _serializer.Tree.Nodes.ForEach(n =>
            {
                var children = BehaviourTree.GetChildren(n);
                children.ForEach(c =>
                {
                    var parentView = FindNodeView(n);
                    var childView = FindNodeView(c);
                    Debug.Assert(parentView != null, "Invalid parent after deserialising");
                    Debug.Assert(childView != null,
                        $"Null child view after deserialising parent{parentView.Node.GetType().Name}");
                    CreateEdgeView(parentView, childView);
                });
            });

            // Set view
            contentViewContainer.transform.position = _serializer.Tree.ViewPosition;
            contentViewContainer.transform.scale = _serializer.Tree.ViewScale;
        }

        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            return ports.ToList().Where(endPort =>
                endPort.direction != startPort.direction &&
                endPort.node != startPort.node).ToList();
        }

        private GraphViewChange OnGraphViewChanged(GraphViewChange graphViewChange)
        {
            if (_dontUpdateModel)
            {
                return graphViewChange;
            }

            var blockedDeletes = new List<GraphElement>();

            if (graphViewChange.elementsToRemove != null)
            {
                graphViewChange.elementsToRemove.ForEach(elem =>
                {
                    var nodeView = elem as NodeView;
                    if (nodeView != null)
                    {
                        // The root node is not deletable
                        if (nodeView.Node is not RootNode)
                        {
                            OnNodeSelected(null);
                            _serializer.DeleteNode(nodeView.Node);
                        }
                        else
                        {
                            blockedDeletes.Add(elem);
                        }
                    }

                    var edge = elem as Edge;
                    if (edge != null)
                    {
                        var parentView = edge.output.node as NodeView;
                        var childView = edge.input.node as NodeView;
                        _serializer.RemoveChild(parentView.Node, childView.Node);
                    }
                });
            }

            if (graphViewChange.edgesToCreate != null)
            {
                graphViewChange.edgesToCreate.ForEach(edge =>
                {
                    var parentView = edge.output.node as NodeView;
                    var childView = edge.input.node as NodeView;
                    _serializer.AddChild(parentView.Node, childView.Node);
                });
            }

            nodes.ForEach(n =>
            {
                var view = n as NodeView;
                // Need to rebind description labels as the serialized properties will be invalidated after removing from array
                view.SetupDataBinding();
                view.SortChildren();
            });

            foreach (var elem in blockedDeletes)
            {
                graphViewChange.elementsToRemove.Remove(elem);
            }

            return graphViewChange;
        }

        public override void BuildContextualMenu(ContextualMenuPopulateEvent evt)
        {
            base.BuildContextualMenu(evt);
            // Add custom menu options

            if (selection.FirstOrDefault() is NodeView nodeView)
            {
                evt.menu.InsertAction(1, "Toggle Breakpoint", action =>
                {
                    if (!Debugger.IsAttached)
                    {
                        UnityEditor.EditorUtility.DisplayDialog("Code Debugger Not Attached",
                            "You currently don't have a debugger attached. For the breakpoints to work, please attach a debugger using your code IDE (Visual Studio, Rider, etc).",
                            "OK", DialogOptOutDecisionType.ForThisSession, "EasyBT.Breakpoint.DebuggerNotAttachedWarning");
                    }
                    _serializer.ToggleBreakpoint(nodeView.Node);
                }, nodeView.Node.HasBreakpoint ? DropdownMenuAction.Status.Checked : DropdownMenuAction.Status.Normal);
            }
        }

        public NodeView CreateNode(Type type, Vector2 position, NodeView parentView)
        {
            _serializer.BeginBatch();

            // Update model
            var node = _serializer.CreateNode(type, position);
            if (parentView != null)
            {
                _serializer.AddChild(parentView.Node, node);
            }

            // Update View
            var nodeView = CreateNodeView(node);
            if (parentView != null)
            {
                AddChild(parentView, nodeView);
            }

            _serializer.EndBatch();

            return nodeView;
        }

        public NodeView CreateNodeWithChild(Type type, Vector2 position, NodeView childView)
        {
            _serializer.BeginBatch();

            // Update Model
            var node = _serializer.CreateNode(type, position);

            // Delete the childs previous parent
            foreach (var connection in childView.Input.connections)
            {
                var childParent = connection.output.node as NodeView;
                _serializer.RemoveChild(childParent.Node, childView.Node);
            }

            // Add as child of new node.
            _serializer.AddChild(node, childView.Node);

            // Update View
            var nodeView = CreateNodeView(node);
            if (nodeView != null)
            {
                AddChild(nodeView, childView);
            }

            _serializer.EndBatch();
            return nodeView;
        }

        private NodeView CreateNodeView(Node node)
        {
            var nodeView = new NodeView(node, BehaviourTreeEditorWindow.Instance.NodeXml);
            AddElement(nodeView);
            nodeView.OnNodeSelected = OnNodeSelected;
            return nodeView;
        }

        public void AddChild(NodeView parentView, NodeView childView)
        {
            // Delete Previous output connections
            if (parentView.Output.capacity == Port.Capacity.Single)
            {
                RemoveElements(parentView.Output.connections);
            }

            // Delete previous child's parent
            RemoveElements(childView.Input.connections);

            CreateEdgeView(parentView, childView);
        }

        private void CreateEdgeView(NodeView parentView, NodeView childView)
        {
            var edge = parentView.Output.ConnectTo(childView.Input);
            AddElement(edge);
        }

        public void RemoveElements(IEnumerable<GraphElement> elementsToRemove)
        {
            _dontUpdateModel = true;
            DeleteElements(
                elementsToRemove); // Just need to delete the ui elements without causing a graphChangedEvent here.
            _dontUpdateModel = false;
        }

        public void UpdateNodeStates()
        {
            if (_serializer == null)
                return;

            if (_serializer.Tree == null)
                return;

            var tickResults = _serializer.Tree.TreeContext?.TickResults;
            if (tickResults != null)
            {
                nodes.ForEach(n =>
                {
                    var view = n as NodeView;
                    view.UpdateState(tickResults);
                });
            }
        }

        public void SelectNode(NodeView nodeView)
        {
            ClearSelection();
            if (nodeView != null)
            {
                AddToSelection(nodeView);
            }
        }
    }
}
