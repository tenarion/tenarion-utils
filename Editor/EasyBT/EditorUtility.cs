using System.Collections.Generic;
using System.Linq;
using DesignPatterns.EasyBT;
using UnityEditor;
using UnityEngine;

namespace Editor.EasyBT
{
    public static class EditorUtility
    {
        public struct ScriptTemplate
        {
            public TextAsset TemplateFile;
            public string DefaultFileName;
            public string SubFolder;
        }

        [System.Serializable]
        public class PackageManifest
        {
            public string Name;
            public string Version;
        }


        public static BehaviourTree CreateNewTree()
        {
            var settings = BehaviourTreeEditorWindow.Instance.Settings;

            var savePath =
                UnityEditor.EditorUtility.SaveFilePanel("Create New", settings.NewTreePath, "New Behavior Tree",
                    "asset");
            if (string.IsNullOrEmpty(savePath))
            {
                return null;
            }

            var assetName = System.IO.Path.GetFileNameWithoutExtension(savePath);
            var folder = System.IO.Path.GetDirectoryName(savePath);
            folder = folder.Substring(folder.IndexOf("Assets"));


            var path = System.IO.Path.Join(folder, $"{assetName}.asset");
            if (System.IO.File.Exists(path))
            {
                Debug.LogError($"Failed to create behaviour tree asset: Path already exists:{assetName}");
                return null;
            }

            var tree = ScriptableObject.CreateInstance<BehaviourTree>();
            tree.name = assetName;
            AssetDatabase.CreateAsset(tree, path);
            AssetDatabase.SaveAssets();
            EditorGUIUtility.PingObject(tree);
            return tree;
        }

        public static void CreateNewScript(ScriptTemplate scriptTemplate, NodeView source, bool isSourceParent,
            Vector2 position)
        {
            BehaviourTreeEditorWindow.Instance.NewScriptDialog.CreateScript(scriptTemplate, source, isSourceParent,
                position);
        }


        public static List<T> LoadAssets<T>() where T : Object
        {
            var assetIds = AssetDatabase.FindAssets($"t:{typeof(T).Name}");

            return assetIds.Select(AssetDatabase.GUIDToAssetPath).Select(AssetDatabase.LoadAssetAtPath<T>).ToList();
        }

        public static List<string> GetAssetPaths<T>() where T : Object
        {
            var assetIds = AssetDatabase.FindAssets($"t:{typeof(T).Name}");
            return assetIds.Select(AssetDatabase.GUIDToAssetPath).ToList();
        }

        public static PackageManifest GetPackageManifest()
        {
            // Loop through all package.json files in the project and find this one..
            var packageJsons = AssetDatabase.FindAssets("package");
            var packagePaths = packageJsons.Select(AssetDatabase.GUIDToAssetPath).ToArray();
            foreach (var path in packagePaths)
            {
                var asset = AssetDatabase.LoadAssetAtPath<TextAsset>(path);
                if (asset)
                {
                    try
                    {
                        var manifest = JsonUtility.FromJson<PackageManifest>(asset.text);
                        if (manifest.Name == "com.tenarion.easyutils")
                        {
                            return manifest;
                        }
                    }
                    catch
                    {
                        // Ignore if the manifest file failed to parse
                    }
                }
            }

            return null;
        }

        public static float SnapTo(float value, int nearestInteger)
        {
            return Mathf.RoundToInt(value / nearestInteger) * nearestInteger;
        }
    }
}
