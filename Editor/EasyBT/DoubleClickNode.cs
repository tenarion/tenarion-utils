using DesignPatterns.EasyBT.Actions;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyBT
{
    public class DoubleClickNode : MouseManipulator
    {
        private double _time;
        private double _doubleClickDuration = 0.3;

        public DoubleClickNode()
        {
            _time = EditorApplication.timeSinceStartup;
        }

        protected override void RegisterCallbacksOnTarget()
        {
            target.RegisterCallback<MouseDownEvent>(OnMouseDown);
        }

        protected override void UnregisterCallbacksFromTarget()
        {
            target.UnregisterCallback<MouseDownEvent>(OnMouseDown);
        }

        private void OnMouseDown(MouseDownEvent evt)
        {
            if (!CanStopManipulation(evt))
                return;

            var clickedElement = evt.target as NodeView;
            if (clickedElement == null)
            {
                var ve = evt.target as VisualElement;
                clickedElement = ve.GetFirstAncestorOfType<NodeView>();
                if (clickedElement == null)
                    return;
            }

            var duration = EditorApplication.timeSinceStartup - _time;
            if (duration < _doubleClickDuration)
            {
                OnDoubleClick(evt, clickedElement);
            }

            _time = EditorApplication.timeSinceStartup;
        }

        private void OpenScriptForNode(MouseDownEvent evt, NodeView clickedElement)
        {
            // Open script in the editor:
            var nodeName = clickedElement.Node.GetType().Name;
            var assetGuids = AssetDatabase.FindAssets($"t:TextAsset {nodeName}");
            foreach (var guid in assetGuids)
            {
                var path = AssetDatabase.GUIDToAssetPath(guid);
                var filename = System.IO.Path.GetFileName(path);
                if (filename == $"{nodeName}.cs")
                {
                    var script = AssetDatabase.LoadAssetAtPath<TextAsset>(path);
                    AssetDatabase.OpenAsset(script);
                    break;
                }
            }

            // Remove the node from selection to prevent dragging it around when returning to the editor.
            BehaviourTreeEditorWindow.Instance.TreeView.RemoveFromSelection(clickedElement);
        }

        private void OpenSubtree(NodeView clickedElement)
        {
            BehaviourTreeEditorWindow.Instance.PushSubTreeView(clickedElement.Node as SubTree);
        }

        private void OnDoubleClick(MouseDownEvent evt, NodeView clickedElement)
        {
            if (clickedElement.Node is SubTree)
            {
                OpenSubtree(clickedElement);
            }
            else
            {
                OpenScriptForNode(evt, clickedElement);
            }
        }
    }
}
