using System;
using System.Linq;
using DesignPatterns.EasyBT;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Editor.EasyBT
{
    public class BlackboardView : VisualElement
    {
        private SerializedBehaviourTree _behaviourTree;

        private ListView _listView;
        private TextField _newKeyTextField;
        private PopupField<Type> _newKeyTypeField;

        private Button _createButton;

        public BlackboardView()
        {
            styleSheets.Add(AssetDatabase.LoadAssetAtPath<StyleSheet>(
                "Packages/com.tenarion.easyutils/Editor/EasyBT/UIBuilder/BlackboardStyle.uss"));
            var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(
                "Packages/com.tenarion.easyutils/Editor/EasyBT/UIBuilder/BlackboardView.uxml");
            visualTree.CloneTree(this);

            RegisterCallback<AttachToPanelEvent>(OnAttachToPanel);
        }

        private void OnAttachToPanel(AttachToPanelEvent evt)
        {
            if (GetFirstAncestorOfType<FloatingPanel>() == null) return;

            var floatingPanel = GetFirstAncestorOfType<FloatingPanel>();
            floatingPanel.Title = "Blackboard";
        }

        internal void Bind(SerializedBehaviourTree behaviourTree)
        {
            _behaviourTree = behaviourTree;

            _listView = this.Q<ListView>("ListView_Keys");
            _newKeyTextField = this.Q<TextField>("TextField_KeyName");
            var popupContainer = this.Q<VisualElement>("PopupField_Placeholder");

            _createButton = this.Q<Button>("Button_KeyCreate");

            // ListView
            _listView.Bind(behaviourTree.SerializedObject);

            _newKeyTypeField = new PopupField<Type>
            {
                label = "Type",
                formatListItemCallback = FormatItem,
                formatSelectedValueCallback = FormatItem
            };

            var types = TypeCache.GetTypesDerivedFrom<BlackboardKey>();
            foreach (var type in types.Where(type => !type.IsGenericType))
            {
                _newKeyTypeField.choices.Add(type);
                _newKeyTypeField.value ??= type;
            }

            popupContainer.Clear();
            popupContainer.Add(_newKeyTypeField);

            // TextField
            _newKeyTextField.RegisterCallback<ChangeEvent<string>>(evt => { ValidateButton(); });

            // Button
            _createButton.clicked -= CreateNewKey;
            _createButton.clicked += CreateNewKey;

            ValidateButton();
        }

        private string FormatItem(Type arg)
        {
            if (arg == null)
            {
                return "(null)";
            }

            return arg.Name.Replace("Key", "");
        }

        private void ValidateButton()
        {
            // Disable the create button if trying to create a non-unique key
            var isValidKeyText = ValidateKeyText(_newKeyTextField.text);
            _createButton.SetEnabled(isValidKeyText);
        }

        private bool ValidateKeyText(string text)
        {
            if (text == "")
            {
                return false;
            }

            var tree = _behaviourTree.Blackboard.serializedObject.targetObject as BehaviourTree;
            bool keyExists = tree.Blackboard.Find(_newKeyTextField.text) != null;
            return !keyExists;
        }

        private void CreateNewKey()
        {
            var newKeyType = _newKeyTypeField.value;
            if (newKeyType != null)
            {
                _behaviourTree.CreateBlackboardKey(_newKeyTextField.text, newKeyType);
            }

            ValidateButton();
        }

        public void ClearView()
        {
            _behaviourTree = null;
            _listView?.Unbind();
        }
    }
}
