using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Core.Extensions;
using DesignPatterns.EasyBT;
using DesignPatterns.EasyBT.Attributes;
using DesignPatterns.EasyBT.Composites;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Node = DesignPatterns.EasyBT.Node;

namespace Editor.EasyBT
{
    public class NodeView : UnityEditor.Experimental.GraphView.Node
    {
        private readonly Dictionary<Type, Color> _defaultNodeColors = new()
        {
            {typeof(Node), new Color(0.8f, 0.8f, 0.8f)},
            { typeof(ActionNode), new Color(0f, 0.4705882f, 0.8078431f) },
            { typeof(CompositeNode), new Color(0.9647059f, 0.682353f, 0.1764706f) },
            { typeof(DecoratorNode), new Color(0.5372549f, 0.6509804f, 0.9843137f) },
            { typeof(RootNode), new Color(1f, 0.3176471f, 0.3294118f) },
            { typeof(ConditionNode), new Color(0.7098039f, 0f, 0.8078431f) }
        };

        public Action<NodeView> OnNodeSelected;
        public Node Node;
        public Port Input;
        public Port Output;

        private NodeDetailsAttribute _details;

        public NodeView NodeParent
        {
            get
            {
                using var iter = Input.connections.GetEnumerator();
                iter.MoveNext();
                return iter.Current?.output.node as NodeView;
            }
        }

        public List<NodeView> NodeChildren
        {
            get
            {
                // This is untested and may not work. Possibly output should be input.
                var children = new List<NodeView>();
                foreach (var edge in Output.connections)
                {
                    if (edge.output.node is NodeView child)
                    {
                        children.Add(child);
                    }
                }

                return children;
            }
        }

        public NodeView(Node node, VisualTreeAsset nodeXml) : base(
            AssetDatabase.GetAssetPath(nodeXml))
        {
            _details = node.GetType().GetCustomAttribute<NodeDetailsAttribute>();
            var name = _details?.Name ?? node.GetType().Name.ToTitleCase();

            capabilities &= ~Capabilities.Snappable; // Disable node snapping
            Node = node;
            title = name;
            viewDataKey = node.Guid;

            style.left = node.Position.x;
            style.top = node.Position.y;
            this.Q<VisualElement>("node-border").style.overflow = Overflow.Visible;

            var color = GetColor(_details);

            CreateInputPorts(color);
            CreateOutputPorts(color);
            SetupClasses(color);
            SetupDataBinding();

            this.AddManipulator(new DoubleClickNode());
        }

        private Color GetColor(NodeDetailsAttribute details)
        {
            if (details == null || details.Color == default)
            {
                return Node switch
                {
                    ConditionNode => _defaultNodeColors[typeof(ConditionNode)],
                    ActionNode => _defaultNodeColors[typeof(ActionNode)],
                    CompositeNode => _defaultNodeColors[typeof(CompositeNode)],
                    DecoratorNode => _defaultNodeColors[typeof(DecoratorNode)],
                    RootNode => _defaultNodeColors[typeof(RootNode)],
                    _ => _defaultNodeColors[typeof(Node)]
                };
            }

            var color = details.Color;
            return color;
        }

        public void SetupDataBinding()
        {
            var serializer = BehaviourTreeEditorWindow.Instance.Serializer;
            var nodeProp = serializer.FindNode(Node);
            if (nodeProp == null)
            {
                return;
            }

            if (Node is ActionNode)
            {
                var descriptionLabel = this.Q<Label>("description");
                var name = _details?.Name ?? Node.GetType().Name.ToTitleCase();
                descriptionLabel.text = name;
            }

            var icon = this.Q<VisualElement>("icon");

            if (Node is ConditionNode)
            {
                var invertProperty = nodeProp.FindPropertyRelative("Invert");
                icon.TrackPropertyValue(invertProperty, UpdateConditionNodeClasses);
            }

            if (!string.IsNullOrEmpty(_details?.IconPath) && Directory.Exists(_details?.IconPath))
            {
                var iconTexture = AssetDatabase.LoadAssetAtPath<Texture2D>(_details.IconPath);
                icon.style.backgroundImage = new StyleBackground(iconTexture);
            }

            var hasBreakpointProp = nodeProp.FindPropertyRelative("HasBreakpoint");
            var breakpoint = this.Q<VisualElement>("breakpoint");
            SetBreakpointVisible(breakpoint, hasBreakpointProp);
            breakpoint.TrackPropertyValue(hasBreakpointProp, property => SetBreakpointVisible(breakpoint, property));
        }

        private void SetBreakpointVisible(VisualElement breakpoint, SerializedProperty property)
        {
            breakpoint.style.display = property.boolValue ? DisplayStyle.Flex : DisplayStyle.None;
        }

        private void SetDescriptionField(Label label, string description)
        {
            label.style.display = string.IsNullOrEmpty(description) ? DisplayStyle.None : DisplayStyle.Flex;
            label.text = description;
        }

        private void UpdateConditionNodeClasses(SerializedProperty obj)
        {
            if (obj.boolValue)
                AddToClassList("invert");
            else
                RemoveFromClassList("invert");
        }

        private void SetupClasses(Color color)
        {
            if (Node is ActionNode)
            {
                AddToClassList("action");

                if (Node is ConditionNode conditionNode)
                {
                    AddToClassList("condition");
                    if (conditionNode.Invert)
                    {
                        AddToClassList("invert");
                    }
                }
            }
            else if (Node is CompositeNode)
            {
                AddToClassList("composite");
                switch (Node)
                {
                    case Sequencer:
                        AddToClassList("sequencer");
                        break;
                    case Selector:
                        AddToClassList("selector");
                        break;
                    case Parallel:
                        AddToClassList("parallel");
                        break;
                }
            }
            else if (Node is DecoratorNode)
            {
                AddToClassList("decorator");
            }
            else if (Node is RootNode)
            {
                AddToClassList("root");
            }

            this.Q<VisualElement>("input").style.backgroundColor = color;

            if (Node.HasBreakpoint)
            {
                AddToClassList("breakpoint");
            }
        }

        private void CreateInputPorts(Color color)
        {
            switch (Node)
            {
                case ActionNode:
                case CompositeNode:
                case DecoratorNode:
                    Input = new NodePort(Direction.Input, Port.Capacity.Single);
                    break;
                case RootNode:
                    break;
            }

            if (Input == null) return;

            Input.portName = "";
            // Input.style.flexDirection = FlexDirection.ColumnReverse;
            Input.style.position = Position.Absolute;
            Input.style.left = new StyleLength(Length.Percent(50));
            Input.style.top = -18;
            Input.style.translate = new StyleTranslate(new Translate(Length.Percent(-50), 0));
            Input.style.justifyContent = Justify.Center;
            Input.Q<Label>("type").SetMargin(0);
            Input.portColor = color;
            var background = new VisualElement
            {
                style =
                {
                    backgroundColor = color,
                    position = Position.Absolute,
                    left = new StyleLength(Length.Percent(50)),
                    translate = new StyleTranslate(new Translate(Length.Percent(-50), 0)),
                    width = 18,
                    height = 18,
                    borderTopLeftRadius = 50,
                    borderTopRightRadius = 50
                }
            };
            Input.Insert(0,background);
            // Input.Q<VisualElement>("connector").style.backgroundColor = color;
            // Input.Q<VisualElement>("connector").SetPadding(4);
            // Add(Input);
            inputContainer.Insert(0,Input);
        }

        private void CreateOutputPorts(Color color)
        {
            switch (Node)
            {
                case ActionNode:
                    // Actions have no outputs
                    break;
                case CompositeNode:
                    Output = new NodePort(Direction.Output, Port.Capacity.Multi);
                    break;
                case DecoratorNode:
                case RootNode:
                    Output = new NodePort(Direction.Output, Port.Capacity.Single);
                    break;
            }

            if (Output == null) return;

            Output.portName = "";
            Output.style.position = Position.Absolute;
            Output.style.left = new StyleLength(Length.Percent(50));
            Output.style.top = new StyleLength(Length.Percent(100));
            Output.style.translate = new StyleTranslate(new Translate(Length.Percent(-50), Length.Percent(-25)));
            Output.style.justifyContent = Justify.Center;
            Output.Q<Label>("type").SetMargin(0);
            Output.portColor = color;
            var background = new VisualElement
            {
                style =
                {
                    backgroundColor = new Color(.22f, .22f, .22f),
                    position = Position.Absolute,
                    left = new StyleLength(Length.Percent(50)),
                    translate = new StyleTranslate(new Translate(Length.Percent(-50), 0)),
                    width = 18,
                    height = 18,
                    borderBottomLeftRadius = 50,
                    borderBottomRightRadius = 50
                }
            };
            Output.Insert(0,background);

            // Add(Output);
            outputContainer.Insert(0,Output);
        }

        public override void SetPosition(Rect newPos)
        {
            var projectSettings = BehaviourTreeEditorWindow.Instance.Settings;

            newPos.x = EditorUtility.SnapTo(newPos.x, projectSettings.GridSnapSizeX);
            newPos.y = EditorUtility.SnapTo(newPos.y, projectSettings.GridSnapSizeY);

            base.SetPosition(newPos);

            var serializer = BehaviourTreeEditorWindow.Instance.Serializer;
            var position = new Vector2(newPos.xMin, newPos.yMin);
            serializer.SetNodePosition(Node, position);
        }

        public override void OnSelected()
        {
            base.OnSelected();
            OnNodeSelected?.Invoke(this);
        }

        public void SortChildren()
        {
            if (Node is CompositeNode composite)
            {
                composite.Children.Sort(SortByHorizontalPosition);
            }
        }

        private int SortByHorizontalPosition(Node left, Node right)
        {
            return left.Position.x < right.Position.x ? -1 : 1;
        }

        public void UpdateState(Dictionary<string, Node.State> tickResults)
        {
            if (!Application.isPlaying) return;

            if (tickResults.TryGetValue(Node.Guid, out var tickResult))
            {
                ApplyActiveNodeStateStyle(tickResult);
            }
            else
            {
                ApplyInactiveNodeStateStyle();
            }
        }

        private void ApplyActiveNodeStateStyle(Node.State state)
        {
            style.borderLeftWidth = 5;
            style.borderRightWidth = 5;
            style.borderTopWidth = 5;
            style.borderBottomWidth = 5;
            style.opacity = 1.0f;

            switch (state)
            {
                case Node.State.Success:
                    style.borderLeftColor = Color.green;
                    style.borderRightColor = Color.green;
                    style.borderTopColor = Color.green;
                    style.borderBottomColor = Color.green;
                    break;
                case Node.State.Failure:
                    style.borderLeftColor = Color.red;
                    style.borderRightColor = Color.red;
                    style.borderTopColor = Color.red;
                    style.borderBottomColor = Color.red;
                    break;
                case Node.State.Running:
                    style.borderLeftColor = Color.yellow;
                    style.borderRightColor = Color.yellow;
                    style.borderTopColor = Color.yellow;
                    style.borderBottomColor = Color.yellow;
                    break;
            }
        }

        private void ApplyInactiveNodeStateStyle()
        {
            style.borderLeftWidth = 0;
            style.borderRightWidth = 0;
            style.borderTopWidth = 0;
            style.borderBottomWidth = 0;
            style.opacity = 0.5f;
        }
    }
}
