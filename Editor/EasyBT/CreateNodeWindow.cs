using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Extensions;
using DesignPatterns.EasyBT;
using DesignPatterns.EasyBT.Attributes;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyBT
{
    public class CreateNodeWindow : ScriptableObject, ISearchWindowProvider
    {
        private Texture2D _icon;
        private BehaviourTreeView _treeView;
        private NodeView _source;
        private bool _isSourceParent;
        private EditorUtility.ScriptTemplate[] _scriptFileAssets;

        private TextAsset GetScriptTemplate(int type)
        {
            var projectSettings = BehaviourTreeProjectSettings.GetOrCreateSettings();

            switch (type)
            {
                case 0:
                    if (projectSettings.ScriptTemplateActionNode)
                    {
                        return projectSettings.ScriptTemplateActionNode;
                    }

                    return BehaviourTreeEditorWindow.Instance.ScriptTemplateActionNode;
                case 1:
                    if (projectSettings.ScriptTemplateConditionNode)
                    {
                        return projectSettings.ScriptTemplateConditionNode;
                    }

                    return BehaviourTreeEditorWindow.Instance.ScriptTemplateConditionNode;
                case 2:
                    if (projectSettings.ScriptTemplateCompositeNode)
                    {
                        return projectSettings.ScriptTemplateCompositeNode;
                    }

                    return BehaviourTreeEditorWindow.Instance.ScriptTemplateCompositeNode;
                case 3:
                    if (projectSettings.ScriptTemplateDecoratorNode)
                    {
                        return projectSettings.ScriptTemplateDecoratorNode;
                    }

                    return BehaviourTreeEditorWindow.Instance.ScriptTemplateDecoratorNode;
            }

            Debug.LogError("Unhandled script template type:" + type);
            return null;
        }

        public void Initialize(BehaviourTreeView treeView, NodeView source, bool isSourceParent)
        {
            _treeView = treeView;
            _source = source;
            _isSourceParent = isSourceParent;

            _icon = new Texture2D(1, 1);
            _icon.SetPixel(0, 0, new Color(0, 0, 0, 0));
            _icon.Apply();

            _scriptFileAssets = new EditorUtility.ScriptTemplate[]
            {
                new() { TemplateFile = GetScriptTemplate(0), DefaultFileName = "NewActionNode", SubFolder = "Actions" },
                new()
                {
                    TemplateFile = GetScriptTemplate(1), DefaultFileName = "NewConditionNode", SubFolder = "Conditions"
                },
                new()
                {
                    TemplateFile = GetScriptTemplate(2), DefaultFileName = "NewCompositeNode", SubFolder = "Composites"
                },
                new()
                {
                    TemplateFile = GetScriptTemplate(3), DefaultFileName = "NewDecoratorNode", SubFolder = "Decorators"
                },
            };
        }

        public List<SearchTreeEntry> CreateSearchTree(SearchWindowContext context)
        {
            var tree = new List<SearchTreeEntry>
            {
                new SearchTreeGroupEntry(new GUIContent("Create Node"))
            };

            if (_isSourceParent || _source == null)
            {
                tree.Add(new SearchTreeGroupEntry(new GUIContent("Actions")) { level = 1 });
                var types = TypeCache.GetTypesDerivedFrom<ActionNode>().ToList();
                types = types.Where(type => !type.IsSubclassOf(typeof(ConditionNode)) && type != typeof(ConditionNode))
                    .ToList();
                AddTypesToTree("Actions", types, tree, context);
            }

            if (_isSourceParent || _source == null)
            {
                tree.Add(new SearchTreeGroupEntry(new GUIContent("Conditions")) { level = 1 });
                var types = TypeCache.GetTypesDerivedFrom<ConditionNode>().ToList();
                AddTypesToTree("Conditions", types, tree, context);
            }

            {
                tree.Add(new SearchTreeGroupEntry(new GUIContent("Composites")) { level = 1 });
                {
                    var types = TypeCache.GetTypesDerivedFrom<CompositeNode>().ToList();
                    AddTypesToTree("Composites", types, tree, context);
                }
            }

            {
                tree.Add(new SearchTreeGroupEntry(new GUIContent("Decorators")) { level = 1 });
                {
                    var types = TypeCache.GetTypesDerivedFrom<DecoratorNode>().ToList();
                    AddTypesToTree("Decorators", types, tree, context);
                }
            }

            {
                tree.Add(new SearchTreeGroupEntry(new GUIContent("New Script...")) { level = 1 });

                Action createActionScript = () => CreateScript(_scriptFileAssets[0], context);
                tree.Add(new SearchTreeEntry(new GUIContent($"New Action Script"))
                    { level = 2, userData = createActionScript });

                Action createConditionScript = () => CreateScript(_scriptFileAssets[1], context);
                tree.Add(new SearchTreeEntry(new GUIContent($"New Condition Script"))
                    { level = 2, userData = createConditionScript });

                Action createCompositeScript = () => CreateScript(_scriptFileAssets[2], context);
                tree.Add(new SearchTreeEntry(new GUIContent($"New Composite Script"))
                    { level = 2, userData = createCompositeScript });

                Action createDecoratorScript = () => CreateScript(_scriptFileAssets[3], context);
                tree.Add(new SearchTreeEntry(new GUIContent($"New Decorator Script"))
                    { level = 2, userData = createDecoratorScript });
            }


            return tree;
        }

        private void AddTypesToTree(string parentPath, List<Type> types, List<SearchTreeEntry> tree,
            SearchWindowContext context)
        {
            types.Sort((x, y) =>
            {
                var xDetails = x.GetCustomAttribute<NodeDetailsAttribute>(false);
                var yDetails = y.GetCustomAttribute<NodeDetailsAttribute>(false);
                switch (xDetails)
                {
                    case null when yDetails == null:
                        return 0;
                    case null:
                        return 1;
                }

                if (yDetails == null)
                    return -1;

                return string.Compare(xDetails.Name, yDetails.Name, StringComparison.Ordinal);
            });
            foreach (var type in types)
            {
                var details =
                    type.GetCustomAttribute<NodeDetailsAttribute>(false);
                var nodeName = details?.Name ?? type.Name.ToTitleCase();
                var deepestCategoryIndex = 1;
                var groupIndex = 0;
                Action invoke = () => CreateNode(type, context);

                if (!string.IsNullOrEmpty(details?.CategoryPath))
                {
                    var categories = details.CategoryPath.Split('/');
                    foreach (var category in categories)
                    {
                        var fullPath = $"{parentPath}/{details.CategoryPath}";
                        groupIndex = tree.FindIndex(x => x.userData is string savedPath && savedPath == fullPath);
                        deepestCategoryIndex++;
                        if (groupIndex == -1)
                        {
                            var group = new SearchTreeGroupEntry(new GUIContent(category))
                            {
                                level = deepestCategoryIndex,
                                userData = fullPath
                            };
                            tree.Add(group);
                            groupIndex = tree.Count - 1;
                        }
                    }

                    tree.Insert(groupIndex + 1,
                        new SearchTreeEntry(new GUIContent($"{nodeName}"))
                            { level = deepestCategoryIndex + 1, userData = invoke });
                }
                else
                {
                    tree.Add(new SearchTreeEntry(new GUIContent($"{nodeName}"))
                        { level = 2, userData = invoke });
                }
            }
        }

        public bool OnSelectEntry(SearchTreeEntry searchTreeEntry, SearchWindowContext context)
        {
            var invoke = (Action)searchTreeEntry.userData;
            invoke?.Invoke();
            return true;
        }

        public void CreateNode(Type type, SearchWindowContext context)
        {
            var editorWindow = BehaviourTreeEditorWindow.Instance;

            var windowMousePosition = editorWindow.rootVisualElement.ChangeCoordinatesTo(
                editorWindow.rootVisualElement.parent, context.screenMousePosition - editorWindow.position.position);
            var graphMousePosition = editorWindow.TreeView.contentViewContainer.WorldToLocal(windowMousePosition);
            var nodeOffset = new Vector2(-75, -20);
            var nodePosition = graphMousePosition + nodeOffset;

            // #TODO: Unify this with CreatePendingScriptNode
            NodeView createdNode;
            if (_source != null)
            {
                if (_isSourceParent)
                {
                    createdNode = _treeView.CreateNode(type, nodePosition, _source);
                }
                else
                {
                    createdNode = _treeView.CreateNodeWithChild(type, nodePosition, _source);
                }
            }
            else
            {
                createdNode = _treeView.CreateNode(type, nodePosition, null);
            }

            _treeView.SelectNode(createdNode);
        }

        public void CreateScript(EditorUtility.ScriptTemplate scriptTemplate, SearchWindowContext context)
        {
            var editorWindow = BehaviourTreeEditorWindow.Instance;

            var windowMousePosition = editorWindow.rootVisualElement.ChangeCoordinatesTo(
                editorWindow.rootVisualElement.parent, context.screenMousePosition - editorWindow.position.position);
            var graphMousePosition = editorWindow.TreeView.contentViewContainer.WorldToLocal(windowMousePosition);
            var nodeOffset = new Vector2(-75, -20);
            var nodePosition = graphMousePosition + nodeOffset;

            EditorUtility.CreateNewScript(scriptTemplate, _source, _isSourceParent, nodePosition);
        }

        public static void Show(Vector2 mousePosition, NodeView source, bool isSourceParent = false)
        {
            var screenPoint = GUIUtility.GUIToScreenPoint(mousePosition);
            var searchWindowProvider = CreateInstance<CreateNodeWindow>();
            searchWindowProvider.Initialize(BehaviourTreeEditorWindow.Instance.TreeView, source, isSourceParent);
            var windowContext = new SearchWindowContext(screenPoint, 240, 320);
            SearchWindow.Open(windowContext, searchWindowProvider);
        }
    }
}
