using DesignPatterns.EasyBT;
using UnityEditor;
using UnityEngine;

namespace Editor.EasyBT
{
    // This is a helper class which wraps a serialized object for finding properties on the behaviour.
    // It's best to modify the behaviour tree via SerializedObjects and SerializedProperty interfaces
    // to keep the UI in sync, and undo/redo
    // It's a hodge podge mix of various functions that will evolve over time. It's not exhaustive by any means.
    [System.Serializable]
    public class SerializedBehaviourTree
    {
        // Wrapper serialized object for writing changes to the behaviour tree
        public SerializedObject SerializedObject;

        public BehaviourTree Tree;

        // Property names. These correspond to the variable names on the behaviour tree

        private const string SPropRootNode = nameof(BehaviourTree.RootNode);
        private const string SPropNodes = nameof(BehaviourTree.Nodes);
        private const string SPropBlackboard = nameof(BehaviourTree.Blackboard);
        private const string SViewTransformPosition = nameof(BehaviourTree.ViewPosition);
        private const string SViewTransformScale = nameof(BehaviourTree.ViewScale);

        private const string SPropBlackboardKeys = nameof(global::DesignPatterns.EasyBT.Blackboard.Keys);

        private const string SPropGuid = nameof(Node.Guid);
        private const string SPropPosition = nameof(Node.Position);
        private const string SPropHasBreakpoint = nameof(Node.HasBreakpoint);
        private const string SPropChild = nameof(DecoratorNode.Child);
        private const string SPropChildren = nameof(CompositeNode.Children);

        private bool _batchMode;

        public SerializedProperty RootNode => SerializedObject.FindProperty(SPropRootNode);

        public SerializedProperty Nodes => SerializedObject.FindProperty(SPropNodes);

        public SerializedProperty Blackboard => SerializedObject.FindProperty(SPropBlackboard);

        public SerializedProperty BlackboardKeys =>
            SerializedObject.FindProperty($"{SPropBlackboard}.{SPropBlackboardKeys}");

        public SerializedBehaviourTree(BehaviourTree tree)
        {
            SerializedObject = new SerializedObject(tree);
            Tree = tree;
        }

        public SerializedProperty FindNode(Node node)
        {
            return FindNode(Nodes, node);
        }

        public SerializedProperty FindNode(SerializedProperty array, Node node)
        {
            for (var i = 0; i < array.arraySize; ++i)
            {
                var current = array.GetArrayElementAtIndex(i);
                if (current.FindPropertyRelative(SPropGuid).stringValue == node.Guid)
                {
                    return current;
                }
            }

            return null;
        }

        public void SetViewTransform(Vector3 position, Vector3 scale)
        {
            SerializedObject.FindProperty(SViewTransformPosition).vector3Value = position;
            SerializedObject.FindProperty(SViewTransformScale).vector3Value = scale;
            SerializedObject.ApplyModifiedPropertiesWithoutUndo();
        }

        public void SetNodePosition(Node node, Vector2 position)
        {
            var nodeProp = FindNode(Nodes, node);
            nodeProp.FindPropertyRelative(SPropPosition).vector2Value = position;
            ApplyChanges();
        }

        public void RemoveNodeArrayElement(SerializedProperty array, Node node)
        {
            for (var i = 0; i < array.arraySize; ++i)
            {
                var current = array.GetArrayElementAtIndex(i);
                if (current.FindPropertyRelative(SPropGuid).stringValue == node.Guid)
                {
                    array.DeleteArrayElementAtIndex(i);
                    return;
                }
            }
        }

        public Node CreateNodeInstance(System.Type type)
        {
            var node = System.Activator.CreateInstance(type) as Node;
            node.Guid = GUID.Generate().ToString();
            return node;
        }

        private SerializedProperty AppendArrayElement(SerializedProperty arrayProperty)
        {
            arrayProperty.InsertArrayElementAtIndex(arrayProperty.arraySize);
            return arrayProperty.GetArrayElementAtIndex(arrayProperty.arraySize - 1);
        }

        public Node CreateNode(System.Type type, Vector2 position)
        {
            var child = CreateNodeInstance(type);
            child.Position = position;

            var newNode = AppendArrayElement(Nodes);
            newNode.managedReferenceValue = child;

            ApplyChanges();

            return child;
        }

        public void SetRootNode(RootNode node)
        {
            RootNode.managedReferenceValue = node;
            ApplyChanges();
        }

        public void DeleteNode(Node node)
        {
            var nodesProperty = Nodes;

            for (var i = 0; i < nodesProperty.arraySize; ++i)
            {
                var prop = nodesProperty.GetArrayElementAtIndex(i);
                var guid = prop.FindPropertyRelative(SPropGuid).stringValue;
                RemoveNodeArrayElement(Nodes, node);
            }

            ApplyChanges();
        }

        public void AddChild(Node parent, Node child)
        {
            var parentProperty = FindNode(Nodes, parent);

            // RootNode, Decorator node
            var childProperty = parentProperty.FindPropertyRelative(SPropChild);
            if (childProperty != null)
            {
                childProperty.managedReferenceValue = child;
                ApplyChanges();
                return;
            }

            // Composite nodes
            var childrenProperty = parentProperty.FindPropertyRelative(SPropChildren);
            if (childrenProperty != null)
            {
                var newChild = AppendArrayElement(childrenProperty);
                newChild.managedReferenceValue = child;
                ApplyChanges();
                return;
            }
        }

        public void RemoveChild(Node parent, Node child)
        {
            var parentProperty = FindNode(Nodes, parent);

            // RootNode, Decorator node
            var childProperty = parentProperty.FindPropertyRelative(SPropChild);
            if (childProperty != null)
            {
                childProperty.managedReferenceValue = null;
                ApplyChanges();
                return;
            }

            // Composite nodes
            var childrenProperty = parentProperty.FindPropertyRelative(SPropChildren);
            if (childrenProperty != null)
            {
                RemoveNodeArrayElement(childrenProperty, child);
                ApplyChanges();
                return;
            }
        }

        public void CreateBlackboardKey(string keyName, System.Type keyType)
        {
            var key = BlackboardKey.CreateKey(keyType);
            if (key != null)
            {
                key.Name = keyName;
                var keysArray = BlackboardKeys;
                keysArray.InsertArrayElementAtIndex(keysArray.arraySize);
                var newKey = keysArray.GetArrayElementAtIndex(keysArray.arraySize - 1);

                newKey.managedReferenceValue = key;

                ApplyChanges();
            }
            else
            {
                Debug.LogError($"Failed to create blackboard key, invalid type:{keyType}");
            }
        }

        public void DeleteBlackboardKey(string keyName)
        {
            var keysArray = BlackboardKeys;
            for (var i = 0; i < keysArray.arraySize; ++i)
            {
                var key = keysArray.GetArrayElementAtIndex(i);
                var itemKey = key.managedReferenceValue as BlackboardKey;
                if (itemKey.Name == keyName)
                {
                    keysArray.DeleteArrayElementAtIndex(i);
                    ApplyChanges();
                    return;
                }
            }
        }

        public void ToggleBreakpoint(Node node)
        {
            var nodeProp = FindNode(node);
            nodeProp.FindPropertyRelative(SPropHasBreakpoint).boolValue = !node.HasBreakpoint;
            ApplyChanges();
        }

        public void BeginBatch()
        {
            _batchMode = true;
        }

        public void ApplyChanges()
        {
            if (!_batchMode)
            {
                SerializedObject.ApplyModifiedProperties();
            }
        }

        public void EndBatch()
        {
            _batchMode = false;
            ApplyChanges();
        }
    }
}
