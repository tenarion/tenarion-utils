using System.Reflection;
using Core.Extensions;
using DesignPatterns.EasyBT.Attributes;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using Toggle = UnityEngine.UIElements.Toggle;

namespace Editor.EasyBT
{
    public class InspectorView : VisualElement
    {
        private Label _nodeName;
        private Label _nodeType;
        private Label _descriptionLabel;
        private VisualElement _fieldContainer;

        public InspectorView()
        {
            var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(
                "Packages/com.tenarion.easyutils/Editor/EasyBT/UIBuilder/InspectorView.uxml");
            visualTree.CloneTree(this);

            _nodeName = this.Q<Label>("node-name");
            _nodeType = this.Q<Label>("node-type");
            _descriptionLabel = this.Q<Label>("description-label");
            _fieldContainer = this.Q<VisualElement>("field-container");

            _descriptionLabel.RegisterValueChangedCallback(OnDescriptionLabelValueChanged);
            RegisterCallback<AttachToPanelEvent>(OnAttachToPanel);
        }

        private void OnAttachToPanel(AttachToPanelEvent evt)
        {
            if (GetFirstAncestorOfType<FloatingPanel>() == null) return;

            var floatingPanel = GetFirstAncestorOfType<FloatingPanel>();
            floatingPanel.Title = "Inspector";
        }

        private void OnDescriptionLabelValueChanged(ChangeEvent<string> evt) => _descriptionLabel.style.display =
            string.IsNullOrEmpty(evt.newValue) ? DisplayStyle.None : DisplayStyle.Flex;

        internal void UpdateSelection(SerializedBehaviourTree serializer, NodeView nodeView)
        {
            ClearView();

            if (nodeView == null)
                return;

            var nodeProperty = serializer.FindNode(nodeView.Node);
            if (nodeProperty == null)
                return;

            nodeProperty.isExpanded = true;

            // Property field
            var field = new PropertyField();
            field.BindProperty(nodeProperty);
            field.RegisterCallback<ChangeEvent<string>>(evt => field.Q<Toggle>().style.display = DisplayStyle.None);

            var type = nodeView.Node.GetType();
            var nodeDetails = type.GetCustomAttribute<NodeDetailsAttribute>();

            if (nodeDetails != null)
            {
                _nodeName.text = nodeDetails.Name ?? type.Name.ToTitleCase();
                _nodeType.text = type.BaseType?.Name.ToTitleCase().Split(" ")[0];
                _descriptionLabel.text = nodeDetails.Description;
            }
            else
            {
                _nodeName.text = type.Name.ToTitleCase();
                _nodeType.text = type.BaseType?.Name.ToTitleCase().Split(" ")[0];
                _descriptionLabel.text = "";
            }

            _fieldContainer.Add(field);
        }

        public void ClearView()
        {
            _fieldContainer?.Clear();
            if (_nodeName != null) _nodeName.text = "";
            if (_nodeType != null) _nodeType.text = "";
            if (_descriptionLabel != null) _descriptionLabel.text = "";
        }
    }
}
