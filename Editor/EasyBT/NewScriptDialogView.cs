using System.IO;
using System.Text.RegularExpressions;
using Core.Extensions;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyBT
{
    [UxmlElement]
    public partial class NewScriptDialogView : VisualElement
    {
        private EditorUtility.ScriptTemplate _scriptTemplate;
        private TextField _textField;
        private TextField _descriptionField;
        private TextField _categoryPathField;
        private ColorField _colorField;
        private ObjectField _iconField;
        private Button _confirmButton;
        private NodeView _source;
        private bool _isSourceParent;
        private Vector2 _nodePosition;

        public void CreateScript(EditorUtility.ScriptTemplate scriptTemplate, NodeView source, bool isSourceParent,
            Vector2 position)
        {
            _scriptTemplate = scriptTemplate;
            _source = source;
            _isSourceParent = isSourceParent;
            _nodePosition = position;

            style.visibility = Visibility.Visible;

            var background = this.Q<VisualElement>("Background");
            var titleLabel = this.Q<Label>("Title");
            _textField = this.Q<TextField>("FileName");
            _descriptionField = this.Q<TextField>("Description");
            _categoryPathField = this.Q<TextField>("CategoryPath");
            _colorField = this.Q<ColorField>("Color");
            _iconField = this.Q<ObjectField>("Icon");
            _confirmButton = this.Q<Button>();

            titleLabel.text = $"New {scriptTemplate.SubFolder.TrimEnd('s')} Script";

            _textField.focusable = true;
            RegisterCallback<PointerEnterEvent>(e => { _textField[0].Focus(); });

            _textField.RegisterCallback<KeyDownEvent>(e =>
            {
                if (e.keyCode == KeyCode.Return || e.keyCode == KeyCode.KeypadEnter)
                {
                    OnConfirm();
                }
            });

            _confirmButton.clicked -= OnConfirm;
            _confirmButton.clicked += OnConfirm;

            background.RegisterCallback<PointerDownEvent>(e =>
            {
                e.StopImmediatePropagation();
                Close();
            });
        }

        private void Close()
        {
            style.visibility = Visibility.Hidden;
        }

        private void OnConfirm()
        {
            var scriptName = Regex.Replace(_textField.text, @"\s+", "");
            var description = _descriptionField.text;
            var categoryPath = _categoryPathField.text;
            var color = _colorField.value;
            var icon = _iconField.value;

            var absolutePath = UnityEditor.EditorUtility.SaveFilePanel("Create New",
                BehaviourTreeEditorWindow.Instance.Settings.NewNodePath, scriptName, "cs");

            if (string.IsNullOrEmpty(absolutePath)) return;

            var relativePath = absolutePath.Replace(Application.dataPath, "Assets");
            var relativeFolder = Path.GetDirectoryName(relativePath);

            if (AssetDatabase.IsValidFolder(relativeFolder))
            {
                var templateString = _scriptTemplate.TemplateFile.text;
                templateString = templateString.Replace("#SCRIPTNAME#", scriptName);
                templateString = templateString.Replace("#DESCRIPTION#", description.WithoutBreakLines());
                templateString = templateString.Replace("#NODENAME#", scriptName.ToTitleCase());
                templateString = templateString.Replace("#CATEGORYPATH#", categoryPath);
                templateString = templateString.Replace("#COLOR#",
                    $"0x{(int)(color.r * 255):X2}{(int)(color.g * 255):X2}{(int)(color.b * 255):X2}");
                templateString =
                    templateString.Replace("#ICON#", icon != null ? AssetDatabase.GetAssetPath(icon) : null);

                if (!File.Exists(absolutePath))
                {
                    File.WriteAllText(absolutePath, templateString);

                    // TODO: There must be a better way to survive domain reloads after script compiling than this
                    BehaviourTreeEditorWindow.Instance.PendingScriptCreation.PendingCreate = true;
                    BehaviourTreeEditorWindow.Instance.PendingScriptCreation.ScriptName = scriptName;
                    BehaviourTreeEditorWindow.Instance.PendingScriptCreation.NodePosition = _nodePosition;
                    if (_source != null)
                    {
                        BehaviourTreeEditorWindow.Instance.PendingScriptCreation.SourceGuid = _source.Node.Guid;
                        BehaviourTreeEditorWindow.Instance.PendingScriptCreation.IsSourceParent = _isSourceParent;
                    }

                    AssetDatabase.Refresh();
                    _confirmButton.SetEnabled(false);
                    EditorApplication.delayCall += WaitForCompilation;
                }
                else
                {
                    Debug.LogError($"Script with that name already exists:{absolutePath}");
                    Close();
                }
            }
            else
            {
                Debug.LogError(
                    $"Invalid folder path:{relativeFolder}. Check the project configuration settings 'newNodePath' is configured to a valid folder");
            }
        }

        private void WaitForCompilation()
        {
            if (EditorApplication.isCompiling)
            {
                EditorApplication.delayCall += WaitForCompilation;
                return;
            }

            _confirmButton.SetEnabled(true);
            Close();
        }
    }
}
