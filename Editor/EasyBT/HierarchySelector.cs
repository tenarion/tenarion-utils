using DesignPatterns.EasyBT;
using UnityEngine.UIElements;

namespace Editor.EasyBT
{
    public class HierarchySelector : MouseManipulator
    {
        protected override void RegisterCallbacksOnTarget()
        {
            target.RegisterCallback<MouseDownEvent>(OnMouseDown);
        }

        protected override void UnregisterCallbacksFromTarget()
        {
            target.UnregisterCallback<MouseDownEvent>(OnMouseDown);
        }

        private void OnMouseDown(MouseDownEvent evt)
        {
            if (!CanStopManipulation(evt))
                return;

            if (target is not BehaviourTreeView graphView)
                return;

            if (evt.target is not NodeView clickedElement)
            {
                var ve = evt.target as VisualElement;
                clickedElement = ve.GetFirstAncestorOfType<NodeView>();
                if (clickedElement == null)
                    return;
            }

            var forceSelectNodeHierarchy = BehaviourTreeEditorWindow.Instance.Settings.AutoSelectNodeHierarchy;
            if (!evt.ctrlKey && !forceSelectNodeHierarchy) return;

            graphView.ClearSelection();
            SelectChildren(evt, graphView, clickedElement);
        }

        private void SelectChildren(MouseDownEvent evt, BehaviourTreeView graphView, NodeView clickedElement)
        {
            // Add children to selection so the root element can be moved
            BehaviourTree.Traverse(clickedElement.Node, node =>
            {
                var view = graphView.FindNodeView(node);
                graphView.AddToSelection(view);
            });

            // Insepctor should always focus on root element so this needs to be selected last
            graphView.RemoveFromSelection(clickedElement);
            graphView.AddToSelection(clickedElement);
        }
    }
}
