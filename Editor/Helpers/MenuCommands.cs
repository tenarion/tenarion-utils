using UnityEditor;
using UnityEngine;

namespace Editor.Helpers
{
    public static class MenuCommands
    {
        [MenuItem("CONTEXT/ScriptableObject/Save")]
        public static void SaveScriptableObject(MenuCommand command)
        {
            var obj = command.context;
            if (obj is not ScriptableObject scriptableObject) return;

            EditorUtility.SetDirty(scriptableObject);
            AssetDatabase.SaveAssets();
        }

        [MenuItem("CONTEXT/ScriptableObject/Save", true)]
        public static bool ValidateSaveScriptableObject(MenuCommand command) => command.context is ScriptableObject;
    }
}
