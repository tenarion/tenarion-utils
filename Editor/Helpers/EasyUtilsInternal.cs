using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using UnityEditor;
using UnityEditorInternal;

namespace Editor.Helpers
{
    internal static class EasyUtilsInternal
    {
        // ReSharper disable all UnusedMember.Global
        // ReSharper disable all InconsistentNaming

        // Selected via reflection
        public const string EASY_SAVE_AVAILABLE = "EASY_SAVE_AVAILABLE";
        public const string EASY_IDENTIFICATION_AVAILABLE = "EASY_IDENTIFICATION_AVAILABLE";
        public const string EASY_STATE_MACHINE_AVAILABLE = "EASY_STATE_MACHINE_AVAILABLE";
        public const string EASY_SINGLETON_AVAILABLE = "EASY_SINGLETON_AVAILABLE";
        public const string EASY_BEHAVIOUR_TREE_AVAILABLE = "EASY_BEHAVIOUR_TREE_AVAILABLE";
        public const string EASY_EVENT_BUS_AVAILABLE = "EASY_EVENT_BUS_AVAILABLE";
        public const string EASY_EVENT_CHANNELS_AVAILABLE = "EASY_EVENT_CHANNELS_AVAILABLE";
        public const string EASY_POOL_AVAILABLE = "EASY_POOL_AVAILABLE";
        public const string EASY_WFC_AVAILABLE = "EASY_WFC_AVAILABLE";
        public const string EASY_UI_AVAILABLE = "EASY_UI_AVAILABLE";
        public const string EASY_DEBUG_AVAILABLE = "EASY_DEBUG_AVAILABLE";
        public const string EASY_INTERFACES_AVAILABLE = "EASY_INTERFACES_AVAILABLE";
        //

        internal static List<string> GetSymbols(bool formatted = false)
        {
            return typeof(EasyUtilsInternal).GetFields().Select(f =>
                formatted ? GetFormattedSymbol(f.GetValue(null).ToString()) : f.GetValue(null).ToString()).ToList();
        }

        internal static string GetFormattedSymbol(string symbol) => symbol.ToLowerInvariant().ToTitleCase().Replace("Available", "").Trim();

        /// <summary>
        /// Gets the first found AssemblyDefinitionAsset based in if it contains a symbol on the define constraints.
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        internal static AssemblyDefinitionAsset GetAssemblyDefinitionAssetBySymbol(string symbol)
        {
            var assets = GetAssemblyDefinitionAssetsBySymbol(symbol);
            return assets.FirstOrDefault();
        }

        internal static List<AssemblyDefinitionAsset> GetAssemblyDefinitionAssetsBySymbol(string symbol)
        {
            var assemblyDefinitionAssets = AssetDatabase.FindAssets("t:AssemblyDefinitionAsset")
                .Select(AssetDatabase.GUIDToAssetPath)
                .Select(AssetDatabase.LoadAssetAtPath<AssemblyDefinitionAsset>)
                .ToList();

            return assemblyDefinitionAssets.Where(ada => ada.text.Contains(symbol)).ToList();
        }

        internal static AssemblyDefinitionAsset GetAssemblyDefinitionAssetByGuid(string guid)
        {
            var path = AssetDatabase.GUIDToAssetPath(guid);

            return string.IsNullOrEmpty(path) ? null : AssetDatabase.LoadAssetAtPath<AssemblyDefinitionAsset>(path);
        }

        internal static AssemblyDefinitionAsset GetAssemblyDefinitionAssetByName(string name)
        {
            var path = AssetDatabase.FindAssets(name)
                .Select(AssetDatabase.GUIDToAssetPath)
                .FirstOrDefault();

            return string.IsNullOrEmpty(path) ? null : AssetDatabase.LoadAssetAtPath<AssemblyDefinitionAsset>(path);
        }

        public static IEnumerable<string> GetSymbolExternalPackages(string symbol)
        {
            switch (symbol)
            {
                case EASY_UI_AVAILABLE:
                    return new[] { "com.unity.timeline" };
                default:
                    return new string[0];
            }
        }
    }
}
