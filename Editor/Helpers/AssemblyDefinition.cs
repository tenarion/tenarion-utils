using System;
using System.Collections.Generic;
using System.Linq;
using Editor.Setup;
using Newtonsoft.Json;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
// ReSharper disable InconsistentNaming

namespace Editor.Helpers
{
    /// <summary>
    /// A class that represents an Assembly Definition file, which is defined as a JSON file.
    /// Note that this class doesn't represent the actual file, but a copy of the data contained in it for easier manipulation.
    /// </summary>
    [Serializable]
    internal class AssemblyDefinition
    {

        public string name;
        public string rootNamespace;
        public string[] references;
        public string[] includePlatforms;
        public string[] excludePlatforms;
        public bool allowUnsafeCode;
        public bool overrideReferences;
        public string[] precompiledReferences;
        public bool autoReferenced;
        public string[] defineConstraints;
        public object[] versionDefines;
        public bool noEngineReferences;

        private AssemblyDefinition()
        {
        }

        public static AssemblyDefinition FromGuid(string guid)
        {
            var path = AssetDatabase.GUIDToAssetPath(guid);

            if (string.IsNullOrEmpty(path))
                return null;

            var asset = AssetDatabase.LoadAssetAtPath<TextAsset>(path);
            return asset == null ? null : JsonConvert.DeserializeObject<AssemblyDefinition>(asset.text);
        }

        /// <summary>
        /// Returns a list of AssemblyDefinition objects based on the symbol.
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public static List<AssemblyDefinition> FromSymbol(string symbol)
        {
            var assets = EasyUtilsInternal.GetAssemblyDefinitionAssetsBySymbol(symbol);
            return assets.Select(FromAsset).ToList();
        }

        public static AssemblyDefinition FromAsset(AssemblyDefinitionAsset asset) =>
            JsonConvert.DeserializeObject<AssemblyDefinition>(asset.text);

        public static AssemblyDefinition FromName(string name)
        {
            var asset = EasyUtilsInternal.GetAssemblyDefinitionAssetByName(name);
            return asset == null ? null : FromAsset(asset);
        }

        /// <summary>
        /// Get the references without the 'GUID:' prefix.
        /// </summary>
        /// <returns></returns>
        public string[] GetFormattedReferences() => references.Select(s => s[(s.LastIndexOf(':') + 1)..]).ToArray();
    }
}
