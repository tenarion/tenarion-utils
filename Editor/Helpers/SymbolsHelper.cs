using Editor.Setup;
using UnityEditor;
using UnityEditor.Build;
using UnityEngine;

namespace Editor.Helpers
{
    public static class SymbolsHelper
    {
        public static void SetSymbol(string symbol)
        {
            var defineSymbols = PlayerSettings.GetScriptingDefineSymbols(
                NamedBuildTarget.FromBuildTargetGroup(EditorUserBuildSettings.selectedBuildTargetGroup));

            if (defineSymbols.Contains(symbol)) return;
            defineSymbols += ";" + symbol;
            PlayerSettings.SetScriptingDefineSymbols(
                NamedBuildTarget.FromBuildTargetGroup(EditorUserBuildSettings.selectedBuildTargetGroup), defineSymbols);
        }

        public static bool HasSymbol(string symbol)
        {
            var defineSymbols = PlayerSettings.GetScriptingDefineSymbols(
                NamedBuildTarget.FromBuildTargetGroup(EditorUserBuildSettings.selectedBuildTargetGroup));
            
            return defineSymbols.Contains(symbol);
        }

        public static void RemoveSymbol(string symbol)
        {
            var defineSymbols = PlayerSettings.GetScriptingDefineSymbols(NamedBuildTarget.FromBuildTargetGroup(EditorUserBuildSettings.selectedBuildTargetGroup));

            if (!defineSymbols.Contains(symbol)) return;
            defineSymbols = defineSymbols.Replace(symbol, "").Replace(";;", ";").Trim(';');
            PlayerSettings.SetScriptingDefineSymbols(NamedBuildTarget.FromBuildTargetGroup(EditorUserBuildSettings.selectedBuildTargetGroup), defineSymbols);
        }
    }
}
