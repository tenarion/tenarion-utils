using System.Linq;
using Editor.Setup.UIBuilder;
using UnityEditor;
using UnityEditor.PackageManager;

namespace Editor.Setup
{
    [InitializeOnLoad]
    internal static class SetupLifecycle
    {
        static SetupLifecycle()
        {
            Events.registeredPackages -= OnPackagesRegistered;
            Events.registeredPackages += OnPackagesRegistered;
        }

        private static void OnPackagesRegistered(PackageRegistrationEventArgs obj)
        {
            if (obj.added.Any(p => p.name == "com.tenarion.easyutils"))
            {
                SetupWindow.ShowWindow();
            }
        }
    }
}
