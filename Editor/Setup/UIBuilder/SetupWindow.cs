using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Editor.Helpers;
using Unity.EditorCoroutines.Editor;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.Setup.UIBuilder
{
    internal class SetupWindow : EditorWindow
    {
        public VisualTreeAsset VisualTreeAsset;
        public List<FeatureItem> Features = new();

        private Button _applyButton;
        private HelpBox _helpBox;

        [MenuItem("Easy Utils/Open Setup Panel")]
        public static void ShowWindow()
        {
            var wnd = GetWindow<SetupWindow>("Easy Utils Setup");
            wnd.minSize = new Vector2(340, 300);
        }

        private void OnEnable()
        {
            Features.Clear();
            Features.Add(new FeatureItem("Easy Utils Core", true));
            var symbols = EasyUtilsInternal.GetSymbols();
            foreach (var symbol in symbols)
                Features.Add(new FeatureItem(EasyUtilsInternal.GetFormattedSymbol(symbol), symbol));
        }

        private void CreateGUI()
        {
            if (VisualTreeAsset == null) return;

            VisualTreeAsset.CloneTree(rootVisualElement);

            var multiColumnListView = rootVisualElement.Q<MultiColumnListView>();
            _applyButton = rootVisualElement.Q<Button>();
            _helpBox = rootVisualElement.Q<HelpBox>();

            _applyButton.clicked += ApplyFeatures;

            multiColumnListView.itemsSource = Features;
            var featureColumn = multiColumnListView.columns["feature"];
            var activeColumn = multiColumnListView.columns["active"];

            activeColumn.makeCell = () => new Label();
            activeColumn.makeCell = () =>
            {
                var toggle = new Toggle
                {
                    style =
                    {
                        alignSelf = Align.Center
                    }
                };
                return toggle;
            };

            featureColumn.bindCell = (e, i) => (e as Label)!.text = Features[i].Title;
            activeColumn.bindCell = (e, i) =>
            {
                var featureItem = Features[i];
                if (featureItem.Title == "Easy Utils Core")
                {
                    (e as Toggle)!.value = true;
                    (e as Toggle)!.SetEnabled(false);
                }
                else
                {
                    (e as Toggle)!.value = featureItem.Active;
                    (e as Toggle)!.RegisterValueChangedCallback(evt =>
                    {
                        featureItem.Active = evt.newValue;
                        ValidateFeatures();
                    });
                }
            };
        }

        private void ApplyFeatures()
        {
            var packagesToInstall = new HashSet<string>();
            var packagesToRemove = new HashSet<string>();
            foreach (var feature in Features.Where(feature => !string.IsNullOrEmpty(feature.Symbol)))
            {
                if (feature.Active)
                {
                    SetFeature(feature);
                    packagesToInstall.UnionWith(EasyUtilsInternal.GetSymbolExternalPackages(feature.Symbol));
                }
                else if (SymbolsHelper.HasSymbol(feature.Symbol))
                {
                    RemoveFeature(feature);
                    packagesToRemove.UnionWith(EasyUtilsInternal.GetSymbolExternalPackages(feature.Symbol));
                }
            }

            EditorCoroutineUtility.StartCoroutineOwnerless(HandlePackagesCoroutine(packagesToInstall,
                packagesToRemove));


            AssetDatabase.Refresh();
        }

        private void SetFeature(FeatureItem feature)
        {
            var symbol = feature.Symbol;
            SymbolsHelper.SetSymbol(symbol);
        }

        private void RemoveFeature(FeatureItem feature)
        {
            SymbolsHelper.RemoveSymbol(feature.Symbol);
        }

        private bool IsFeatureValid(FeatureItem featureItem)
        {
            var dependencies = Features.Where(f => f.Symbol != null && featureItem.Dependencies.Contains(f.Symbol))
                .ToList();
            return dependencies.All(feature => feature.Active);
        }

        private void ValidateFeatures()
        {
            var message = string.Empty;
            var valid = true;
            foreach (var feature in Features.Where(feature => feature.Title != "Easy Utils Core"))
            {
                if (!IsFeatureValid(feature) && feature.Active)
                {
                    valid = false;
                    var dependencies = feature.Dependencies.Where(d => !Features.First(f => f.Symbol == d).Active)
                        .Select(d => Features.First(f => f.Symbol == d).Title).Distinct();

                    message +=
                        $"{feature.Title} depens on {string.Join(", ", dependencies)}\n";
                }
            }

            if (valid)
            {
                _applyButton.SetEnabled(true);
                _helpBox.style.display = DisplayStyle.None;
            }
            else
            {
                _helpBox.text =
                    $"Some features are dependent on others. Please, make sure all dependencies are enabled:\n{message}";
                _helpBox.style.display = DisplayStyle.Flex;
                _applyButton.SetEnabled(false);
            }
        }

        private IEnumerator HandlePackagesCoroutine(IEnumerable<string> packagesToInstall,
            IEnumerable<string> packagesToRemove)
        {
            var listRequest = Client.List(true);
            while (!listRequest.IsCompleted)
            {
                yield return null;
            }

            var availablePackages = listRequest.Result;

            foreach (var packageName in packagesToInstall)
            {
                if (string.IsNullOrEmpty(packageName))
                {
                    Debug.LogError("Package name is null or empty.");
                    yield break;
                }

                if (availablePackages.Any(packageInfo => packageInfo.name == packageName)) yield break;

                var addRequest = Client.Add(packageName);

                while (!addRequest.IsCompleted)
                {
                    yield return null;
                }

                if (addRequest.Status == StatusCode.Failure)
                    Debug.LogError(
                        $"Failed to install the package {packageName} required by the feature. Error : {addRequest.Error.message}");
            }

            foreach (var packageName in packagesToRemove)
            {
                if (string.IsNullOrEmpty(packageName))
                {
                    Debug.LogError("Package name is null or empty.");
                    yield break;
                }

                if (availablePackages.All(packageInfo => packageInfo.name != packageName)) yield break;

                var removeRequest = Client.Remove(packageName);

                while (!removeRequest.IsCompleted)
                {
                    yield return null;
                }

                if (removeRequest.Status == StatusCode.Failure)
                    Debug.LogError(
                        $"Failed to remove the package {packageName} required by the feature. Error : {removeRequest.Error.message}");
            }
        }

        private static bool IsExternalPackage(string guid)
        {
            var path = AssetDatabase.GUIDToAssetPath(guid);
            return !path.Contains("com.tenarion.easyutils");
        }

        internal class FeatureItem : IComparable<FeatureItem>
        {
            public string Title { get; }
            public bool Active { get; set; }
            public string Symbol { get; }
            public List<string> Dependencies { get; }
            public List<AssemblyDefinition> ReferencedAssemblies { get; set; }

            public FeatureItem(string title, string symbol)
            {
                Title = title;
                Active = SymbolsHelper.HasSymbol(symbol);
                Symbol = symbol;

                Dependencies = new List<string>();
                ReferencedAssemblies = AssemblyDefinition.FromSymbol(symbol);
                if (ReferencedAssemblies.Count == 0) return;

                foreach (var asmdef in ReferencedAssemblies)
                {
                    var guids = asmdef.GetFormattedReferences();
                    foreach (var guid in guids)
                    {
                        if (IsExternalPackage(guid))
                            continue;
                        var asmdefReference = AssemblyDefinition.FromGuid(guid);
                        if (asmdefReference != null && asmdefReference.defineConstraints.Length > 0)
                            Dependencies.AddRange(asmdefReference.defineConstraints);
                    }
                }
            }

            public FeatureItem(string title, bool active)
            {
                Title = title;
                Active = active;
                Symbol = null;
                Dependencies = new List<string>();
            }

            public int CompareTo(FeatureItem other)
            {
                if (ReferenceEquals(this, other)) return 0;
                if (ReferenceEquals(null, other)) return 1;
                return string.Compare(Symbol, other.Symbol, StringComparison.Ordinal);
            }
        }
    }
}
