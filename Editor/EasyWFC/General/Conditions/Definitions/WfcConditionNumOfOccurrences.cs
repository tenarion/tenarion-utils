using System;
using EasyWFC.General.Conditions;
using EasyWFC.General.Generation;
using EasyWFC.Nodes;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Editor.EasyWFC.General.Conditions.Definitions
{
    public class WfcConditionNumOfOccurrences : WfcCondition<int>
    {
        public WfcConditionNumOfOccurrences(WfcNode currentNode) : base(currentNode)
        {
        }

        public override string Name { get; set; }
        public override WfcNode TargetNeighbour { get; set; }
        public override int Data { get; set; }
        public Comparator Comparator { get; set; }


        public override bool Evaluate(WfcSnapshot snapshot)
        {
            return Comparator switch
            {
                Comparator.Equal => snapshot.NumOfOccurrences(TargetNeighbour, true) == Data,
                Comparator.NotEqual => snapshot.NumOfOccurrences(TargetNeighbour, true) != Data,
                Comparator.GreaterThan => snapshot.NumOfOccurrences(TargetNeighbour, true) > Data,
                Comparator.LessThan => snapshot.NumOfOccurrences(TargetNeighbour, true) < Data,
                Comparator.GreaterThanOrEqual => snapshot.NumOfOccurrences(TargetNeighbour, true) >= Data,
                Comparator.LessThanOrEqual => snapshot.NumOfOccurrences(TargetNeighbour, true) <= Data,
                _ => throw new ArgumentOutOfRangeException()
            } || snapshot.GlobalNumOfOccurrences(TargetNeighbour) == 0;
        }

        public override LogicGate LogicGate { get; set; }

        public override VisualElement CreateVisualElement()
        {
            var container = new VisualElement
                { style = { flexDirection = FlexDirection.Row } };
            var integerField = new IntegerField
            {
                value = Data
            };
            integerField.RegisterValueChangedCallback(evt => Data = evt.newValue);

            var comparatorField = new EnumField(Comparator);
            comparatorField.RegisterValueChangedCallback(evt => Comparator = (Comparator)evt.newValue);

            var objectField = new ObjectField
            {
                objectType = typeof(WfcNode),
                value = TargetNeighbour,
                allowSceneObjects = false
            };
            objectField.RegisterValueChangedCallback(evt => TargetNeighbour = (WfcNode)evt.newValue);
            container.Add(new Label("Number of occurrences of neighbour "));
            container.Add(objectField);
            container.Add(new Label(" must be "));
            container.Add(comparatorField);
            container.Add(new Label(" to "));
            container.Add(integerField);
            return container;
        }
    }
}