using System;
using EasyWFC.General.Conditions;
using EasyWFC.General.Generation;
using EasyWFC.Nodes;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyWFC.General.Conditions.Definitions
{
    [Serializable]
    public class WfcConditionMustAppear : WfcCondition<WfcNeighbourPosition>
    {
        [field: SerializeField] public override string Name { get; set; }
        public override WfcNode TargetNeighbour { get; set; }
        [field: SerializeField] public override WfcNeighbourPosition Data { get; set; }
        [field: SerializeField] public override LogicGate LogicGate { get; set; }

        public override bool Evaluate(WfcSnapshot snapshot)
        {
            return snapshot.CellIs(Data, TargetNeighbour);
        }

        public override VisualElement CreateVisualElement()
        {
            var container = new VisualElement { style = { flexDirection = FlexDirection.Row } };
            var enumField = new EnumField(Data);
            var objectField = new ObjectField
            {
                objectType = typeof(WfcNode),
                value = TargetNeighbour,
                allowSceneObjects = false
            };

            objectField.RegisterValueChangedCallback(evt => TargetNeighbour = (WfcNode)evt.newValue);
            enumField.RegisterValueChangedCallback(SetDataFromEvent);
            container.Add(new Label("Neighbour "));
            container.Add(objectField);
            container.Add(new Label(" should appear on "));
            container.Add(enumField);
            return container;
        }

        public WfcConditionMustAppear(WfcNode currentNode) : base(currentNode)
        {
        }
    }
}