using Core.Extensions;
using EasyWFC.General.Generation;
using EasyWFC.General.Rules;
using EasyWFC.Nodes;
using Editor.EasyWFC.Nodes.Editors.VisualElements;
using Editor.EasyWFC.Nodes.Editors.VisualElements.Manipulators;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyWFC.Nodes.Editors
{
    [CustomEditor(typeof(WfcNode))]
    [CanEditMultipleObjects]
    public class WfcNodeEditor : UnityEditor.Editor
    {
        private Color _bgColor = new(0.2f, 0.2f, 0.2f);
        private Color _borderColor = new(0.4f, 0.4f, 0.4f);
        private Slider _weightField;
        private FloatField _weightFieldValue;
        private VisualElement _upConstraint;
        private VisualElement _downConstraint;
        private VisualElement _leftConstraint;
        private VisualElement _rightConstraint;

        private VisualElement _contentViewport;
        private WfcNeighbourVisualElement _selectedNeighbour;
        private VisualElementSelection _neighbourSelectionManipulator;


        private WfcNode _node;
        private VisualElement _root;
        private MeshPreview _meshPreview;

        private bool _isUpdatingFromEvent;

        public override VisualElement CreateInspectorGUI()
        {
            _root = new VisualElement
            {
                style =
                {
                    flexDirection = FlexDirection.Column,
                    flexGrow = 1,
                    justifyContent = Justify.FlexStart
                }
            };

            _root.schedule.Execute(_ =>
            {
                // Only do this if the view port is null to save resources.
                if (_contentViewport == null)
                {
                    // Find the template container.
                    var rootVisualContainer = _root.GetFirstAncestorOfType<TemplateContainer>();
                    if (rootVisualContainer != null)
                        // Find the view port element.
                        _contentViewport = rootVisualContainer.Q<VisualElement>("unity-content-viewport");
                }

                // The viewport exists.
                if (_contentViewport != null)
                    // Update the root size to match the entire inspector.
                    _root.style.height = _contentViewport.resolvedStyle.height - 66;
            }).Every(100);
            _root.SetBackgroundColor(_bgColor);
            _root.SetMargin(0);
            _root.SetPadding(10);
            _node = (WfcNode)target;
            if (_node.Prefab != null)
            {
                // Get all MeshFilters in the GameObject
                var meshFilters = _node.Prefab.GetComponentsInChildren<MeshFilter>();
                var combine = new CombineInstance[meshFilters.Length];

                // Combine all meshes into a single mesh
                for (var i = 0; i < meshFilters.Length; i++)
                {
                    combine[i].mesh = meshFilters[i].sharedMesh;
                    combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
                }

                // Create a new mesh and assign the combined mesh to it
                var combinedMesh = new Mesh();
                combinedMesh.CombineMeshes(combine);

                // Create a MeshPreview with the combined mesh
                _meshPreview = new MeshPreview(combinedMesh);
            }

            DrawNode();
            return _root;
        }

        private void DrawNode()
        {
            EditorUtility.SetDirty(_node);
            var idField = new TextField("ID") { value = _node.Id, isReadOnly = true };
            var nameField = new TextField("Name") { value = _node.Name, bindingPath = "Name" };
            nameField.Bind(serializedObject);

            var weightContainer = MakeWeightField();
            var prefabContainer = MakePrefabField();

            var neighbours = MakeNeighbourList();
            var rules = MakeRuleList();

            _root.Add(idField);
            _root.Add(nameField);
            _root.Add(weightContainer);
            _root.Add(prefabContainer);
            _root.Add(rules);
            _root.Add(neighbours);
        }

        private VisualElement MakePrefabField()
        {
            var prefabContainer = new VisualElement { style = { flexDirection = FlexDirection.Column } };
            var prefabField = new ObjectField("Prefab")
            {
                objectType = typeof(GameObject), value = _node.Prefab, bindingPath = "Prefab", allowSceneObjects = false
            };
            prefabField.Bind(serializedObject);
            prefabField.TrackPropertyValue(serializedObject.FindProperty("Prefab"), property =>
            {
                var mesh = (property.objectReferenceValue as GameObject)?.GetComponent<MeshFilter>()
                    .sharedMesh;
                if (_meshPreview == null) _meshPreview = new MeshPreview(mesh);
                else _meshPreview.mesh = mesh;
            });
            // var topEntranceField = new Vector3Field("Top Entrance Position")
            // {
            //     value = _node.TopEntrancePosition, bindingPath = "TopEntrancePosition"
            // };
            // topEntranceField.Bind(serializedObject);
            // topEntranceField.AddManipulator(new GameObjectToVector3FieldDragAndDropManipulator(true));
            // var rightEntranceField = new Vector3Field("Right Entrance Position")
            // {
            //     value = _node.RightEntrancePosition, bindingPath = "RightEntrancePosition"
            // };
            // rightEntranceField.Bind(serializedObject);
            // rightEntranceField.AddManipulator(new GameObjectToVector3FieldDragAndDropManipulator(true));
            // var bottomEntranceField = new Vector3Field("Bottom Entrance Position")
            // {
            //     value = _node.BottomEntrancePosition,
            //     bindingPath = "BottomEntrancePosition"
            // };
            // bottomEntranceField.Bind(serializedObject);
            // bottomEntranceField.AddManipulator(new GameObjectToVector3FieldDragAndDropManipulator(true));
            // var leftEntranceField = new Vector3Field("Left Entrance Position")
            // {
            //     value = _node.LeftEntrancePosition, bindingPath = "LeftEntrancePosition"
            // };
            // leftEntranceField.Bind(serializedObject);
            // leftEntranceField.AddManipulator(new GameObjectToVector3FieldDragAndDropManipulator(true));

            prefabContainer.Add(prefabField);
            // prefabContainer.Add(topEntranceField);
            // prefabContainer.Add(rightEntranceField);
            // prefabContainer.Add(bottomEntranceField);
            // prefabContainer.Add(leftEntranceField);

            return prefabContainer;
        }

        private VisualElement MakeRuleList()
        {
            var rulesList = new VisualElement
                { style = { backgroundColor = new StyleColor(new Color(.2f, .2f, .2f)) } };

            rulesList.SetPadding(4);
            rulesList.SetBorderRadius(8);
            rulesList.SetMargin(8, 0, 0, 0);

            var rulesLabel = new Label("Rules")
                { style = { unityFontStyleAndWeight = new StyleEnum<FontStyle>(FontStyle.Bold) } };

            var rulesListView = new ListView(_node.Rules, makeItem: () => new WfcRuleVisualElement())
            {
                bindItem = (element, i) => ((WfcRuleVisualElement)element).SetRule(_node.Rules[i]),
                itemsSource = _node.Rules,
                style = { backgroundColor = new StyleColor(new Color(.2f, .2f, .2f)), flexGrow = 1 },
                reorderable = true,
                virtualizationMethod = CollectionVirtualizationMethod.DynamicHeight
            };

            rulesListView.SetMargin(4, 0, 8, 0);
            rulesListView.SetBorderRadius(8);

            var buttonContainer = new VisualElement
                { style = { flexDirection = FlexDirection.Row, flexShrink = 0, flexGrow = 0 } };
            var addRuleButton = new Button(() =>
            {
                var newRule = new WfcRule(_node);
                _node.Rules.Add(newRule);
                rulesListView.Rebuild();
                EditorUtility.SetDirty(_node);
            })
            {
                text = "+",
                style =
                {
                    flexBasis = new StyleLength(Length.Percent(8)),
                    backgroundColor = new StyleColor(new Color(.2f, .2f, .2f))
                }
            };

            addRuleButton.SetBorderWidth(1);
            addRuleButton.SetBorderRadius(4);
            addRuleButton.SetBorderColor(new Color(.1f, .1f, .1f));

            var removeRuleButton = new Button(() =>
            {
                if (_node.Rules.Count <= 0) return;
                _node.Rules.RemoveAt(_node.Rules.Count - 1);
                rulesListView.Rebuild();
                EditorUtility.SetDirty(_node);
            })
            {
                text = "-",
                style =
                {
                    flexBasis = new StyleLength(Length.Percent(8)),
                    backgroundColor = new StyleColor(new Color(.2f, .2f, .2f))
                }
            };

            removeRuleButton.SetBorderWidth(1);
            removeRuleButton.SetBorderRadius(4);
            removeRuleButton.SetBorderColor(new Color(.1f, .1f, .1f));

            buttonContainer.Add(addRuleButton);
            buttonContainer.Add(removeRuleButton);

            rulesList.Add(rulesLabel);
            rulesList.Add(rulesListView);
            rulesList.Add(buttonContainer);
            return rulesList;
        }

        private VisualElement MakeNeighbourList()
        {
            var container = new VisualElement
            {
                style =
                {
                    flexDirection = FlexDirection.Column, borderTopColor = _borderColor, flexGrow = 1
                }
            };
            container.SetBorderWidth(2, 0, 0, 0);
            container.SetPadding(8, 0, 0, 0);
            container.SetMargin(8, 0, 0, 0);

            var neighboursScrollView = new ScrollView { style = { height = new StyleLength(Length.Percent(100)) } };
            var neighboursContainer = new VisualElement();
            neighboursScrollView.Add(neighboursContainer);

            foreach (var neighbour in _node.Neighbours)
            {
                var neighbourSelectionManipulator = new VisualElementSelection();
                var neighbourElement = new WfcNeighbourVisualElement();
                neighbourElement.AddManipulator(neighbourSelectionManipulator);
                neighbourElement.SetNeighbour(neighbour);
                neighboursContainer.Add(neighbourElement);

                neighbourElement.RegisterCallback<ClickEvent>(evt =>
                {
                    if (_selectedNeighbour != null && _selectedNeighbour != neighbourElement)
                        _neighbourSelectionManipulator.OnSelect(evt, false);
                    _selectedNeighbour = neighbourElement;
                    _neighbourSelectionManipulator = neighbourSelectionManipulator;
                    EditorUtility.SetDirty(_node);
                });
            }

            var addNeighbourButton = new Button(() =>
            {
                var newNeighbour = new WfcNeighbour();
                _node.Neighbours.Add(newNeighbour);

                var neighbourSelectionManipulator = new VisualElementSelection();
                var neighbourElement = new WfcNeighbourVisualElement();
                neighbourElement.AddManipulator(new VisualElementSelection());
                neighbourElement.SetNeighbour(newNeighbour);
                neighboursContainer.Add(neighbourElement);

                neighbourElement.RegisterCallback<ClickEvent>(evt =>
                {
                    if (_selectedNeighbour != null && _selectedNeighbour != neighbourElement)
                        _neighbourSelectionManipulator.OnSelect(evt, false);
                    _selectedNeighbour = neighbourElement;
                    _neighbourSelectionManipulator = neighbourSelectionManipulator;
                    EditorUtility.SetDirty(_node);
                });
                EditorUtility.SetDirty(_node);
            }) { text = "Add Neighbour" };

            var removeNeighbourButton = new Button(() =>
            {
                if (_node.Neighbours.Count <= 0) return;
                if (_selectedNeighbour != null)
                {
                    _node.Neighbours.Remove(_selectedNeighbour.GetNeighbour());
                    neighboursContainer.Remove(_selectedNeighbour);
                    _selectedNeighbour = null;
                    _neighbourSelectionManipulator = null;
                }
                else
                {
                    _node.Neighbours.RemoveAt(_node.Neighbours.Count - 1);
                    neighboursContainer.RemoveAt(neighboursContainer.childCount - 1);
                    _selectedNeighbour = null;
                    _neighbourSelectionManipulator = null;
                }

                EditorUtility.SetDirty(_node);
            }) { text = "Remove Neighbour" };

            container.Add(neighboursScrollView);
            container.Add(addNeighbourButton);
            container.Add(removeNeighbourButton);

            return container;
        }

        private VisualElement MakeWeightField()
        {
            var weightContainer = new VisualElement { style = { flexDirection = FlexDirection.Row } };
            _weightField = new Slider("Weight", 0, 2)
            {
                value = _node.Weight, style = { flexBasis = new StyleLength(new Length(90, LengthUnit.Percent)) },
                bindingPath = "Weight"
            };
            _weightFieldValue = new FloatField
                { value = _node.Weight, style = { flexBasis = new StyleLength(new Length(10, LengthUnit.Percent)) } };
            _weightField.Bind(serializedObject);

            weightContainer.Add(_weightField);
            weightContainer.Add(_weightFieldValue);

            _weightField.RegisterValueChangedCallback(OnWeightChanged);
            _weightFieldValue.RegisterValueChangedCallback(OnWeightFieldChanged);

            return weightContainer;
        }

        private void OnWeightFieldChanged(ChangeEvent<float> evt)
        {
            var value = Mathf.Clamp(evt.newValue, _weightField.lowValue, _weightField.highValue);
            _weightField.value = value;
            ((FloatField)evt.currentTarget).value = value;
        }

        private void OnWeightChanged(ChangeEvent<float> evt)
        {
            _weightFieldValue.value = evt.newValue;
        }

        public override bool UseDefaultMargins()
        {
            return false;
        }

        public override bool HasPreviewGUI()
        {
            return true;
        }

        public override void OnPreviewGUI(Rect r, GUIStyle background)
        {
            _meshPreview?.OnPreviewGUI(r, background);
        }

        public override void OnPreviewSettings()
        {
            _meshPreview?.OnPreviewSettings();
        }

        private void OnDisable()
        {
            _weightField.UnregisterValueChangedCallback(OnWeightChanged);
            _weightFieldValue.UnregisterValueChangedCallback(OnWeightFieldChanged);
            _meshPreview?.Dispose();
        }
    }
}