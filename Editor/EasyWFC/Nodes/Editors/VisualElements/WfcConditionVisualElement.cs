using Core.Extensions;
using EasyWFC.General.Conditions;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyWFC.Nodes.Editors.VisualElements
{
    public class WfcConditionVisualElement : VisualElement
    {
        public WfcConditionVisualElement()
        {
        }

        public void SetCondition(WfcCondition condition)
        {
            style.flexDirection = FlexDirection.Row;
            Clear();
            this.SetBackgroundColor(new Color(.35f, .35f, .35f));
            this.SetBorderRadius(8);
            this.SetPadding(8);
            this.SetMargin(0, 0, 4, 0);

            var conditionName = new Label(condition.Name) { style = { marginBottom = 4 } };

            Add(conditionName);
            Add(condition.CreateVisualElement());
        }
    }
}