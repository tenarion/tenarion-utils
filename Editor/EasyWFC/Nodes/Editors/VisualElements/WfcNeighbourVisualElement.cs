using Core.Extensions;
using EasyWFC.General.Generation;
using EasyWFC.Nodes;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyWFC.Nodes.Editors.VisualElements
{
    public class WfcNeighbourVisualElement : VisualElement
    {
        private VisualMatrix<WfcNeighbourPosition> _visualMatrix;
        private bool _isSelected;

        private WfcNeighbour _neighbour;

        public WfcNeighbourVisualElement(WfcNeighbour neighbour)
        {
            SetNeighbour(neighbour);
        }

        public WfcNeighbourVisualElement()
        {
        }

        public void SetNeighbour(WfcNeighbour neighbour)
        {
            Clear();
            _neighbour = neighbour;
            this.SetBorderRadius(8);
            this.SetPadding(8);
            this.SetMargin(4);

            var neighbourLabel = new Label("Neighbour")
                { style = { unityFontStyleAndWeight = new StyleEnum<FontStyle>(FontStyle.Bold), marginBottom = 8 } };

            var neighbourField = new ObjectField
            {
                objectType = typeof(WfcNode),
                value = neighbour.Node
            };


            _visualMatrix = new VisualMatrix<WfcNeighbourPosition>
            {
                style =
                {
                    display = neighbour.Node != null ? DisplayStyle.Flex : DisplayStyle.None
                }
            };
            _visualMatrix.SetMargin(8);
            _visualMatrix.Set(WfcNeighbourPosition.Top, neighbour.GetAllowedPosition(WfcNeighbourPosition.Top), 1, 0);
            _visualMatrix.Set(WfcNeighbourPosition.Bottom, neighbour.GetAllowedPosition(WfcNeighbourPosition.Bottom), 1,
                2);
            _visualMatrix.Set(WfcNeighbourPosition.Left, neighbour.GetAllowedPosition(WfcNeighbourPosition.Left), 0, 1);
            _visualMatrix.Set(WfcNeighbourPosition.Right, neighbour.GetAllowedPosition(WfcNeighbourPosition.Right), 2,
                1);

            _visualMatrix.OnCellChanged += (pair, _, _) =>
            {
                neighbour.SetAllowedPosition(pair.Item1, pair.Item2 == VisualMatrixState.Active);
            };

            neighbourField.RegisterValueChangedCallback(evt =>
            {
                neighbour.Node = (WfcNode)evt.newValue;
                if(evt.newValue) _visualMatrix.Show();
                else _visualMatrix.Hide();
            });

            Add(neighbourLabel);
            Add(neighbourField);
            Add(_visualMatrix);
        }

        public WfcNeighbour GetNeighbour()
        {
            return _neighbour;
        }
    }
}
