using System;
using System.Linq;
using Core.Extensions;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyWFC.Nodes.Editors.VisualElements
{
    public class VisualMatrix<T> : VisualElement
    {
        private readonly (T Element, VisualMatrixState State)[,] _matrix;

        private readonly Sprite _crossSprite;
        private readonly Sprite _circleSprite;
        private readonly Sprite _checkSprite;

        public event Action<(T Element, VisualMatrixState State), int, int> OnCellChanged;


        public VisualMatrix()
        {
            _matrix = new (T, VisualMatrixState)[3, 3];
            style.flexDirection = FlexDirection.Column;
            _crossSprite = UnityEngine.Resources.Load<Sprite>("Images/cross_t");
            _circleSprite = UnityEngine.Resources.Load<Sprite>("Images/circle_t");
            _checkSprite = UnityEngine.Resources.Load<Sprite>("Images/check_t");


            for (var y = 0; y < 3; y++)
            {
                var buttonRow = new VisualElement { style = { flexDirection = FlexDirection.Row } };
                for (var x = 0; x < 3; x++)
                {
                    var x1 = x;
                    var y1 = y;
                    var button = new Button
                    {
                        style =
                        {
                            width = 20,
                            height = 20,
                            backgroundColor = new StyleColor(new Color(.39f, .39f, .39f))
                        }
                    };
                    button.SetMargin(0);
                    button.SetBorderRadius(0);
                    buttonRow.Add(button);

                    if ((x == 0 && y == 0) || (x == 2 && y == 2) || (x == 0 && y == 2) || (x == 2 && y == 0))
                    {
                        _matrix[x, y] = (default, VisualMatrixState.ReadOnly);
                        continue;
                    }

                    if (x == 1 && y == 1)
                    {
                        button.style.backgroundImage =
                            new StyleBackground(_circleSprite);
                        _matrix[x, y] = (default, VisualMatrixState.ReadOnly);
                    }
                    else
                    {
                        button.style.backgroundImage = new StyleBackground(_crossSprite);
                        button.clicked += () => ChangeState(x1, y1);
                    }
                }

                Add(buttonRow);
            }
        }

        private void ChangeState(int x, int y)
        {
            if (Children().ElementAt(y).Children().ElementAt(x) is not Button button) return;

            var element = _matrix[x, y].Element;

            switch (_matrix[x, y].State)
            {
                case VisualMatrixState.Inactive:
                    _matrix[x, y] = (_matrix[x, y].Element, VisualMatrixState.Active);
                    button.style.backgroundImage =
                        new StyleBackground(_checkSprite);
                    break;
                case VisualMatrixState.Active:
                    _matrix[x, y] = (_matrix[x, y].Element, VisualMatrixState.Inactive);
                    button.style.backgroundImage =
                        new StyleBackground(_crossSprite);
                    break;
                case VisualMatrixState.ReadOnly:
                default:
                    // Do nothing
                    break;
            }

            OnCellChanged?.Invoke((element, _matrix[x, y].State), x, y);
        }

        public void Set(T key, int x, int y)
        {
            if (x < 0 || x > 2 || y < 0 || y > 2)
            {
                Debug.LogError("Invalid index on visual matrix");
                return;
            }

            if (Children().ElementAt(y).Children().ElementAt(x) is not Button button) return;
            _matrix[x, y] = (key, VisualMatrixState.Inactive);
            switch (_matrix[x, y].State)
            {
                case VisualMatrixState.Inactive:
                    button.style.backgroundImage =
                        new StyleBackground(_crossSprite);
                    break;
                case VisualMatrixState.Active:
                    button.style.backgroundImage =
                        new StyleBackground(_checkSprite);
                    break;
                case VisualMatrixState.ReadOnly:
                default:
                    // Do nothing
                    break;
            }
        }

        public void Set(T key, bool state, int x, int y)
        {
            if (x < 0 || x > 2 || y < 0 || y > 2)
            {
                Debug.LogError("Invalid index on visual matrix");
                return;
            }

            // Debug.Log(key + " " + state + " " + x + " " + y);
            if (Children().ElementAt(y).Children().ElementAt(x) is not Button button) return;
            _matrix[x, y] = (key, state ? VisualMatrixState.Active : VisualMatrixState.Inactive);

            switch (_matrix[x, y].State)
            {
                case VisualMatrixState.Inactive:
                    button.style.backgroundImage =
                        new StyleBackground(_crossSprite);
                    break;
                case VisualMatrixState.Active:
                    button.style.backgroundImage =
                        new StyleBackground(_checkSprite);
                    break;
                case VisualMatrixState.ReadOnly:
                default:
                    // Do nothing
                    break;
            }
        }

        public T Get(int x, int y) => _matrix[x, y].Element;

        public (T Element, VisualMatrixState State) GetState(int x, int y) => _matrix[x, y];

        public bool IsReadOnly(int x, int y) => _matrix[x, y].State == VisualMatrixState.ReadOnly;

        public bool IsActive(T key)
        {
            return _matrix.Cast<(T Element, VisualMatrixState State)>()
                .Any(tuple => tuple.Element.Equals(key) && tuple.State == VisualMatrixState.Active);
        }

        public bool IsActive(int x, int y) => _matrix[x, y].State == VisualMatrixState.Active;
    }
}
