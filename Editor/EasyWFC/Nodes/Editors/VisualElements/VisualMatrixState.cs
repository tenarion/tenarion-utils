namespace Editor.EasyWFC.Nodes.Editors.VisualElements
{
    public enum VisualMatrixState
    {
        Inactive,
        Active,
        ReadOnly
    }
}