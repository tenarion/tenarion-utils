using Core.Extensions;
using EasyWFC.General.Conditions;
using EasyWFC.General.Rules;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyWFC.Nodes.Editors.VisualElements
{
    public class WfcRuleVisualElement : VisualElement
    {
        public WfcRuleVisualElement(WfcRule rule)
        {
            SetRule(rule);
        }

        public WfcRuleVisualElement()
        {
        }

        public void SetRule(WfcRule rule)
        {
            Clear();
            this.SetBorderRadius(8);
            this.SetPadding(8);
            this.SetMargin(0, 0, 4, 0);
            style.backgroundColor = new StyleColor(new Color(.25f, .25f, .25f));

            var ruleLabel = new Label("New Rule") { style = { marginBottom = 4 } };
            var conditionBlockListView =
                new ListView(rule.ConditionBlocks, makeItem: () =>
                {
                    var container = new VisualElement();
                    container.Add(new WfcConditionBlockVisualElement());
                    var logicGateField = new EnumField
                        { style = { marginBottom = 8, alignSelf = Align.Center }, name = "logicGateField" };
                    container.Add(logicGateField);
                    return container;
                })
                {
                    bindItem = (element, i) =>
                    {
                        element.Q<WfcConditionBlockVisualElement>().SetConditionBlock(rule.ConditionBlocks[i]);
                        var logicGateField = element.Q<EnumField>("logicGateField");
                        logicGateField.Init(rule.ConditionBlocks[i].LogicGate);
                        logicGateField.RegisterValueChangedCallback(evt =>
                            rule.ConditionBlocks[i].LogicGate = (LogicGate)evt.newValue);
                    },
                    itemsSource = rule.ConditionBlocks,
                    reorderable = true,
                    virtualizationMethod = CollectionVirtualizationMethod.DynamicHeight
                };

            var buttonContainer = new VisualElement
            {
                style =
                {
                    flexDirection = FlexDirection.Row,
                    flexGrow = 0,
                    flexShrink = 0,
                    marginTop = 8
                }
            };
            var addConditionBlockButton = new Button(() =>
            {
                rule.ConditionBlocks.Add(new WfcConditionBlock(rule.CurrentNode));
                conditionBlockListView.Rebuild();
                EditorUtility.SetDirty(rule.CurrentNode);
            })
            {
                text = "Add Condition Block"
            };

            var removeConditionBlockButton = new Button(() =>
            {
                if (rule.ConditionBlocks.Count <= 0) return;
                rule.ConditionBlocks.RemoveAt(rule.ConditionBlocks.Count - 1);
                conditionBlockListView.Rebuild();
                EditorUtility.SetDirty(rule.CurrentNode);
            })
            {
                text = "Remove Condition Block"
            };

            buttonContainer.Add(addConditionBlockButton);
            buttonContainer.Add(removeConditionBlockButton);

            var content = new VisualElement
            {
                style = { flexDirection = FlexDirection.Column, flexGrow = 1, justifyContent = Justify.SpaceBetween }
            };

            content.Add(conditionBlockListView);
            content.Add(buttonContainer);

            Add(ruleLabel);
            Add(content);
        }
    }
}