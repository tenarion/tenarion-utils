using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyWFC.Nodes.Editors.VisualElements.Manipulators
{
    public class VisualElementSelection : MouseManipulator
    {
        private Color _originalColor = new(0.19f, 0.19f, 0.19f);
        private Color _hoverColor = new(0.3f, 0.3f, 0.3f);
        private Color _selectedColor = new(0.17f, 0.36f, 0.53f);
        private bool _isSelected;

        protected override void RegisterCallbacksOnTarget()
        {
            target.style.backgroundColor = _originalColor;
            target.RegisterCallback<MouseEnterEvent>(OnMouseEnter);
            target.RegisterCallback<MouseLeaveEvent>(OnMouseLeave);
            target.RegisterCallback<ClickEvent>(OnMouseClick);
        }

        private void OnMouseEnter(MouseEnterEvent evt)
        {
            if (!_isSelected) target.style.backgroundColor = _hoverColor;
        }

        private void OnMouseLeave(MouseLeaveEvent evt)
        {
            if (!_isSelected) target.style.backgroundColor = _originalColor;
        }

        public void OnSelect(ClickEvent evt, bool selected)
        {
            _isSelected = selected;
            target.style.backgroundColor = _isSelected ? _selectedColor : _originalColor;
        }

        protected override void UnregisterCallbacksFromTarget()
        {
            target.UnregisterCallback<MouseEnterEvent>(OnMouseEnter);
            target.UnregisterCallback<MouseLeaveEvent>(OnMouseLeave);
            target.UnregisterCallback<ClickEvent>(OnMouseClick);
        }

        private void OnMouseClick(ClickEvent evt)
        {
            OnSelect(evt, true);
        }
    }
}