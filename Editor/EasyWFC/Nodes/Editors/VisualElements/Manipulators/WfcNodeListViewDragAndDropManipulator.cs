using EasyWFC.Nodes;
using Editor.EasyUI.Manipulators;
using UnityEngine.UIElements;

namespace Editor.EasyWFC.Nodes.Editors.VisualElements.Manipulators
{
    public class WfcNodeListViewDragAndDropManipulator : DragAndDropManipulator<ListView, WfcNode>
    {
        protected override void HandleData(ListView targetView, WfcNode[] data)
        {
            targetView.itemsSource.Add(data);
            targetView.Rebuild();
        }
    }
}