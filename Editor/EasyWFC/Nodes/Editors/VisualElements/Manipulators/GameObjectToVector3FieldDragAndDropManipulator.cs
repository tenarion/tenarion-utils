using Editor.EasyUI.Manipulators;
using UnityEditor.Search;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyWFC.Nodes.Editors.VisualElements.Manipulators
{
    public class GameObjectToVector3FieldDragAndDropManipulator : DragAndDropManipulator<Vector3Field, GameObject>
    {
        private readonly bool _localPosition;

        public GameObjectToVector3FieldDragAndDropManipulator(bool localPosition = false)
        {
            _localPosition = localPosition;
        }

        protected override void HandleData(Vector3Field targetView, GameObject[] data)
        {
            targetView.value = _localPosition ? data[0].transform.localPosition : data[0].transform.position;
        }
    }
}