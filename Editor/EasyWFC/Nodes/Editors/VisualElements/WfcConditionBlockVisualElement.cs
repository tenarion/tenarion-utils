using System;
using System.Linq;
using Core.Extensions;
using EasyWFC.General.Conditions;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.EasyWFC.Nodes.Editors.VisualElements
{
    public class WfcConditionBlockVisualElement : VisualElement
    {
        public WfcConditionBlockVisualElement()
        {
        }

        public void SetConditionBlock(WfcConditionBlock conditionBlock)
        {
            Clear();
            this.SetBorderRadius(8);
            this.SetPadding(8);
            this.SetMargin(0, 0, 4, 0);
            this.SetBackgroundColor(new Color(.3f, .3f, .3f));

            var conditionListView = new ListView(conditionBlock.Conditions, makeItem: () =>
            {
                var container = new VisualElement();
                container.Add(new WfcConditionVisualElement());
                var logicGateField = new EnumField { style = { marginBottom = 8 }, name = "logicGateField" };
                container.Add(logicGateField);
                return container;
            })
            {
                bindItem = (element, i) =>
                {
                    element.Q<WfcConditionVisualElement>().SetCondition(conditionBlock.Conditions[i]);
                    var logicGateField = element.Q<EnumField>("logicGateField");
                    logicGateField.style.alignSelf = Align.Center;

                    logicGateField.Init(conditionBlock.Conditions[i].LogicGate);
                    logicGateField.RegisterValueChangedCallback(evt =>
                        conditionBlock.Conditions[i].LogicGate = (LogicGate)evt.newValue);
                },
                itemsSource = conditionBlock.Conditions,
                reorderable = true,
                showBoundCollectionSize = true,
                virtualizationMethod = CollectionVirtualizationMethod.DynamicHeight
            };

            var buttonContainer = new VisualElement
            {
                style =
                {
                    flexDirection = FlexDirection.Row,
                    flexGrow = 0,
                    flexShrink = 0,
                    marginTop = 8
                }
            };

            var addConditionButton = new Button()
            {
                text = "Add Condition"
            };

            addConditionButton.clicked += () =>
            {
                // Create a new GenericMenu.
                var menu = new GenericMenu();

                // Get all types that derive from WfcCondition.
                var conditionTypes = AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(assembly => assembly.GetTypes())
                    .Where(type => type.IsSubclassOf(typeof(WfcCondition)) && !type.IsAbstract);

                // Add an item for each condition type.
                foreach (var conditionType in conditionTypes)
                    menu.AddItem(new GUIContent(conditionType.Name), false, () =>
                    {
                        // When an item is selected, create an instance of the selected type and add it to the list.
                        var condition =
                            (WfcCondition)Activator.CreateInstance(conditionType, args: conditionBlock.CurrentNode);
                        conditionBlock.Conditions.Add(condition);
                        conditionListView.Rebuild();
                        EditorUtility.SetDirty(conditionBlock.CurrentNode);
                    });

                // Show the menu under the button.
                menu.DropDown(addConditionButton.worldBound);
            };

            var removeConditionButton = new Button(() =>
            {
                if (conditionBlock.Conditions.Count <= 0) return;
                conditionBlock.Conditions.RemoveAt(conditionBlock.Conditions.Count - 1);
                conditionListView.Rebuild();
                EditorUtility.SetDirty(conditionBlock.CurrentNode);
            })
            {
                text = "Remove Condition"
            };

            buttonContainer.Add(addConditionButton);
            buttonContainer.Add(removeConditionButton);

            var bottomContainer = new VisualElement
            {
                style =
                {
                    flexDirection = FlexDirection.Column,
                    flexGrow = 0,
                    flexShrink = 0
                }
            };
            var content = new VisualElement
            {
                style =
                {
                    flexDirection = FlexDirection.Column, flexGrow = 1, justifyContent = Justify.SpaceBetween
                }
            };

            bottomContainer.Add(buttonContainer);
            content.Add(conditionListView);
            content.Add(bottomContainer);

            Add(new Label("Condition Block"));
            Add(content);
        }
    }
}