using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditor.PackageManager.UI;
using UnityEngine;
using UnityEngine.UIElements;
using PackageInfo = UnityEditor.PackageManager.PackageInfo;

namespace Editor.Nicho
{
    // [InitializeOnLoad]
    // public class CustomPackageUI : IPackageManagerExtension
    // {
    //     private const string PackageName = "com.tenarion.easyutils";
    //     private const string HeaderSelectedClass = "tabHeaderSelected";
    //
    //     private PackageInfo _currentPackageInfo;
    //
    //     private bool _mustRender;
    //
    //     private VisualElement _renderedHeader;
    //     private VisualElement _renderedBody;
    //
    //     private VisualElement _targetHeader;
    //     private VisualElement _targetBody;
    //
    //     static CustomPackageUI()
    //     {
    //         PackageManagerExtensions.RegisterExtension(new CustomPackageUI());
    //     }
    //
    //     public VisualElement CreateExtensionUI()
    //     {
    //         _renderedHeader = new Button(() =>
    //         {
    //             SetButtonsUnpressed(_renderedHeader.parent);
    //             _renderedHeader.AddToClassList(HeaderSelectedClass);
    //         }) { text = "Nicho" };
    //         _renderedBody = new NichoPackageManagerBody();
    //         return _renderedHeader;
    //         // refrence Unity's assembly and via reflection get the types of some internal classes
    //     }
    //
    //
    //     public void OnPackageSelectionChange(PackageInfo packageInfo)
    //     {
    //         if (_renderedHeader.parent != null)
    //         {
    //             _targetHeader = _renderedHeader.parent.parent.Q<VisualElement>("packageDetailsTabViewHeaderContainer");
    //             _targetBody = _renderedHeader.parent.parent.Q<VisualElement>("packageDetailsTabViewBodyContainer");
    //             MonoBehaviour.print(_targetBody);
    //         }
    //
    //         _currentPackageInfo = packageInfo;
    //
    //         _mustRender = _currentPackageInfo?.name == PackageName;
    //         if (!_mustRender)
    //         {
    //             _renderedHeader.RemoveFromHierarchy();
    //             _renderedBody.RemoveFromHierarchy();
    //         }
    //         else
    //         {
    //             _targetHeader.Add(_renderedHeader);
    //             _targetBody.Add(_renderedBody);
    //         }
    //
    //         var packageManagerWindowType = typeof(PackageManagerExtensions).Assembly;
    //
    //         //from packageManagerWindowType get all types from the namespace UnityEditor.PackageManager.UI.Internal, keep in mind that this is a internal namespace
    //         var internalNamespaceTypes = packageManagerWindowType.GetTypes()
    //             .Where(t => t.Namespace == "UnityEditor.PackageManager.UI.Internal");
    //         MonoBehaviour.print(internalNamespaceTypes);
    //     }
    //
    //     private void SetButtonsUnpressed(VisualElement parent)
    //     {
    //         parent.Query<Button>().ForEach(button => { button.RemoveFromClassList(HeaderSelectedClass); });
    //     }
    //
    //     public void OnPackageAddedOrUpdated(PackageInfo packageInfo)
    //     {
    //         // throw new System.NotImplementedException();
    //     }
    //
    //     public void OnPackageRemoved(PackageInfo packageInfo)
    //     {
    //         // throw new System.NotImplementedException();
    //     }
    // }
}
