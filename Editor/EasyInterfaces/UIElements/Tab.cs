using UnityEngine.UIElements;

namespace Editor.EasyInterfaces.UIElements
{
	internal class Tab : Toggle
	{
		public Tab(string text)
		{
			this.text = text;
			RemoveFromClassList(ussClassName);
			AddToClassList(ussClassName);
		}
	}
}
